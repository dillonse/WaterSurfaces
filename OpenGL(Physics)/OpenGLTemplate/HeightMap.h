#pragma once
#include <glm/common.hpp>

class HeightParameters {

};

class RenderParameters {


};

class HeightMap {
public:
	glm::ivec2 size;
	glm::vec2 offset;
	virtual float getHeight(int i, int j,HeightParameters& pars)=0;
	virtual void Render(RenderParameters& pars)=0;
};

