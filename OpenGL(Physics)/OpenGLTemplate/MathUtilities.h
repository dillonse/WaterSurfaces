#pragma once
#include <glm\common.hpp>
#include <glm\gtx\common.hpp>
class MathUtilities {
public:
	static float cross(glm::vec2, glm::vec2);
	static glm::vec2 barycentric_interpolation(glm::vec2 p0, glm::vec2 p1, glm::vec2 p2, glm::vec2 q,glm::vec2 v0, glm::vec2 v1, glm::vec2 v2);
	static float barycentric_interpolation(glm::vec2 p0, glm::vec2 p1, glm::vec2 p2, glm::vec2 q, float v0, float v1, float v2);
	static glm::vec2 projection(glm::vec2 p1, glm::vec2 p2, glm::vec2 q);
	static glm::vec2 lin_inter(glm::vec2 p0, glm::vec2 p1, glm::vec2 q,glm::vec2 v0, glm::vec2 v1);
	static glm::vec2 bilin_inter(glm::vec2 p0, glm::vec2 p1, glm::vec2 p2, glm::vec2 p3, glm::vec2 q, glm::vec2 v0, glm::vec2 v1, glm::vec2 v2, glm::vec2 v3);
	static float lin_inter(glm::vec2 p0, glm::vec2 p1, glm::vec2 q, float v0, float v1);
	static float bilin_inter(glm::vec2 p0, glm::vec2 p1, glm::vec2 p2, glm::vec2 p3, glm::vec2 q, float v0, float v1, float v2, float v3);
	static glm::vec2 IDW(glm::vec2 p0, glm::vec2 p1, glm::vec2 p2, glm::vec2 p3, glm::vec2 query,glm::vec2 v0, glm::vec2 v1, glm::vec2 v2, glm::vec2 v3);
	static float IDW(glm::vec2 p0, glm::vec2 p1, glm::vec2 p2, glm::vec2 p3, glm::vec2 query, float v0, float v1, float v2, float v3);
	static bool isLeft(glm::vec2 start, glm::vec2 end, glm::vec2 query);
	static bool isRight(glm::vec2 start, glm::vec2 end, glm::vec2 query);
	static float normalizedNoise(float scale);
	static float normalizedNoiseTiny();
};