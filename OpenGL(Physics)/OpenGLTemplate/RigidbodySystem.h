#pragma once
#include "Rigidbody.h"
#include "UnaryForce.h"
#include "Plane.h"
#include "Contact.h"
class RigidbodySystem {
public:
	int gravityIndex = -1;
	std::vector<UnaryForce*> unaryForces;
	std::vector<Rigidbody> rigidBodies;
	std::vector<Plane> planes;
	std::vector<std::pair<int,int>> broadPhaseCollisions;
	std::vector<Contact> contacts;
	material lineMat;
	void UpdateNearestPoints();
	void UpdateForcesTorque(float deltaTime);
	void UpdateMomentums(float deltaTime);
	void UpdateAuxilary(float deltaTime);
	void UpdateGeometry(float deltaTime);
	void UpdateBoundingVolumes(float deltaTime);
	void DetectCollisionBoundingSpheres();
	void DetectCollisionOBBs();
	void DetectCollisions();
	void DetectContacts();
	void HandleContacts(float deltaTime);
	void Render();
	void RenderDebug();
};