#include "MathUtilities.h"
#include <random>
#include "global.h"

std::default_random_engine generator;
std::uniform_real_distribution<float> distribution(0.0f, 1.0f);
std::uniform_real_distribution<float> tinyDistribution(0.0f, 0.0f);


float MathUtilities::cross(glm::vec2 v0, glm::vec2 v1) {
	return v0.x*v1.y - v0.y*v1.x;
}

glm::vec2 MathUtilities::barycentric_interpolation(glm::vec2 p0, glm::vec2 p1, glm::vec2 p2, glm::vec2 q,
	glm::vec2 v0, glm::vec2 v1, glm::vec2 v2) {
	float w2 = glm::length(glm::cross(glm::vec3(q.x, 0, q.y) - glm::vec3(p0.x, 0, p0.y), glm::vec3(q.x, 0, q.y) - glm::vec3(p1.x, 0, p1.y)));
	float w0 = glm::length(glm::cross(glm::vec3(q.x, 0, q.y) - glm::vec3(p1.x, 0, p1.y), glm::vec3(q.x, 0, q.y) - glm::vec3(p2.x, 0, p2.y)));
	float w1 = glm::length(glm::cross(glm::vec3(q.x, 0, q.y) - glm::vec3(p2.x, 0, p2.y), glm::vec3(q.x, 0, q.y) - glm::vec3(p0.x, 0, p0.y)));
	float total = glm::length(glm::cross(glm::vec3(p1.x, 0, p1.y) - glm::vec3(p0.x, 0, p0.y), glm::vec3(p2.x, 0, p2.y) - glm::vec3(p0.x, 0, p0.y)));
	w2 /= total; w0 /= total; w1 /= total;
	return w0*v0 + w1*v1 + w2*v2;
}

float MathUtilities::barycentric_interpolation(glm::vec2 p0, glm::vec2 p1, glm::vec2 p2, glm::vec2 q,
	float v0, float v1, float v2) {
	float w2 = glm::length(glm::cross(glm::vec3(q.x, 0, q.y) - glm::vec3(p0.x, 0, p0.y), glm::vec3(q.x, 0, q.y) - glm::vec3(p1.x, 0, p1.y)));
	float w0 = glm::length(glm::cross(glm::vec3(q.x, 0, q.y) - glm::vec3(p1.x, 0, p1.y), glm::vec3(q.x, 0, q.y) - glm::vec3(p2.x, 0, p2.y)));
	float w1 = glm::length(glm::cross(glm::vec3(q.x, 0, q.y) - glm::vec3(p2.x, 0, p2.y), glm::vec3(q.x, 0, q.y) - glm::vec3(p0.x, 0, p0.y)));
	float total = glm::length(glm::cross(glm::vec3(p1.x, 0, p1.y) - glm::vec3(p0.x, 0, p0.y), glm::vec3(p2.x, 0, p2.y) - glm::vec3(p0.x, 0, p0.y)));
	w2 /= total; w0 /= total; w1 /= total;
	return w0*v0 + w1*v1 + w2*v2;
}

glm::vec2 MathUtilities::projection(glm::vec2 p1, glm::vec2 p2, glm::vec2 q) {
	return p1 + (glm::dot(q - p1, p2 - p1) / glm::dot(p2 - p1, p2 - p1))*(p2 - p1);
}

glm::vec2 MathUtilities::lin_inter(glm::vec2 p0, glm::vec2 p1, glm::vec2 q,
	glm::vec2 v0, glm::vec2 v1) {
	float total_dist = glm::distance(p0, p1);
	return (glm::distance(q, p0) / total_dist)*v1 + (1.0f - (glm::distance(q, p0) / total_dist))*v0;
}

glm::vec2 MathUtilities::bilin_inter(glm::vec2 p0, glm::vec2 p1, glm::vec2 p2, glm::vec2 p3, glm::vec2 q,
	glm::vec2 v0, glm::vec2 v1, glm::vec2 v2, glm::vec2 v3) {
	glm::vec2 p01 = MathUtilities::projection(p0, p1, q);
	glm::vec2 v01 = MathUtilities::lin_inter(p0, p1, q, v0, v1);

	glm::vec2 p23 = MathUtilities::projection(p2, p3, q);
	glm::vec2 v23 = MathUtilities::lin_inter(p2, p3, q, v2, v3);

	glm::vec2 p0123 = MathUtilities::projection(p01, p23, q);
	glm::vec2 v0123 = MathUtilities::lin_inter(p01, p23, q, v01, v23);
	return v0123;
}

float MathUtilities::lin_inter(glm::vec2 p0, glm::vec2 p1, glm::vec2 q,
	float v0, float v1) {
	float total_dist = glm::distance(p0, p1);
	return (glm::distance(q, p0) / total_dist)*v1 + (1.0f - (glm::distance(q, p0) / total_dist))*v0;
}

float MathUtilities::bilin_inter(glm::vec2 p0, glm::vec2 p1, glm::vec2 p2, glm::vec2 p3, glm::vec2 q,
	float v0, float v1, float v2, float v3) {
	glm::vec2 p01 = MathUtilities::projection(p0, p1, q);
	float v01 = MathUtilities::lin_inter(p0, p1, q, v0, v1);

	glm::vec2 p23 = MathUtilities::projection(p2, p3, q);
	float v23 = MathUtilities::lin_inter(p2, p3, q, v2, v3);

	glm::vec2 p0123 = MathUtilities::projection(p01, p23, q);
	float v0123 = MathUtilities::lin_inter(p01, p23, q, v01, v23);
	return v0123;
}



glm::vec2 MathUtilities::IDW(glm::vec2 p0, glm::vec2 p1, glm::vec2 p2, glm::vec2 p3, glm::vec2 query,
	glm::vec2 v0, glm::vec2 v1, glm::vec2 v2, glm::vec2 v3) {
	int p = 3;
	float w0 = glm::max(glm::pow(glm::distance(p0, query), p),0.0001f);
	float w1 = glm::max(glm::pow(glm::distance(p1, query), p),0.0001f);
	float w2 = glm::max(glm::pow(glm::distance(p2, query), p),0.0001f);
	float w3 = glm::max(glm::pow(glm::distance(p3, query), p),0.0001f);
	float total_w = (1.0f / w0) + (1.0f / w1) + (1.0f / w2) + (1.0f / w3);
	return  (v0 / w0 + v1 / w1 + v2 / w2 + v3 / w3) / total_w;
}

float MathUtilities::IDW(glm::vec2 p0, glm::vec2 p1, glm::vec2 p2, glm::vec2 p3, glm::vec2 query,
	float v0, float v1, float v2, float v3) {
	int p = 3;
	float w0 = glm::max(glm::pow(glm::distance(p0, query), p), 0.0001f);
	float w1 = glm::max(glm::pow(glm::distance(p1, query), p), 0.0001f);
	float w2 = glm::max(glm::pow(glm::distance(p2, query), p), 0.0001f);
	float w3 = glm::max(glm::pow(glm::distance(p3, query), p), 0.0001f);
	float total_w = (1.0f / w0) + (1.0f / w1) + (1.0f / w2) + (1.0f / w3);
	return  (v0 / w0 + v1 / w1 + v2 / w2 + v3 / w3) / total_w;
}

bool MathUtilities::isLeft(glm::vec2 start, glm::vec2 end, glm::vec2 query) {
	if (end == start)return true;
	float sign = ((end.x - start.x)*(query.y - start.y) - (end.y - start.y)*(query.x - start.x));
	if (sign != 0)return sign > 0;
	else {
		glm::vec2 normal;
		if ((start.x == end.x ? start.y > end.y:start.x > end.x)) {
			normal = glm::normalize(start - end);
		}
		else {
			normal = glm::normalize(end - start);
		}
		normal = glm::vec2(normal.y, -normal.x);
		query += normal*1.0f;
		return ((end.x - start.x)*(query.y - start.y) - (end.y - start.y)*(query.x - start.x)) > 0;
	}
}

bool MathUtilities::isRight(glm::vec2 start, glm::vec2 end, glm::vec2 query) {
	if (end == start)return true;
	return isLeft(end, start, query);
}

float MathUtilities::normalizedNoise(float scale)
{
	return scale*distribution(generator)*2.0f*grid::offset.x;
}

float MathUtilities::normalizedNoiseTiny() {
	return tinyDistribution(generator);
}