#include <SOIL.h>
#include <GL/glew.h>
#include "textures.h"
#include <vector>
#include "global.h"

GLuint textures::loadCubemap(std::vector<std::string> faces)
{
	GLuint tex_cubemap=SOIL_load_OGL_cubemap(faces[0].c_str(), faces[1].c_str(), faces[2].c_str(), faces[3].c_str(), faces[4].c_str(), faces[5].c_str(),
		SOIL_LOAD_RGB,
		SOIL_CREATE_NEW_ID,
		SOIL_FLAG_MIPMAPS);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	if (tex_cubemap == 0)return -1;
	return tex_cubemap;
}

GLuint textures::loadTextureRGB(std::string path)
{
	GLuint tex_2d= SOIL_load_OGL_texture(path.c_str(),
		SOIL_LOAD_AUTO,
		SOIL_CREATE_NEW_ID,
		SOIL_FLAG_NTSC_SAFE_RGB | SOIL_FLAG_MULTIPLY_ALPHA
	);
	if (tex_2d == 0)return-1;
	return tex_2d;
}

GLuint textures::loadTextureGrayScale(std::string path) {
	GLuint tex_2d = SOIL_load_OGL_texture(path.c_str(),
		1,
		SOIL_CREATE_NEW_ID,
		SOIL_FLAG_NTSC_SAFE_RGB | SOIL_FLAG_MULTIPLY_ALPHA
	);
	if (tex_2d == 0)return-1;
	return tex_2d;
}

GLuint textures::loadTexture(std::vector<GLfloat>& image, GLuint imageTexture1)
{
	if (image.empty()) {
		return -1;
	}
	else {
		//glTexEnvi(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);
		glTexEnvi(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_REPLACE);
		//GLuint imageTexture1;
		if (imageTexture1 == -1) {
			glGenTextures(1, &imageTexture1);
			glBindTexture(GL_TEXTURE_2D, imageTexture1);

			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
			glTexImage2D(GL_TEXTURE_2D,         // Type of texture
				0,                   // Pyramid level (for mip-mapping) - 0 is the top level
				GL_RGB,              // Internal colour format to convert to
				grid::size.x,          // Image width  i.e. 640 for Kinect in standard mode
				grid::size.y,          // Image height i.e. 480 for Kinect in standard mode
				0,                   // Border width in pixels (can either be 1 or 0)
				GL_RED,              // Input image format (i.e. GL_RGB, GL_RGBA, GL_BGR etc.)
				GL_FLOAT,    // Image data type
				&(image[0]));        // The actual image data itself
		}
		else {
			glBindTexture(GL_TEXTURE_2D, imageTexture1);

			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
			glTexImage2D(GL_TEXTURE_2D,         // Type of texture //
				0,                   // Pyramid level (for mip-mapping) - 0 is the top level
				GL_RED,              // Internal colour format to convert to
				grid::size.x,          // Image width  i.e. 640 for Kinect in standard mode
				grid::size.y,          // Image height i.e. 480 for Kinect in standard mode
				0,                   // Border width in pixels (can either be 1 or 0)
				GL_RED,              // Input image format (i.e. GL_RGB, GL_RGBA, GL_BGR etc.)
				GL_FLOAT,    // Image data type
				&(image[0]));        // The actual image data itself
		}

		return imageTexture1;
	}
}

GLuint textures::loadTexture(std::vector<glm::vec2>& image, GLuint imageTexture1)
{
	if (image.empty()) {
		return -1;
	}
	else {
		//glTexEnvi(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);
		glTexEnvi(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_REPLACE);
		//GLuint imageTexture1;
		if (imageTexture1 == -1) {
			glGenTextures(1, &imageTexture1);
			glBindTexture(GL_TEXTURE_2D, imageTexture1);

			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
			glTexImage2D(GL_TEXTURE_2D,         // Type of texture
				0,                   // Pyramid level (for mip-mapping) - 0 is the top level
				GL_RG,              // Internal colour format to convert to
				grid::size.x,          // Image width  i.e. 640 for Kinect in standard mode
				grid::size.y,          // Image height i.e. 480 for Kinect in standard mode
				0,                   // Border width in pixels (can either be 1 or 0)
				GL_RG,              // Input image format (i.e. GL_RGB, GL_RGBA, GL_BGR etc.)
				GL_FLOAT,    // Image data type
				&(image[0]));        // The actual image data itself
		}
		else {
			glBindTexture(GL_TEXTURE_2D, imageTexture1);

			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
			glTexImage2D(GL_TEXTURE_2D,         // Type of texture //
				0,                   // Pyramid level (for mip-mapping) - 0 is the top level
				GL_RG,              // Internal colour format to convert to
				grid::size.x,          // Image width  i.e. 640 for Kinect in standard mode
				grid::size.y,          // Image height i.e. 480 for Kinect in standard mode
				0,                   // Border width in pixels (can either be 1 or 0)
				GL_RG,              // Input image format (i.e. GL_RGB, GL_RGBA, GL_BGR etc.)
				GL_FLOAT,    // Image data type
				&(image[0]));        // The actual image data itself
		}

		return imageTexture1;
	}
}


void textures::updateWaterHeights(std::vector<GLfloat>& heights,GLuint textureID) {



	glTextureSubImage2D(textureID, GL_TEXTURE_2D, 0, 0, grid::size.x, grid::size.y, GL_RED,GL_FLOAT,&(heights[0]));
}
