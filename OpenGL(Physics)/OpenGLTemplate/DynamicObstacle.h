#pragma once
#include "instance.h"
#include "Wavefront.h"
#include "Rigidbody.h"
#include "WaterHeightMap.h"
#include "WFHeightMap.h"

class  DynamicObstacle
{
private:
	enum Location {
		LOCATION_INSIDE, LOCATION_OUTSIDE, LOCATION_ON
	};
	Location findLocation(glm::vec2 p0, glm::vec2 p1, glm::vec2 query);

public:
	DynamicObstacle(WaterHeightMap outMap);
	static int instance_counter;
	int Obstacle_id;
	bool isSelfDriven = false;
	bool canMoveXZ = false;
	bool releaseWavefront = false;
	bool produceWavefront = false;
	Rigidbody* rigidbody=NULL;
	instance inst;
	std::vector<glm::vec2> immersionOutLine;
	std::vector<glm::vec2> edgeNormals;
	std::vector<glm::vec2> vertexNormals;
	std::pair<int, int> anchorPathLeft;
	std::pair<int, int> anchorPathRight;
	WaterHeightMap map;
	void UpdateMap(Wavefront& wavefront,WFHeightMap& wfmap,float time);
	Wavefront* wavefrontLeft=NULL;
	Wavefront* wavefrontRight=NULL;
	glm::vec3 previousLinearMomentum=glm::vec3(0.0f);
	glm::vec3 lastEffectiveLinearMomentum = glm::vec3(0.0f);
	glm::vec3 releaseComparisonMomentum = glm::vec3(0.0f);
	void UpdateImmersionOutLine();
	void UpdateNormals();
	Rigidbody* ConstructRigidbody();
	void UpdateAnchorPaths();
	void PropagateWavefront();
	void CheckWavefrontRefreshStatus();
	void Update(Wavefront& w);
	void Render();
	float renderingHeight = 0.0f;
	float oldRenderingHeight = 0.0f;
};
