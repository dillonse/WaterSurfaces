#include "Boat.h"


void Boat::Init()
{
	obstacle.inst = mainBody;
}

void Boat::Render()
{
	//inst.transform = glm::mat4(1.0f);
	//inst.transform = rigidbody->orientation;
	//inst.transform[3] = glm::vec4(rigidbody->position + glm::vec3(0.0f, 0.0f, 0.0f), 1.0f);
	mainBody.transform = glm::mat4(0.0f);
	mainBody.transform = obstacle.rigidbody->orientation;
	mainBody.transform[3] = glm::vec4(obstacle.rigidbody->position + glm::vec3(0.0f, 0.0f, 0.0f), 1.0f);
	otherInst.transform = mainBody.transform;
	//obstacle.rigidbody->Render(*mainBody.mat,true);
	//mainBody.RenderTriangles();
	otherInst.RenderTriangles();
}
