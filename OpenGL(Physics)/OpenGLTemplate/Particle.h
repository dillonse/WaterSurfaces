#pragma once
#include <glm\vec3.hpp>
typedef struct Particle {
	Particle(glm::vec3 position, glm::vec3 velocity, float mass,float lifetime) :position(position),initialPosition(position),velocity(velocity),initialVelocity(velocity),mass(mass),lifetime(lifetime),initalLifetime(lifetime) {};
	glm::vec3 initialPosition;
	glm::vec3 initialVelocity;
	float initalLifetime;
	glm::vec3 position;
	glm::vec3 velocity;
	glm::vec3 force_accumulator;
	float lifetime;
	float mass;
}Particle;