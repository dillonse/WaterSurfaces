#include "Dampers.h"
#include <glm/gtc/functions.hpp>
#include <iostream>
float DampingCoefficients::linearValue(float time) {
	if (time > startTime&&time < endTime) {
		if (time < middle) {
			float t = 1 - ((middle - time) / (middle - startTime));
			return cuttoffValue*(1 - t) + peakValue*(t);
		}
		else {
			float t = 1 - ((endTime - time) / (endTime - middle));
			return cuttoffValue*(t) + peakValue*(1-t);
		}
	}
	return cuttoffValue;
}

float DampingCoefficients::expValue(float time) {
	if (time > startTime&&time < endTime) {
		float expBegin = glm::log(startTime+1);
		float expEnd = glm::log(endTime+1);
		float expMiddle = glm::log(middle+1);
		if (time < middle) {
			float t = 1 - ((middle - time) / (middle - startTime));
			float exp= expBegin*(1 - t)*beginValue + expMiddle*(t)*peakValue;
			return glm::exp(exp);
		}
		else {
			float t = 1 - ((endTime - time) / (endTime - middle));
			float exp= expMiddle*(t)*peakValue + expEnd*(1-t)*cuttoffValue;
			return glm::exp(exp);
		}
	}
	return cuttoffValue;
}