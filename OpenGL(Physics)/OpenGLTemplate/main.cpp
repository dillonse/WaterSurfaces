
//fan model from https://www.cgtrader.com/free-3d-models/exterior/industrial/industrial-fan

#include <Windows.h>
#include "debug.h"
#include <cstdio>
#include <cstdlib>
#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <random>
#include <GL/glew.h>
#include <GL/glut.h>
#include <glm/glm.hpp>
#include<glm/gtc/matrix_transform.hpp>
#include <glm/gtx/string_cast.hpp>
#include <glm/gtx/matrix_decompose.hpp>
#include <glm/gtc/constants.hpp>
#include "textfile.h"
#include "AuxilaryIO.h"
#include "VBO.h"
#include "textures.h"
#include "shaders.h"
#include "model.h"
#include "instance.h"
#include <string>
#include <tuple>
#include <algorithm>
#include "global.h"
#include "MathUtilities.h"
//Physics
#include "Rigidbody.h"
#include "RigidbodySystem.h"
#include "ParticleSystem.h"
#include "Contact.h"
#include "VolumeModel.h"
#include "SurfaceModel.h"
#include "WavefrontSystem.h"
#include "StaticObstacle.h"
#include "DynamicObstacle.h"
#include "Boat.h"

//particle systems
ParticleSystem particleSystem;
std::vector<instance> colliders;
//rigidbodies
RigidbodySystem rigidbodySystem;
Rigidbody* rigidbody;


//models
model skybox_mod;
model volume_mod;
model particle_mod;
model object_mod;
model ocean_mod;
model box_obstacle_mod;
model bounding_sphere_mod;
model bounding_box_mod;
model crate_mod;
model boat_wood_mod;
model boat_else_mod;
model cargo_mod;
model port_mod;
model port_obst1_mod;
model port_obst2_mod;
//materials
material skybox_mat;
material volume_mat;
material particle_mat;
material object_mat;
material color_mat;
material ocean_mat;
material box_obstacle_mat;
material crate_mat;
material sand_mat;
material boat_wood_mat;
material boat_else_mat;
material cargo_mat;
material port_mat;
//volume model
VolumeModel volumeModel;
//surface model
SurfaceModel surfaceModel;
//wavefront model
WavefrontSystem wavefrontSystem(glm::ivec2(100, 100), glm::vec2(2, 2), glm::ivec2(400, 400), glm::vec2(0.5f,0.5f));
//demo variables

//static obstacles
StaticObstacle box_obstacle;
DynamicObstacle dynamic_box_obstacle(wavefrontSystem.WMap);

//instances
instance skybox_inst;
instance volume_inst;
instance particle_inst;
instance object_inst;
instance box_obstacle_inst;
instance crate_inst;
instance boat_wood_inst;
instance boat_else_inst;
instance cargo_inst;
instance port_inst;

int cameraRotationDirection = 1;
float cameraRotationSpeed = 50.0f;
int cameraZoomDirection = 1;
float cameraZoomSpeed=50.0f;

bool pause = true;
bool show_heatmaps = false;

float lightSpeed = 0.05f;
glm::vec3 lightMovementAmplitude(5,15,15);
float sphereRotationSpeed = 50;

int numParticles = 5000;
float fan_strength_left = 0.0f;
float fan_strength_right = 0.0f;
float lifetime=100;

float avgSize = 2;

glm::vec3 rotationVector = glm::vec3(0, 0, 0);

int wall_height= 2;
int wall_width = 2;
float explosionForce = 1000.0f;
float fragment_distance = 0.5f;
glm::vec3 explosionPos = glm::vec3(-0.25f,-0.25f,1.0f);

instance collider_cube;
instance  collidee_cube;

glm::vec3 floor_normal = glm::vec3(0, 1, 0);

float navigationSpeed = 1.0f;
float simulationSpeed = 0.1f;

bool gravityOn = false;

float restit_coeff = 0.5;

//heatmap data

GLfloat * Amplitude_data;
GLfloat * AngFreq_data;
GLfloat * ArrTime_data;
GLfloat * Height_data;

void printtext(int x, int y, std::string String)
{	
	//(x,y) is from the bottom left of the window
	glDisable(GL_DEPTH_TEST);
	glUseProgram(0);
	//font_mat.BindTextures();
	//font_mat.BindValues();
	//glm::mat4 projection = glm::ortho(-1, 1, -1, 1);
	//GLuint PLocation = glGetUniformLocation(font_mat.ShaderID, "P");

	//glUniformMatrix4fv(PLocation, 1, GL_FALSE,&projection[0][0]);
	glm::mat4 inverseV = glm::inverse(camera::ViewMatrix);
	glm::mat4 inverseP = glm::inverse(camera::ProjectionMatrix);
	glm::vec3 text_pos = glm::vec4(0.5, 0.5, -1, 0); //inverseV*inverseP*glm::vec4(1,1,-1,1);
//	glLoadIdentity();
//	std::cout << glm::to_string(text_pos) << std::endl;
	//glRasterPos2f(text_pos.x,text_pos.y);
	glRasterPos3f(text_pos.x, text_pos.y,-1);
	for (int i = 0; i<String.size(); i++)
	{
		glutBitmapCharacter(GLUT_BITMAP_9_BY_15, String[i]);
	}
}




void initPhysics() {
	grid::offset = wavefrontSystem.WMap.offset;
	grid::size = wavefrontSystem.WMap.size;
	volumeModel=VolumeModel(volume_inst,36,1.0f,1.0f);
	surfaceModel = SurfaceModel(volumeModel);
	particle_inst.transform = glm::scale(glm::mat4(1.0f), glm::vec3(0.1, 0.1, 0.1));
	particleSystem.particleInstance = particle_inst;
	particleSystem.unaryForces.push_back(new GravityForce());
	object_inst.mod = new model(object_mod);
	object_inst.mat = &object_mat;
	//rigidbody = new Rigidbody(object_inst, glm::vec3(5, 5, 5), object_inst, object_inst);
	//rigidbodySystem.rigidBodies.push_back(*rigidbody);
	//rigidbodySystem.unaryForces.push_back(new GravityForce());
	rigidbodySystem.UpdateGeometry(time::deltaTime);
	//wavefront system initialisation
	Wavefront wavefront;
	int wave_num=1;
	float vert_num = 40;
	float interval = 360 / vert_num;  
	glm::vec2 offset = glm::vec2(0.0f);
	glm::vec2 global_offset = glm::vec2(0, 0);

	/*
	for (int i = 0; i < vert_num; i++) {
		wavefront.vertices_bufferA.push_back(Wavefront_vertex(glm::vec3(glm::sin(glm::radians(interval*(float)i)), 0, glm::cos(glm::radians(interval*(float)i))) + offset + global_offset , glm::vec3(glm::sin(glm::radians(interval*(float)i))*wave_speed, 0, glm::cos(glm::radians(interval*(float)i))*wave_speed)));
	}
	wavefront.wavenum = wave_num;
	wavefrontSystem.wavefronts.push_back(wavefront);*/  
	offset = glm::vec2(150, 150);
	wavefront.VWrite.clear();
	wave_num = 50;
	for (int i = 0; i < vert_num; i++) {
		//if(i>10&&i<13)
		//wavefront.vertices_bufferA.push_back(Wavefront_vertex(glm::vec2(glm::sin(glm::radians(interval*(float)i)), glm::cos(glm::radians(interval*(float)i))) + offset + global_offset,glm::vec2(glm::sin(glm::radians(interval*(float)i)), glm::cos(glm::radians(interval*(float)i)))));
		wavefront.VWrite.push_back(Wavefront_vertex(glm::vec2(i+1,i+1)*10.0f,(glm::vec2(1.0f,-1.0f))));
	}
	//wavefront.vertices_bufferA.push_back(Wavefront_vertex(glm::vec2(glm::sin(glm::radians(interval*(float)0)), glm::cos(glm::radians(interval*(float)0))) + offset + global_offset, glm::vec2(1.0f))); // glm::vec2(glm::sin(glm::radians(interval*(float)0)), glm::cos(glm::radians(interval*(float)0)))));
	//wavefront.wavenum = wave_num;
	wavefront.angular_frequency = 100.0f;
	wavefrontSystem.wavefronts.push_back(wavefront);
	offset = glm::vec2(90, 90);
	wavefront.VWrite.clear();
	for (int i = 0; i < vert_num; i++) {
		wavefront.VWrite.push_back(Wavefront_vertex(glm::vec2(i - 1, i - 1)*10.0f, (glm::vec2(-1.0f, 1.0f))));
		//wavefront.VWrite.push_back(Wavefront_vertex(glm::vec2((i) * 10, i * 10),(glm::vec2(1.0f, -1.0f))));
		//wavefront.VWrite.push_back(Wavefront_vertex(glm::vec2(i, wavefrontSystem.WMap.size.x - i)*wavefrontSystem.WMap.offset.x*2.0f - glm::vec2(0.1, 0.1), (glm::vec2(1.0f, 1.0f))));
	}
	wavefront.angular_frequency = 100.0f;
	wavefrontSystem.wavefronts.push_back(wavefront);
	//init the heightmap of the wavefront system
	for (int i = 0; i < wavefrontSystem.TMap.size.x; i++) {
		for (int j = 0; j < wavefrontSystem.TMap.size.y; j++) {
			//wavefrontSystem.TMap.heights[i][j] = -1*((float)i / (float)wavefrontSystem.TMap.size.x)*1* ((float)j / (float)wavefrontSystem.TMap.size.y);
		}
	}
	boat_else_inst.transform = glm::translate(glm::mat4(1.0f), glm::vec3(10, 0, 10));
	boat_wood_inst.transform = glm::translate(glm::mat4(1.0f), glm::vec3(10, 0, 10));
	wavefrontSystem.boat.otherInst = boat_wood_inst;
	wavefrontSystem.boat.mainBody = boat_else_inst;
	wavefrontSystem.boat.Init();
	wavefrontSystem.boat.obstacle.inst.transform;
	wavefrontSystem.boat.obstacle.immersionOutLine.clear();
	wavefrontSystem.boat.obstacle.UpdateImmersionOutLine();
	wavefrontSystem.boat.obstacle.UpdateNormals();
	int num_of_obstacles = 10;
	for (int i = 0; i < num_of_obstacles; i++) {
		//glm::vec2 offset  = glm::vec2(MathUtilities::normalizedNoise(10.0f),MathUtilities::normalizedNoise(10.0f));
		//offset *= 10.0f;
		
		//box_obstacle.inst.transform = glm::scale(glm::mat4(1.0f), glm::vec3(20, 1, 1));
		//box_obstacle_inst.transform = glm::translate(glm::mat4(1.0f), glm::vec3(i*offset.x, 0, i*offset.y));
		box_obstacle.inst.transform = glm::rotate(glm::mat4(1.0f), glm::radians(45.0f), glm::vec3(0, 1, 0))*box_obstacle.inst.transform;
		box_obstacle.inst.transform = glm::translate(glm::mat4(1.0f), glm::vec3((14)*offset.x, 0, (7)*offset.y)+float(i)*glm::vec3(10,0,10))*box_obstacle.inst.transform;
		//box_obstacle.inst.transform = glm::scale(glm::mat4(1.0f), glm::vec3(10, 1, 10))*box_obstacle.inst.transform;
		//box_obstacle.immersionOutLine.clear();
		//box_obstacle.UpdateImmersionOutLine();
		dynamic_box_obstacle.inst.transform = glm::translate(glm::mat4(1.0f), float(i)*glm::vec3(10, 0, 10)+MathUtilities::normalizedNoise(10)*glm::vec3(0,0,1));
		dynamic_box_obstacle.immersionOutLine.clear();
		dynamic_box_obstacle.UpdateImmersionOutLine();
		dynamic_box_obstacle.UpdateNormals();
		//wavefrontSystem.staticObstacles.push_back(box_obstacle);
		wavefrontSystem.dynamicObstacles.push_back(dynamic_box_obstacle);
	}
	wavefrontSystem.Init();
	//rigidbody init of flowing object
	Rigidbody* rigidbody = wavefrontSystem.boat.obstacle.ConstructRigidbody();
	rigidbodySystem.rigidBodies.push_back(*rigidbody);
	wavefrontSystem.boat.obstacle.rigidbody = &rigidbodySystem.rigidBodies[0];
	rigidbodySystem.UpdateGeometry(time::deltaTime);
	for (int i = 0; i < wavefrontSystem.dynamicObstacles.size(); i++) {
		Rigidbody* rigidbody =
			wavefrontSystem.dynamicObstacles[i].ConstructRigidbody();
		rigidbodySystem.rigidBodies.push_back(*rigidbody);
		rigidbodySystem.UpdateGeometry(time::deltaTime);
	}
	for (int i = 0; i < wavefrontSystem.dynamicObstacles.size(); i++) {
		wavefrontSystem.dynamicObstacles[i].rigidbody = &rigidbodySystem.rigidBodies[i + 1];
	}
	wavefrontSystem.boat.obstacle.rigidbody = &rigidbodySystem.rigidBodies[0];
	wavefrontSystem.boat.obstacle.rigidbody->mass = 100.0f;
	wavefrontSystem.boat.obstacle.rigidbody->updateTorqueFromForces = false;
	StaticObstacle portObst;
	portObst.inst.transform = port_inst.transform;
	portObst.inst.mod = &port_obst1_mod;
	portObst.UpdateImmersionOutLine();
	wavefrontSystem.staticObstacles.push_back(portObst);
	portObst.inst.mod = &port_obst2_mod;
	portObst.immersionOutLine.clear();
	portObst.UpdateImmersionOutLine();
	wavefrontSystem.staticObstacles.push_back(portObst);
	///////
	Amplitude_data = new GLfloat[3 * wavefrontSystem.WFMap.size.x *wavefrontSystem.WFMap.size.y];
	AngFreq_data = new  GLfloat[3 * wavefrontSystem.WFMap.size.x *wavefrontSystem.WFMap.size.y];
	ArrTime_data = new  GLfloat[3 * wavefrontSystem.WFMap.size.x *wavefrontSystem.WFMap.size.y];
	Height_data = new  GLfloat[3 * wavefrontSystem.TMap.size.x *wavefrontSystem.TMap.size.y];
	for (int i = 0; i < 3 * wavefrontSystem.WFMap.size.x *wavefrontSystem.WFMap.size.y; i++) {
		Amplitude_data[i] = 0.0f;
		AngFreq_data[i] = 0.25f;
		Height_data[i] = 0.5f;
		ArrTime_data[i] = 0.75f;
	}
	ocean_mod.vertices.resize(6 * wavefrontSystem.WFMap.size.x*wavefrontSystem.WFMap.size.y);
	ocean_mod.normals.resize(6 * wavefrontSystem.WFMap.size.x*wavefrontSystem.WFMap.size.y);
}

void updatePhysics() {
	time::currentFrameTime = glutGet(GLUT_ELAPSED_TIME);
	time::pastDeltaTime = time::deltaTime;
	time::deltaTime = ((float)(time::currentFrameTime - time::previousFrameTime)) / 1000.0;
	//time::deltaTime *= 0.5f;
	time::previousFrameTime = time::currentFrameTime;
	rendering::uvAdvocationFactor += time::deltaTime;
	if (!pause) {
	/*	volumeModel.updateColumnVolumes();
	//	surfaceModel.updateHeights();
		surfaceModel.updateExternalPressures();
		surfaceModel.updateVerticalVelocities();
		surfaceModel.updateHorizontalVelocities();
		surfaceModel.updateParticleSystem(particleSystem);
		surfaceModel.updateCollisions(rigidbodySystem);
		std::vector<instance> inst;
		particleSystem.Update(0.01f,inst);
		rigidbodySystem.UpdateForcesTorque(time::deltaTime);
		rigidbodySystem.UpdateMomentums(time::deltaTime);
		rigidbodySystem.UpdateAuxilary(time::deltaTime);
		rigidbodySystem.UpdateGeometry(time::deltaTime);
		rigidbodySystem.UpdateBoundingVolumes(time::deltaTime);*/
		
		//update the heightmaps
		for (int i = 0; i < wavefrontSystem.dynamicObstacles.size(); i++) {
			//wavefrontSystem.dynamicObstacles[i].inst.transform = glm::translate(glm::mat4(1.0f), 10.0f*glm::vec3(0, 0, -0.001))*
				//wavefrontSystem.dynamicObstacles[i].inst.transform;
		}

		wavefrontSystem.Update();
		rigidbodySystem.UpdateForcesTorque(time::deltaTime);
		rigidbodySystem.UpdateMomentums(time::deltaTime);
		rigidbodySystem.UpdateAuxilary(time::deltaTime);
		rigidbodySystem.UpdateGeometry(time::deltaTime);
		rigidbodySystem.UpdateBoundingVolumes(time::deltaTime);
		float maxAmp = 0.0f;
		float maxAngFreq = 0.0f;
		float maxArrTime = 0.0f;
		float maxHeight = 0.0f;
		for (int i = 0; i < wavefrontSystem.WFMap.size.x; i++) {
			for (int j = 0; j < wavefrontSystem.WFMap.size.y; j++) {
				float amp = 0.0f;
				float angfreq = 0.f;
				float arrtime = 0.0f;
				float height = 0.0f;
				for(int k=0;k<wavefrontSystem.WFMap.nodes[i][j].records.size();k++){
					amp += wavefrontSystem.WFMap.nodes[i][j].records[k].amplitude;
					angfreq += wavefrontSystem.WFMap.nodes[i][j].records[k].angular_frequency;
					arrtime += wavefrontSystem.WFMap.nodes[i][j].records[k].travel_time;
				}
				if (amp > maxAmp) {
					maxAmp = amp;
				}
				if (angfreq > maxAngFreq) {
					maxAngFreq = angfreq;
				}
				if (arrtime > maxArrTime) {
					maxArrTime = arrtime;
				}
				height = glm::abs(wavefrontSystem.TMap.nodes[i][j].height);
				if (height > maxHeight) {
					maxHeight = height;
				}
			}
		}
		//static bool print = false;
		for (int i = 0; i < wavefrontSystem.WFMap.size.x; i++) {
			for (int j = 0; j < wavefrontSystem.WFMap.size.y; j++) {
				//if (!print&&i<2)
				//	std::cout << "indices " << (i*wavefrontSystem.WFMap.size.y + j) * 3 << std::endl;
				float amp = 0.0f;
				float angfreq = 0.0f;
				float arrtime = 0.0f;
				float height = 0.0f;
				if (wavefrontSystem.WFMap.nodes[i][j].records.size() > 0) {
					amp = wavefrontSystem.WFMap.nodes[i][j].records[0].amplitude;
					Amplitude_data[(i*wavefrontSystem.WFMap.size.y + j) * 3]= amp / maxAmp;
					Amplitude_data[(i*wavefrontSystem.WFMap.size.y + j) * 3 + 1]= amp / maxAmp;
					Amplitude_data[(i*wavefrontSystem.WFMap.size.y + j) * 3 + 2]= 0.0f;
				
					angfreq = wavefrontSystem.WFMap.nodes[i][j].records[0].angular_frequency;
					AngFreq_data[(i*wavefrontSystem.WFMap.size.y + j) * 3] = 0.0f;
					AngFreq_data[(i*wavefrontSystem.WFMap.size.y + j) * 3 + 1] = 0.0f;
					AngFreq_data[(i*wavefrontSystem.WFMap.size.y + j) * 3 + 2] = angfreq / maxAngFreq;

					arrtime = wavefrontSystem.WFMap.nodes[i][j].records[0].travel_time;
					ArrTime_data[(i*wavefrontSystem.WFMap.size.y + j) * 3] =arrtime / maxArrTime;
					ArrTime_data[(i*wavefrontSystem.WFMap.size.y + j) * 3 + 1] = 0.0f;
					ArrTime_data[(i*wavefrontSystem.WFMap.size.y + j) * 3 + 2] = 0.0f;
					
				}
				else {
				/*	Amplitude_data[(i*wavefrontSystem.WFMap.size.y + j) * 3] = 0.0f;
					Amplitude_data[(i*wavefrontSystem.WFMap.size.y + j) * 3 + 1] = 1.0f;
					Amplitude_data[(i*wavefrontSystem.WFMap.size.y + j) * 3 + 2] = 0.0f;

					AngFreq_data[(i*wavefrontSystem.WFMap.size.y + j) * 3] = 0.0f;
					AngFreq_data[(i*wavefrontSystem.WFMap.size.y + j) * 3 + 1] = 1.0f;
					AngFreq_data[(i*wavefrontSystem.WFMap.size.y + j) * 3 + 2] = 0.0f;

					ArrTime_data[(i*wavefrontSystem.WFMap.size.y + j) * 3] = 0.0f;
					ArrTime_data[(i*wavefrontSystem.WFMap.size.y + j) * 3 + 1] = 1.0f;
					ArrTime_data[(i*wavefrontSystem.WFMap.size.y + j) * 3 + 2] = 0.0f;*/
				}
				height = glm::abs(wavefrontSystem.TMap.nodes[i][j].height);
				glm::vec2 refractionNormal = (wavefrontSystem.TMap.nodes[i][j].refractionNormal);
				if(refractionNormal!=glm::vec2(0.0f))
					refractionNormal = glm::normalize(refractionNormal);
				float contourIndex = (wavefrontSystem.TMap.nodes[i][j].contourIndex);
			/*	Height_data[(i*wavefrontSystem.WFMap.size.y + j) * 3] = 0.0f;
				Height_data[(i*wavefrontSystem.WFMap.size.y + j) * 3 + 1] = height / maxHeight;
				Height_data[(i*wavefrontSystem.WFMap.size.y + j) * 3 + 2] = 0.0f;*/
				if (refractionNormal == glm::vec2(0, 1)) {
					Height_data[(i*wavefrontSystem.WFMap.size.y + j) * 3] = 1.0f;
						Height_data[(i*wavefrontSystem.WFMap.size.y + j) * 3 + 1] = 0;
					Height_data[(i*wavefrontSystem.WFMap.size.y + j) * 3 + 2] = 0;
				}
				else if (refractionNormal == glm::vec2(1, 0)) {
					Height_data[(i*wavefrontSystem.WFMap.size.y + j) * 3] = 0.0f;
					Height_data[(i*wavefrontSystem.WFMap.size.y + j) * 3 + 1] = 1.0f;
					Height_data[(i*wavefrontSystem.WFMap.size.y + j) * 3 + 2] = 0;
				}
				else if (refractionNormal.x>0&&refractionNormal.y>0){ //== glm::normalize(glm::vec2(1, 1))) {
					Height_data[(i*wavefrontSystem.WFMap.size.y + j) * 3] = 0.0f;
					Height_data[(i*wavefrontSystem.WFMap.size.y + j) * 3 + 1] = 0.0f;
					Height_data[(i*wavefrontSystem.WFMap.size.y + j) * 3 + 2] = 1.0;
				}
				else if (refractionNormal == glm::vec2(0, -1)) {
					Height_data[(i*wavefrontSystem.WFMap.size.y + j) * 3] = 0.0f;
					Height_data[(i*wavefrontSystem.WFMap.size.y + j) * 3 + 1] = 1.0f;
					Height_data[(i*wavefrontSystem.WFMap.size.y + j) * 3 + 2] = 1.0f;
				}
				else if (refractionNormal == glm::vec2(-1, 0)) {
					Height_data[(i*wavefrontSystem.WFMap.size.y + j) * 3] = 1.0f;
					Height_data[(i*wavefrontSystem.WFMap.size.y + j) * 3 + 1] = 0.0f;
					Height_data[(i*wavefrontSystem.WFMap.size.y + j) * 3 + 2] = 1.0f;
				}
				else if (refractionNormal.x<0&&refractionNormal.y>0){// == glm::normalize(glm::vec2(-1, 1))) {
					Height_data[(i*wavefrontSystem.WFMap.size.y + j) * 3] = 1.0f;
					Height_data[(i*wavefrontSystem.WFMap.size.y + j) * 3 + 1] = 1.0f;
					Height_data[(i*wavefrontSystem.WFMap.size.y + j) * 3 + 2] = 0.0f;
				}
				else if (refractionNormal.x>0&&refractionNormal.y<0){// glm::normalize(glm::vec2(1, -1))) {
					Height_data[(i*wavefrontSystem.WFMap.size.y + j) * 3] = 0.5f;
					Height_data[(i*wavefrontSystem.WFMap.size.y + j) * 3 + 1] = 0.5f;
					Height_data[(i*wavefrontSystem.WFMap.size.y + j) * 3 + 2] = 0.5f;
				}
				else if (refractionNormal.x<0&&refractionNormal.y<0){// glm::normalize(glm::vec2(-1, -1))) {
					Height_data[(i*wavefrontSystem.WFMap.size.y + j) * 3] = 0.5f;
					Height_data[(i*wavefrontSystem.WFMap.size.y + j) * 3 + 1] = 0.0f;
					Height_data[(i*wavefrontSystem.WFMap.size.y + j) * 3 + 2] = 0.5f;
				}
				else if (refractionNormal == glm::vec2(0, 0)) {
					Height_data[(i*wavefrontSystem.WFMap.size.y + j) * 3] = 1.0f;
					Height_data[(i*wavefrontSystem.WFMap.size.y + j) * 3 + 1] = 1.0f;
					Height_data[(i*wavefrontSystem.WFMap.size.y + j) * 3 + 2] = 1.0f;
				}
				else {
					Height_data[(i*wavefrontSystem.WFMap.size.y + j) * 3] = 0.0f;
					Height_data[(i*wavefrontSystem.WFMap.size.y + j) * 3 + 1] = 0.0f;
					Height_data[(i*wavefrontSystem.WFMap.size.y + j) * 3 + 2] = 0.0f;
				}
				//Height_data[(i*wavefrontSystem.WFMap.size.y + j) * 3] = //refractionNormal.x);//0.5+0.5*refractionNormal.x; //contourIndex / 5.0f;//0.0f
				//Height_data[(i*wavefrontSystem.WFMap.size.y + j) * 3 + 1] = glm::abs(refractionNormal.y);//0.5 + 0.5*refractionNormal.y;//0.0f; // height / maxHeight;
				//Height_data[(i*wavefrontSystem.WFMap.size.y + j) * 3 + 2] = 0.0f;
			}
		}
		//if (!print) {
		//	print = true;
		//}
		//message += "\n"+std::to_string(volumeModel.columns[0][0].volume)+"\n";
		//message += std::to_string(volumeModel.columns[1][0].volume) + "\n";
		//message += std::to_string(volumeModel.columns[2][0].volume) + "\n";
		//volumeModel.printVolumes();
	}
}

void init() {
	//enable backface culling
	glFrontFace(GL_CCW);
	glCullFace(GL_BACK);
	glEnable(GL_CULL_FACE);
	//set depth and transparency
	glEnable(GL_DEPTH_TEST);
	glClearColor(1.0, 1.0, 1.0, 0.0);
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	//set transformations
	camera::ViewMatrix = glm::lookAt(   // View matrix
		glm::vec3(0, 0, 0), // Camera is at (4,3,3), in World Space
		glm::vec3(0, 0, 1), // and looks at the origin
		glm::vec3(0, 1, 0)  // Head is up (set to 0,-1,0 to look upside-down)
	);



	//lights::light0.position = glm::vec3(15, 2, 16);
	//lights::light0.BindShadowMap();
}

void changeSize(int w, int h) {
	//return;
	std::cout << "w,h" << w << "," << h << std::endl;
	// Prevent a divide by zero, when window is too short
	// (you cant make a window of zero width).
	if (h == 0)
		h = 1;
	camera::ratio = 1.0* w / h;
	// Reset the coordinate system before modifying
	// Set the viewport to be the entire window
	glViewport(0, 0, w, h);
	// Set the correct perspective.
	camera::ProjectionMatrix = glm::perspective(glm::radians(camera::FOV), camera::ratio, 0.1f, 1000.0f); //Projection matrix
	viewport::width = w;
	viewport::height = h;
}


void handleControls() {
	if (controls::Key_A || controls::Key_D) {
		camera::ViewMatrix = glm::translate(camera::ViewMatrix, -camera::position);
		camera::ViewMatrix = glm::rotate(camera::ViewMatrix, glm::radians(cameraRotationDirection*time::deltaTime*navigationSpeed*cameraRotationSpeed), glm::vec3(0, 1, 0));
		camera::ViewMatrix = glm::translate(camera::ViewMatrix, camera::position);

		camera::ViewMatrixNoTranslation = glm::rotate(camera::ViewMatrixNoTranslation, glm::radians(cameraRotationDirection*time::deltaTime*navigationSpeed*cameraRotationSpeed), glm::vec3(0, 1, 0));
	}
	if (controls::Key_L) {
		camera::position += glm::vec3(time::deltaTime*navigationSpeed, 0, 0);
		camera::ViewMatrix = glm::translate(camera::ViewMatrix, glm::vec3(time::deltaTime*navigationSpeed, 0, 0));
	}
	if (controls::Key_J) {
		camera::position += glm::vec3(-time::deltaTime*navigationSpeed, 0, 0);
		camera::ViewMatrix = glm::translate(camera::ViewMatrix, glm::vec3(-time::deltaTime*navigationSpeed, 0, 0));
	}
	if (controls::Key_I) {
		camera::position += glm::vec3(0, 0, time::deltaTime*navigationSpeed);
		camera::ViewMatrix = glm::translate(camera::ViewMatrix, glm::vec3(0, 0, time::deltaTime*navigationSpeed));
	}
	if (controls::Key_K) {
		camera::position += glm::vec3(0, 0, -time::deltaTime*navigationSpeed);
		camera::ViewMatrix = glm::translate(camera::ViewMatrix, glm::vec3(0, 0, -time::deltaTime*navigationSpeed));
	}
	if (controls::Key_U) {
		camera::position += glm::vec3(0, time::deltaTime*navigationSpeed, 0);
		camera::ViewMatrix = glm::translate(camera::ViewMatrix, glm::vec3(0, time::deltaTime*navigationSpeed, 0));
	}
	if (controls::Key_O) {
		camera::position += glm::vec3(0, -time::deltaTime*navigationSpeed, 0);
		camera::ViewMatrix = glm::translate(camera::ViewMatrix, glm::vec3(0, -time::deltaTime*navigationSpeed, 0));
	}
	if (controls::Key_W || controls::Key_S) {
		camera::FOV += time::deltaTime*navigationSpeed*cameraZoomDirection*cameraZoomSpeed;
		camera::ProjectionMatrix = glm::perspective(glm::radians(camera::FOV), camera::ratio, 0.1f, 1000.0f); //Projection matrix
	}
	if (controls::Key_1) {
		surfaceModel.forces[0][0] = 100.0f;
	}
	else {
		surfaceModel.forces[0][0] = 0.0f;
	}
	
}


void ControlBoat() {
	wavefrontSystem.boat.obstacle.isSelfDriven = false;
	static float engine_speed = 0.0f;
	static float engine_acceleration = 1000.0f;
	static float engine_rolloff = 500.0f;
	static float engine_max_speed = 1000.0f;
	static float engine_max_rev_speed = -25.0f;
	static float street_control = 1.0f;
	static float max_lin_mom = 500.0f;
	static float max_ang_mom = 200.0f;
	bool engine_used = false;
	if (controls::Key_8) {
		//if(!controls::Key_4&&!controls::Key_6)
		engine_speed += time::deltaTime*engine_acceleration;
		engine_speed = glm::clamp(engine_speed, engine_max_rev_speed, engine_max_speed);
		//wavefrontSystem.dynamicObstacles[0].rigidbody->linearMomentum += glm::vec3(1, 0, 0);
		//float prev = glm::length(wavefrontSystem.boat.obstacle.rigidbody->linearMomentum);
		if (wavefrontSystem.boat.obstacle.canMoveXZ) {
			glm::vec3 offset = wavefrontSystem.boat.obstacle.rigidbody->orientation[2];
			if (!controls::Key_6&&controls::Key_4) {
				wavefrontSystem.boat.obstacle.rigidbody->netTorque *= 0.99;
			}
			if (glm::length(wavefrontSystem.boat.obstacle.rigidbody->linearMomentum) < max_lin_mom) {
				wavefrontSystem.boat.obstacle.rigidbody->netForce += wavefrontSystem.boat.obstacle.rigidbody->orientation[2] * engine_speed;//glm::vec3(10, 0, 0);
			}
			wavefrontSystem.boat.obstacle.isSelfDriven = true;
			engine_used = true;
		}
		//float next = glm::length(wavefrontSystem.boat.obstacle.rigidbody->linearMomentum);
		//std::cout << "sign is:" << glm::sign(prev - next) << std::endl;
		//wavefrontSystem.boat.obstacle.rigidbody->angularMomentum = glm::vec3(0.0f);

	}
	if (controls::Key_2) {
		engine_speed -= time::deltaTime*engine_acceleration;
		engine_speed = glm::clamp(engine_speed, engine_max_rev_speed, engine_max_speed);
		//wavefrontSystem.dynamicObstacles[0].rigidbody->linearMomentum += glm::vec3(1, 0, 0);
		wavefrontSystem.boat.obstacle.rigidbody->netForce += glm::vec3(1, 0, 1)*-wavefrontSystem.boat.obstacle.rigidbody->orientation[2] * engine_speed;
		wavefrontSystem.boat.obstacle.isSelfDriven = true;
		engine_used = true;
		//wavefrontSystem.boat.obstacle.rigidbody->angularMomentum = glm::vec3(0.0f);
	}
	if (controls::Key_4) {
		//wavefrontSystem.dynamicObstacles[0].rigidbody->linearMomentum += glm::vec3(1, 0, 0);
		//wavefrontSystem.boat.obstacle.rigidbody->angularMomentum = wavefrontSystem.boat.obstacle.rigidbody->orientation[0] * engine_speed*street_control;
		//wavefrontSystem.boat.obstacle.rigidbody->forces[0] += wavefrontSystem.boat.obstacle.rigidbody->orientation[0] * engine_speed*street_control;
		if (wavefrontSystem.boat.obstacle.canMoveXZ) {
			//std::cout << "net torque was " << glm::to_string(wavefrontSystem.boat.obstacle.rigidbody->netTorque) << std::endl;
			if (glm::length(wavefrontSystem.boat.obstacle.rigidbody->netTorque) < max_ang_mom) {
				wavefrontSystem.boat.obstacle.rigidbody->netTorque += wavefrontSystem.boat.obstacle.rigidbody->orientation[2] * engine_speed*street_control;
			}
			//wavefrontSystem.boat.obstacle.isSelfDriven = true;
			//std::cout << "net torque is " << glm::to_string(wavefrontSystem.boat.obstacle.rigidbody->netTorque) << std::endl;
		}
		//engine_used = true;
	}
	if (controls::Key_6) {
		//wavefrontSystem.dynamicObstacles[0].rigidbody->linearMomentum += glm::vec3(1, 0, 0);
		//wavefrontSystem.boat.obstacle.rigidbody->angularMomentum = wavefrontSystem.boat.obstacle.rigidbody->orientation[0] * engine_speed*street_control;
		//wavefrontSystem.boat.obstacle.rigidbody->forces[0] += -wavefrontSystem.boat.obstacle.rigidbody->orientation[0] * engine_speed*street_control;
		if (wavefrontSystem.boat.obstacle.canMoveXZ) {
			//std::cout << "net torque was " << glm::to_string(wavefrontSystem.boat.obstacle.rigidbody->netTorque) << std::endl;
			if (glm::length(wavefrontSystem.boat.obstacle.rigidbody->netTorque) < max_ang_mom) {
				wavefrontSystem.boat.obstacle.rigidbody->netTorque += -wavefrontSystem.boat.obstacle.rigidbody->orientation[2] * engine_speed*street_control;
			}
			//wavefrontSystem.boat.obstacle.isSelfDriven = true;
			//std::cout << "net torque is " << glm::to_string(wavefrontSystem.boat.obstacle.rigidbody->netTorque) << std::endl;
		}
		
		//engine_used = true;
	}
	//std::cout << "is self driven" << wavefrontSystem.boat.obstacle.isSelfDriven << std::endl;
	if (!engine_used) {
		float prev_speed = engine_speed;
		engine_speed -= glm::sign(engine_speed)*engine_rolloff*time::deltaTime;
		if (glm::sign(prev_speed)*glm::sign(engine_speed) == -1.0f) {
			engine_speed = 0.0f;
		}
		//std::cout << "speed was" << prev_speed << " now is " << engine_speed << std::endl;
		//wavefrontSystem.boat.obstacle.rigidbody->netTorque *= 0.99f;
	}
}

std::ofstream performance("D:/performance.txt");
#include "windows.h"
#include "psapi.h"
std::string VMEM() {
	PROCESS_MEMORY_COUNTERS_EX pmc;
	GetProcessMemoryInfo(GetCurrentProcess(), reinterpret_cast<PPROCESS_MEMORY_COUNTERS>(&pmc), sizeof(pmc));
	SIZE_T virtualMemUsedByMe = pmc.PrivateUsage;
	return std::to_string(virtualMemUsedByMe);
}

std::string RAMMEM() {
	PROCESS_MEMORY_COUNTERS_EX pmc;
	GetProcessMemoryInfo(GetCurrentProcess(), reinterpret_cast<PPROCESS_MEMORY_COUNTERS>(&pmc), sizeof(pmc));
	SIZE_T virtualMemUsedByMe = pmc.PrivateUsage;
	SIZE_T physMemUsedByMe = pmc.WorkingSetSize;
	return std::to_string(physMemUsedByMe);
}

void renderScene(void) {
	//glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	static float viewOffsety = 2.0f;
	static float viewOffsetx = 0.0f;
	static float viewOffsetz = -1.0f;
	float t0= glutGet(GLUT_ELAPSED_TIME);
	//performance << t0 << ",";
	if (controls::Key_plus) {
		if (controls::Key_X) {
			viewOffsetx += time::deltaTime;
		}
		else if (controls::Key_Y) {
			viewOffsety += time::deltaTime;
		}
		else if (controls::Key_Z) {
			viewOffsetz += time::deltaTime;
		}
	}
	else if (controls::Key_minus) {
		if (controls::Key_X) {
			viewOffsetx -= time::deltaTime;
		}
		else if (controls::Key_Y) {
			viewOffsety -= time::deltaTime;
		}
		else if (controls::Key_Z) {
			viewOffsetz -= time::deltaTime;
		}
	}
	//static glm::vec3 previousRotation = wavefrontSystem.boat.mainBody.transform
	
	//camera::ViewMatrix = glm::inverse(wavefrontSystem.boat.mainBody.transform)*glm::translate(glm::mat4(1.0f), glm::vec3(1,1,1));
	//wavefrontSystem.boat.mainBody.
	if(!pause)
		message.clear();
	handleControls();
	float t1 = glutGet(GLUT_ELAPSED_TIME);
	//performance << t1 << ",";
	updatePhysics();
	float t2 = glutGet(GLUT_ELAPSED_TIME);
	performance << t2-t1 << ",";
	ControlBoat();
	glm::mat4 filteredMatrix = wavefrontSystem.boat.obstacle.rigidbody->orientation;
	//std::cout << "height " << wavefrontSystem.boat.obstacle.rigidbody->position.y << std::endl;
	filteredMatrix[3] = glm::vec4(wavefrontSystem.boat.obstacle.rigidbody->position, 1.0f);
	//filteredMatrix[3] = glm::vec4(wavefrontSystem.boat.obstacle.rigidbody->position,1.0f);
	camera::ViewMatrix = glm::inverse(filteredMatrix*glm::translate(glm::mat4(1.0f), glm::vec3(viewOffsetx, viewOffsety, viewOffsetz))*glm::rotate(glm::mat4(1.0f), glm::radians(180.0f), glm::vec3(0, 1, 0)));//*glm::translate(glm::mat4(1.0f),glm::vec3(0.0f,-1.0f,0.0f));//glm::translate(glm::mat4(1.0f), glm::vec3(wavefrontSystem.boat.obstacle.rigidbody->position+glm::vec3(0,2,0))));
	camera::ViewMatrixNoTranslation = camera::ViewMatrix;
	camera::ViewMatrixNoTranslation[3] = glm::vec4(0, 0, 0, 1);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	//heatmaps
	if (show_heatmaps) {
		glUseProgram(NULL);

		GLint iViewport[4];
		glGetIntegerv(GL_VIEWPORT, iViewport);
		glm::vec2 scaleCoeff = glm::vec2(wavefrontSystem.WFMap.size.y*2.0f, wavefrontSystem.WFMap.size.x*2.0f);
		glPixelZoom(iViewport[2] / scaleCoeff.x, iViewport[3] / scaleCoeff.y);

		glRasterPos2i(-1, 0);
		glDrawPixels(wavefrontSystem.WFMap.size.y, wavefrontSystem.WFMap.size.x, GL_RGB, GL_FLOAT, Amplitude_data);

		glRasterPos2i(-1, -1);
		glDrawPixels(wavefrontSystem.WFMap.size.y, wavefrontSystem.WFMap.size.x, GL_RGB, GL_FLOAT, ArrTime_data);

		glRasterPos2i(0, 0);
		glDrawPixels(wavefrontSystem.WFMap.size.y, wavefrontSystem.WFMap.size.x, GL_RGB, GL_FLOAT, AngFreq_data);

		glRasterPos2i(0, -1);
		glDrawPixels(wavefrontSystem.WFMap.size.y, wavefrontSystem.WFMap.size.x, GL_RGB, GL_FLOAT, Height_data);

		//glRasterPos2i(0, 1);
		//GLint iViewport[4];
		//glGetIntegerv(GL_VIEWPORT, iViewport);
		//glm::vec2 scaleCoeff = glm::vec2(wavefrontSystem.WFMap.size.x*2.0f, wavefrontSystem.WFMap.size.y*2.0f);
		//glPixelZoom(iViewport[2] / scaleCoeff.x, iViewport[3] / scaleCoeff.y);
		//glDrawPixels(wavefrontSystem.WFMap.size.x, wavefrontSystem.WFMap.size.y, GL_RGB, GL_FLOAT, AngFreq_data);
	}
	else {
		float t3 = glutGet(GLUT_ELAPSED_TIME);
		//performance << t3 << ",";
		//scene
		//render the skybox
		glDepthMask(GL_FALSE);
		skybox_inst.RenderTriangles();
		glDepthMask(GL_TRUE);
		glPolygonMode(GL_FRONT, GL_LINE);
		glPolygonMode(GL_FRONT, GL_FILL);
		float t4 = glutGet(GLUT_ELAPSED_TIME);
		performance << t4-t3 << ",";
		wavefrontSystem.Render(ocean_mat,ocean_mod,color_mat,sand_mat);
		float t5 = glutGet(GLUT_ELAPSED_TIME);
		performance << t5-t4 << ",";
		cargo_inst.RenderTriangles();
		port_inst.RenderTriangles();
		float t6 = glutGet(GLUT_ELAPSED_TIME);
		performance << t6-t5 << ",";
		//boat.Render();
		//boat_wood_inst.RenderTriangles();
		//boat_else_inst.RenderTriangles();
		
		//glPolygonMode(GL_FRONT, GL_FILL);
	
	}
	//printScreen();
	float t7 = glutGet(GLUT_ELAPSED_TIME);
	performance << t7 - t0 << ",";
	performance << VMEM()<<","<<RAMMEM()<< std::endl;
	glutSwapBuffers();
}

void changeSkybox(std::string skyboxPath) {
	std::vector<std::string> skyboxPaths;
	skyboxPaths.push_back(std::string(skyboxPath).append("posx.jpg"));
	skyboxPaths.push_back(std::string(skyboxPath).append("negx.jpg"));
	skyboxPaths.push_back(std::string(skyboxPath).append("posy.jpg"));
	skyboxPaths.push_back(std::string(skyboxPath).append("negy.jpg"));
	skyboxPaths.push_back(std::string(skyboxPath).append("posz.jpg"));
	skyboxPaths.push_back(std::string(skyboxPath).append("negz.jpg"));
	skybox_mat.SkyboxMapID = textures::loadCubemap(skyboxPaths);

}

void processNormalKeys(unsigned char key, int x, int y) {
	std::string skyboxPath;
	std::vector<std::string> skyboxPaths;
	static float zoom = 45.0;
	
	//gravityOn = !gravityOn;
	GravityForce* newGravity = NULL;
	switch (key)
	{
	case ' ':
		pause = !pause;
		std::cout << "pause is " << std::to_string(pause) << std::endl;
		break;
	case 27:
		exit(0);
	case 'h':
		show_heatmaps = !show_heatmaps;
		break;
	case 'a':
		controls::Key_A = true;
		cameraRotationDirection = -1;
		break;
	case 'd':
		controls::Key_D = true;
		cameraRotationDirection = 1;
		break;
	case 'w':
		controls::Key_W = true;
		cameraZoomDirection = 1;
		break;
	case 's':
		controls::Key_S = true;
		cameraZoomDirection = -1;
		break;
	case 'i':
		controls::Key_I = true;
		break;
	case 'j':
		controls::Key_J = true;
		break;
	case 'k':
		controls::Key_K = true;
		break;
	case 'l':
		controls::Key_L = true;
		break;
	case 'o':
		controls::Key_O = true;
		break;
	case 'u':
		controls::Key_U = true;
		break;
	case 'x':
		controls::Key_X = true;
		break;
	case 'y':
		controls::Key_Y = true;
		break;
	case 'z':
		controls::Key_Z = true;
		break;
	case '0':
		controls::Key_0 = true;
		break;
	case '2':
		controls::Key_2 = true;
		break;
	case '4':
		controls::Key_4 = true;
		break;
	case '5':
		controls::Key_5 = true;
		break;
	case '6':
		controls::Key_6 = true;
		break;
	case '8':
		controls::Key_8 = true;
		break;
	case '7':
		controls::Key_7 = true;
		break;
	case '9':
		controls::Key_9 = true;
		break;
	case '1':
		controls::Key_1 = true;
		break;
	case '3':
		controls::Key_3 = true;
		break;
	case '+':
		controls::Key_plus = true;
		break;
	case '-':
		controls::Key_minus = true;
		break;
	case 'g':
		gravityOn = !gravityOn;
		newGravity = new GravityForce();
		if (!gravityOn) {
			newGravity->gravityAcceleration = glm::vec3(0.0f);
		}
		else {
			newGravity->gravityAcceleration = glm::vec3(0, -1, 0)*9.8f;
		}
		delete rigidbodySystem.unaryForces[5];
		rigidbodySystem.unaryForces[5] = newGravity;
		controls::Key_G = true;
		break;
	case '*':
		controls::Key_times = true;
		break;
	case '/':
		controls::Key_divide = true;
		break;
	default:
		break;
	}
	glutPostRedisplay();
}


void ProcessNormalKeysUp(unsigned char key, int x, int y) {
	switch (key) {
	case 'a':
		controls::Key_A = false;
		break;
	case 'd':
		controls::Key_D = false;
		break;
	case 'w':
		controls::Key_W = false;
		break;
	case 's':
		controls::Key_S = false;
		break;
	case 'i':
		controls::Key_I = false;
		break;
	case 'j':
		controls::Key_J = false;
		break;
	case 'k':
		controls::Key_K = false;
		break;
	case 'l':
		controls::Key_L = false;
		break;
	case 'o':
		controls::Key_O = false;
		break;
	case 'u' :
		controls::Key_U = false;
		break;
	case 'x':
		controls::Key_X = false;
		break;
	case 'y':
		controls::Key_Y = false;
		break;
	case 'z':
		controls::Key_Z = false;
		break;
	case '0':
		controls::Key_0 = false;
		break;
	case '2':
		controls::Key_2 = false;
		break;
	case '4':
		controls::Key_4 = false;
		break;
	case '5':
		controls::Key_5 = false;
		break;
	case '6':
		controls::Key_6 = false;
		break;
	case '8':
		controls::Key_8 = false;
		break;
	case '7':
		controls::Key_7 = false;
		break;
	case '9':
		controls::Key_9 = false;
		break;
	case '1':
		controls::Key_1 = false;
		break;
	case '3':
		controls::Key_3 = false;
		break;
	case '+':
		controls::Key_plus = false;
		break;
	case '-':
		controls::Key_minus = false;
		break;
	case 'g':
		controls::Key_G = false;
		break;
	case '*':
		controls::Key_times = false;
		break;
	case '/':
		controls::Key_divide = false;
		break;
	}

}

void processSpecialKeys(int key, int x, int y) {
	switch (key)
	{
	case GLUT_KEY_RIGHT:
		controls::Key_Right = true;
		//chamber_mat.choice++;
		break;
	case GLUT_KEY_LEFT:
		controls::Key_Left = true;
		//chamber_mat.choice--;
		break;
	default:
		break;
	}
	glutPostRedisplay();
}

void processSpecialKeysUp(int key, int x, int y) {
	switch (key)
	{
	case GLUT_KEY_RIGHT:
		controls::Key_Right = false;
		break;
	case GLUT_KEY_LEFT:
		controls::Key_Left = false;
		break;
	default:
		break;
	}
}

void processMouseMotion(int x, int y) {
	//return;
	static int previousX = 0;
	static int previousY = 0;
	glm::vec3 offset=glm::vec3(0,0,0);
	previousX = controls::MousePositionX;
	previousY = controls::MousePositionY;
	if (controls::MouseRight||controls::MouseLeft) {
		//calculate the x interval and the y interval
		int deltaX = x - previousX;
		int deltaY = y - previousY;
		int epsilon = 10;
		if (controls::MouseRight) {
			if (deltaX > epsilon) {
			}
			else if (deltaX < epsilon) {
			}
			if (deltaY > epsilon) {
			}
			else if (deltaY < epsilon) {
			}
		}
		else {
			if (deltaX > epsilon) {
			}
			else if (deltaX < epsilon) {
			}
			if (deltaY > epsilon) {
			}
			else if (deltaY < epsilon) {
			}
		}
	}
	controls::MousePositionX = x;
	controls::MousePositionY = y;
}

void ProcessMousePassiveMotion(int x, int y) {
	controls::MousePositionX = x;
	controls::MousePositionY = y;
}

void ProcessMouseWheel(int wheel, int direction, int x, int y) {
	float wheelspeed = 10.0f;
	if (direction != 0) {
	}
}

void processMouseClick(int button, int state, int x, int y) {
	if (button == GLUT_LEFT_BUTTON) {
		if (state == GLUT_DOWN)
			controls::MouseLeft = true;
		else if (state == GLUT_UP)
			controls::MouseLeft = false;
	}
	if (button == GLUT_RIGHT_BUTTON) {
		if (state == GLUT_DOWN)
			controls::MouseRight = true;
		else if (state == GLUT_UP)
			controls::MouseRight = false;
	}
}


void loadShaders() {
	//create a list of the shader paths
	std::vector<std::tuple<std::string, std::string,std::string>> shaderPaths;
	std::vector<GLuint> shaderIDs;
	shaderPaths.push_back(std::make_tuple(
		"D:/WaterSurfaces/OpenGL(Physics)/OpenGLTemplate/shaders/DebugShader/debug.vert",
		"D:/WaterSurfaces/OpenGL(Physics)/OpenGLTemplate/shaders/DebugShader/debug.frag", ""));
		//"D:/WaterSurfaces/OpenGL(Physics)/OpenGLTemplate/shaders/DebugShader/debug.geom"));
	shaderPaths.push_back(std::make_tuple(
		"D:/WaterSurfaces/OpenGL(Physics)/OpenGLTemplate/shaders/SkyboxShader/skybox.vert",
		"D:/WaterSurfaces/OpenGL(Physics)/OpenGLTemplate/shaders/SkyboxShader/skybox.frag",""));
	shaderPaths.push_back(std::make_tuple(
		"D:/WaterSurfaces/OpenGL(Physics)/OpenGLTemplate/shaders/NormalMapShader/normalMap.vert",
		"D:/WaterSurfaces/OpenGL(Physics)/OpenGLTemplate/shaders/NormalMapShader/normalMap.frag",""));
	shaderPaths.push_back(std::make_tuple(
		"D:/WaterSurfaces/OpenGL(Physics)/OpenGLTemplate/shaders/FresnelShader/fresnel.vert",
		"D:/WaterSurfaces/OpenGL(Physics)/OpenGLTemplate/shaders/FresnelShader/fresnel.frag",
		"D:/WaterSurfaces/OpenGL(Physics)/OpenGLTemplate/shaders/FresnelShader/fresnel.geom"));
	shaderPaths.push_back(std::make_tuple(
		"D:/WaterSurfaces/OpenGL(Physics)/OpenGLTemplate/shaders/TextureShader/texture.vert",
		"D:/WaterSurfaces/OpenGL(Physics)/OpenGLTemplate/shaders/TextureShader/texture.frag",""));
	shaderPaths.push_back(std::make_tuple(
		"D:/WaterSurfaces/OpenGL(Physics)/OpenGLTemplate/shaders/TextureNormalMapShader/texnormal.vert",
		"D:/WaterSurfaces/OpenGL(Physics)/OpenGLTemplate/shaders/TextureNormalMapShader/texnormal.frag",""));
	shaderPaths.push_back(std::make_tuple(
		"D:/WaterSurfaces/OpenGL(Physics)/OpenGLTemplate/shaders/ReflectionShader/reflection.vert",
		"D:/WaterSurfaces/OpenGL(Physics)/OpenGLTemplate/shaders/ReflectionShader/reflection.frag",""));
	shaderPaths.push_back(std::make_tuple(
		"D:/WaterSurfaces/OpenGL(Physics)/OpenGLTemplate/shaders/RefractionShader/refraction.vert",
		"D:/WaterSurfaces/OpenGL(Physics)/OpenGLTemplate/shaders/RefractionShader/refraction.frag",""));
	shaderPaths.push_back(std::make_tuple(
		"D:/WaterSurfaces/OpenGL(Physics)/OpenGLTemplate/shaders/ReflectionNormalMapShader/reflnormal.vert",
		"D:/WaterSurfaces/OpenGL(Physics)/OpenGLTemplate/shaders/ReflectionNormalMapShader/reflnormal.frag",""));
	shaderPaths.push_back(std::make_tuple(
		"D:/WaterSurfaces/OpenGL(Physics)/OpenGLTemplate/shaders/RefractionNormalMapShader/refrnormal.vert",
		"D:/WaterSurfaces/OpenGL(Physics)/OpenGLTemplate/shaders/RefractionNormalMapShader/refrnormal.frag", ""));
	shaderPaths.push_back(std::make_tuple(
		"D:/WaterSurfaces/OpenGL(Physics)/OpenGLTemplate/shaders/FresnelNormalMapShader/fresrnormal.vert",
		"D:/WaterSurfaces/OpenGL(Physics)/OpenGLTemplate/shaders/FresnelNormalMapShader/fresrnormal.frag", ""));
	shaderPaths.push_back(std::make_tuple(
		"D:/WaterSurfaces/OpenGL(Physics)/OpenGLTemplate/shaders/DepthMapShader/depth.vert",
		"D:/WaterSurfaces/OpenGL(Physics)/OpenGLTemplate/shaders/DepthMapShader/depth.frag", ""));
	shaderPaths.push_back(std::make_tuple(
		"D:/WaterSurfaces/OpenGL(Physics)/OpenGLTemplate/shaders/DepthMapShader/debugDepthMap.vert",
		"D:/WaterSurfaces/OpenGL(Physics)/OpenGLTemplate/shaders/DepthMapShader/debugDepthMap.frag", ""));
	shaderPaths.push_back(std::make_tuple(
		"D:/WaterSurfaces/OpenGL(Physics)/OpenGLTemplate/shaders/ShadowReceiverShader/shadow.vert",
		"D:/WaterSurfaces/OpenGL(Physics)/OpenGLTemplate/shaders/ShadowReceiverShader/shadow.frag", ""));
	shaderPaths.push_back(std::make_tuple(
		"D:/WaterSurfaces/OpenGL(Physics)/OpenGLTemplate/shaders/TextureNormalMapLightShader/texnormallight.vert",
		"D:/WaterSurfaces/OpenGL(Physics)/OpenGLTemplate/shaders/TextureNormalMapLightShader/texnormallight.frag", ""));
	shaderPaths.push_back(std::make_tuple(
		"D:/WaterSurfaces/OpenGL(Physics)/OpenGLTemplate/shaders/CollidersShader/collider.vert",
		"D:/WaterSurfaces/OpenGL(Physics)/OpenGLTemplate/shaders/CollidersShader/collider.frag", ""));
	shaderPaths.push_back(std::make_tuple(
		"D:/PhysicsSplashingFluids/OpenGL(Physics)/OpenGLTemplate/shaders/ParticleShaderColors/particleColor.vert",
		"D:/PhysicsSplashingFluids/OpenGL(Physics)/OpenGLTemplate/shaders/ParticleShaderColors/particleColor.frag", ""));
	shaderPaths.push_back(std::make_tuple(
		"D:/WaterSurfaces/OpenGL(Physics)/OpenGLTemplate/shaders/FeatureHighlightShader/fhigh.vert",
		"D:/WaterSurfaces/OpenGL(Physics)/OpenGLTemplate/shaders/FeatureHighlightShader/fhigh.frag", ""));
	shaderPaths.push_back(std::make_tuple(
		"D:/WaterSurfaces/OpenGL(Physics)/OpenGLTemplate/shaders/FontShader/font.vert",
		"D:/WaterSurfaces/OpenGL(Physics)/OpenGLTemplate/shaders/FontShader/font.frag", ""));

	//load the shaders
	shaders::loadShaders(shaderPaths, shaderIDs);
	//assign to global variables
	skybox_mat.ShaderID = shaderIDs[1];
	volume_mat.ShaderID = shaderIDs[4];
	volume_mat.color = glm::vec4(1, 0, 0, 0);
	particle_mat.ShaderID = shaderIDs[16];
	object_mat.ShaderID = shaderIDs[0];
	color_mat.ShaderID = shaderIDs[0];
	color_mat.color = glm::vec4(1, 0, 0, 1);
	ocean_mat.ShaderID = shaderIDs[3];
	ocean_mat.color = glm::vec4(0, 0, 1, 1);
	ocean_mat.AlphaValue = 0.4f;
	box_obstacle_mat.ShaderID = shaderIDs[0];
	crate_mat.ShaderID = shaderIDs[4];
	sand_mat.ShaderID = shaderIDs[4];
	boat_wood_mat.ShaderID = shaderIDs[5];
	boat_else_mat.ShaderID = shaderIDs[5];
	//particle_mat.color = glm::vec4(0, 0,1.0f, 1);
	cargo_mat.ShaderID = shaderIDs[5];
	port_mat.ShaderID = shaderIDs[4];
}

void loadTexture() {
	//load the skybox maps
	std::string skyboxPath = "D:/WaterSurfaces/OpenGL(Physics)/OpenGLTemplate/textures/TropicalSunnyDay/";
	std::vector<std::string> skyboxPaths;
	skyboxPaths.push_back(std::string(skyboxPath).append("posx.jpg"));
	skyboxPaths.push_back(std::string(skyboxPath).append("negx.jpg"));
	skyboxPaths.push_back(std::string(skyboxPath).append("posy.jpg"));
	skyboxPaths.push_back(std::string(skyboxPath).append("negy.jpg"));
	skyboxPaths.push_back(std::string(skyboxPath).append("posz.jpg"));
	skyboxPaths.push_back(std::string(skyboxPath).append("negz.jpg"));
	skybox_mat.SkyboxMapID = textures::loadCubemap(skyboxPaths);
	ocean_mat.SkyboxMapID = textures::loadCubemap(skyboxPaths);
	ocean_mat.DiffuseMapID = textures::loadTextureGrayScale("D:/WaterSurfaces/OpenGL(Physics)/OpenGLTemplate/textures/Heightmap.png");
	//load the volume diff map
	ocean_mat.NormalMapID = textures::loadTextureRGB("D:/WaterSurfaces/OpenGL(Physics)/OpenGLTemplate/textures/wave.png");
	volume_mat.DiffuseMapID = textures::loadTextureRGB("D:/PhysicsSplashingFluids/OpenGL(Physics)/OpenGLTemplate/textures/water.jpg");
	crate_mat.DiffuseMapID = textures::loadTextureRGB("D:/WaterSurfaces/OpenGL(Physics)/OpenGLTemplate/textures/crate_1.png");
	sand_mat.DiffuseMapID = textures::loadTextureRGB("D:/WaterSurfaces/OpenGL(Physics)/OpenGLTemplate/textures/sand.jpg");
	boat_wood_mat.DiffuseMapID = textures::loadTextureRGB("D:/WaterSurfaces/OpenGL(Physics)/OpenGLTemplate/textures/bakedTexture.png");
	boat_wood_mat.NormalMapID = textures::loadTextureRGB("D:/WaterSurfaces/OpenGL(Physics)/OpenGLTemplate/textures/bakedTextureNormal.png");
	boat_else_mat.DiffuseMapID = textures::loadTextureRGB("D:/WaterSurfaces/OpenGL(Physics)/OpenGLTemplate/textures/Fabric1.jpg");
	boat_else_mat.NormalMapID = textures::loadTextureRGB("D:/WaterSurfaces/OpenGL(Physics)/OpenGLTemplate/textures/fabric_normal.png");
	boat_else_mat.SpecularMapID = textures::loadTextureRGB("D:/WaterSurfaces/OpenGL(Physics)/OpenGLTemplate/textures/boatRest.png");
	cargo_mat.DiffuseMapID = textures::loadTextureRGB("D:/WaterSurfaces/OpenGL(Physics)/OpenGLTemplate/textures/cargoShip.png");
	cargo_mat.NormalMapID = textures::loadTextureRGB("D:/WaterSurfaces/OpenGL(Physics)/OpenGLTemplate/textures/cargoNormal.png");
	port_mat.DiffuseMapID = textures::loadTextureRGB("D:/WaterSurfaces/OpenGL(Physics)/OpenGLTemplate/textures/frontend-large.jpg");
	if (boat_else_mat.DiffuseMapID == -1) {
		exit(-420);
	}
	if (boat_else_mat.NormalMapID == -1) {
		exit(-430);
	}
	if (boat_else_mat.SpecularMapID == -1) {
		exit(-440);
	}
}

void loadObject() {
	//load the skybox cube
	if (!loadOBJ("C:/Users/dillonse/Documents/Visual Studio 2015/Projects/OpenGL(Physics)/OpenGLTemplate/models/skybox.obj", skybox_mod.vertices, skybox_mod.normals, skybox_mod.UVs))
		exit(-1);
	skybox_mod.flipFaceOrientation();
	buffers::Load(skybox_mod);
	//load the volume model
	if (!loadOBJ("D:/PhysicsSplashingFluids/OpenGL(Physics)/OpenGLTemplate/models/volumeCube.obj", volume_mod.vertices, volume_mod.normals, volume_mod.UVs))
		exit(-1);
	buffers::Load(volume_mod);
	//load the particle model
	if (!loadOBJ("D:/PhysicsSplashingFluids/OpenGL(Physics)/OpenGLTemplate/models/demo_ball.obj", particle_mod.vertices, particle_mod.normals, particle_mod.UVs));
	buffers::Load(particle_mod);
	//load the object model
	if (!loadOBJ("D:/PhysicsSplashingFluids/OpenGL(Physics)/OpenGLTemplate/models/demo_object.obj", object_mod.vertices, object_mod.normals, object_mod.UVs));
	buffers::Load(object_mod);

	if (!loadOBJ("D:/WaterSurfaces/OpenGL(Physics)/OpenGLTemplate/models/cube_with_UVs.obj", box_obstacle_mod.vertices, box_obstacle_mod.normals, box_obstacle_mod.UVs))
		exit(-1);
	box_obstacle_mod.flipFaceOrientation();
	buffers::Load(box_obstacle_mod);

	if (!loadOBJ("D:/WaterSurfaces/OpenGL(Physics)/OpenGLTemplate/models/Crate2.obj", crate_mod.vertices, crate_mod.normals, crate_mod.UVs)) {
		exit(-1);
	}
	buffers::Load(crate_mod);
	if (!loadOBJ("D:/WaterSurfaces/OpenGL(Physics)/OpenGLTemplate/models/unified.obj", boat_wood_mod.vertices, boat_wood_mod.normals, boat_wood_mod.UVs)) {
		exit(-1);
	}
	boat_wood_mod.computeTangentBasis();
	buffers::Load(boat_wood_mod);
	if (!loadOBJ("D:/WaterSurfaces/OpenGL(Physics)/OpenGLTemplate/models/boatElse.obj", boat_else_mod.vertices, boat_else_mod.normals, boat_else_mod.UVs)) {
		exit(-1);
	}
	//boat_else_mod.flipFaceOrientation();
	boat_else_mod.computeTangentBasis();
	buffers::Load(boat_else_mod);
	if (!loadOBJ("D:/WaterSurfaces/OpenGL(Physics)/OpenGLTemplate/models/CargoShip2.obj", cargo_mod.vertices, cargo_mod.normals, cargo_mod.UVs)) {
		exit(-1);
	}
	cargo_mod.computeTangentBasis();
	buffers::Load(cargo_mod);
	if (!loadOBJ("D:/WaterSurfaces/OpenGL(Physics)/OpenGLTemplate/models/PortBase.obj", port_mod.vertices, port_mod.normals, port_mod.UVs)) {
		exit(-1);
	}
	buffers::Load(port_mod);
	if (!loadOBJ("D:/WaterSurfaces/OpenGL(Physics)/OpenGLTemplate/models/PortBaseObst1.obj", port_obst1_mod.vertices, port_obst1_mod.normals, port_obst1_mod.UVs)) {
		exit(-1);
	}
	if (!loadOBJ("D:/WaterSurfaces/OpenGL(Physics)/OpenGLTemplate/models/PortBaseObst2.obj", port_obst2_mod.vertices, port_obst2_mod.normals, port_obst2_mod.UVs)) {
		exit(-1);
	}
}



int main(int argc, char **argv) {
	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_DEPTH | GLUT_DOUBLE | GLUT_RGBA);
	glutInitWindowPosition(0, 0);
	glutInitWindowSize(1200, 800);
	glutCreateWindow("GLSL Example");

	glutDisplayFunc(renderScene);
	glutIdleFunc(renderScene);
	glutReshapeFunc(changeSize);
	glutKeyboardFunc(processNormalKeys);
	glutKeyboardUpFunc(ProcessNormalKeysUp);
	glutSpecialFunc(processSpecialKeys);
	glutSpecialUpFunc(processSpecialKeysUp);
	glutMotionFunc(processMouseMotion);
	glutMouseFunc(processMouseClick);
	glutPassiveMotionFunc(ProcessMousePassiveMotion);
	glutMouseWheelFunc(ProcessMouseWheel);
	glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);
	glewInit();
	init();

	if (glewIsSupported("GL_VERSION_4_5"))
		printf("Ready for OpenGL 4.5\n");
	else {
		printf("OpenGL 4.5 not supported\n");
		//exit(1);
	}
	//load the auxilary data to memory and pass them to GPU
	std::cout << "loading shaders" << std::endl;
	loadShaders();
	loadObject();
	loadTexture();
	//connect the materials and models to instances
	///skybox
	skybox_inst.mod = &skybox_mod;
	skybox_inst.mat = &skybox_mat;
	///volume model
	volume_inst.mod = &volume_mod;
	volume_inst.mat = &volume_mat;
	///particle model
	particle_inst.mod = &particle_mod;
	particle_inst.mat = &particle_mat;
	camera::ViewMatrix = glm::rotate(camera::ViewMatrix, 79.0f, glm::vec3(0, 1, 0));

	box_obstacle_inst.mod = &box_obstacle_mod;
	box_obstacle_inst.mat = &box_obstacle_mat;
	box_obstacle_mat.color = glm::vec4(0, 1, 1, 1);
	//box_obstacle_inst.transform = glm::scale(glm::mat4(1.0), glm::vec3(10, 10,10));
	box_obstacle_inst.transform = glm::rotate(glm::mat4(1.0f), glm::radians(55.0f), glm::vec3(0.0f, 1.0f, 0.0f));
	box_obstacle_inst.transform = glm::translate(glm::mat4(1.0f), glm::vec3(11,0,10))*box_obstacle_inst.transform;
	box_obstacle.inst = box_obstacle_inst;
	//box_obstacle.UpdateImmersionOutLine();

	dynamic_box_obstacle.inst = box_obstacle.inst;
	crate_inst.mat = &crate_mat;
	crate_inst.mod = &crate_mod;
	dynamic_box_obstacle.inst = crate_inst;
	
	boat_wood_inst.mod = &boat_wood_mod;
	boat_wood_inst.mat = &boat_wood_mat;

	boat_else_inst.mod = &boat_else_mod;
	boat_else_inst.mat = &boat_else_mat;
	//dynamic_box_obstacle.UpdateImmersionOutLine();
	cargo_inst.mat = &cargo_mat;
	cargo_inst.mod = &cargo_mod;
	
	cargo_inst.transform = glm::translate(glm::mat4(1.0f), glm::vec3(2*65,20,2*65))*glm::rotate(glm::mat4(1.0f),glm::radians(45.0f),glm::vec3(0,1,0));
	

	port_inst.mat = &port_mat;
	port_inst.mod = &port_mod;
	port_inst.transform = glm::translate(glm::mat4(1.0f), glm::vec3(120, 25, 80))*glm::rotate(glm::mat4(1.0f),glm::radians(90.0f),glm::vec3(0,1,0));
	initPhysics();
	glutMainLoop();

	return 0;
}

