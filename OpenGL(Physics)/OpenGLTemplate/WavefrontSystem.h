#pragma once
#include "Wavefront.h"
#include <vector>
#include "WFHeightmap.h"
#include "TerrainHeightMap.h"
#include "WaterHeightMap.h"
#include "StaticObstacle.h"
#include "DynamicObstacle.h"
#include "Boat.h"
//#define DELTA_TIME 0.01f

class WavefrontSystem {
public:
	WavefrontSystem(glm::ivec2 interactive_size,glm::vec2 interactive_offset,glm::ivec2 size, glm::vec2 offset):
		WFMap(interactive_size, interactive_offset),TMap(interactive_size, interactive_offset),WMap(size,offset),boat(WMap){ ; };
	Boat boat;
	std::vector<Wavefront> wavefronts;
	std::vector<StaticObstacle> staticObstacles;
	std::vector<DynamicObstacle> dynamicObstacles;
	std::vector<std::vector<float>> heights;
	std::vector<std::vector<glm::vec3>> normals;
	std::vector<GLfloat> renderHeights;
	std::vector<glm::vec2> renderDirections;
	std::vector<float> renderSpeeds;
	WFHeightMap WFMap;
	WaterHeightMap WMap;
	TerrainHeightMap TMap;
	void Init();
	void Render(material& mat,model& mod,material& debug_mat,material& sand_mat);
	void Update();
	void UpdateFlowForces(WaterHeightMapHeightParameters& params);
	void UpdateMaps(Wavefront& wavefront);
	float time = 0.0f;
};

