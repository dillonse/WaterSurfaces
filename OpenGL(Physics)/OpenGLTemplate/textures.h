#pragma once
#include<GL/freeglut.h>
#include <string>
#include <vector>
#include <glm/common.hpp>

namespace textures {
	GLuint loadCubemap(std::vector<std::string> face);
	GLuint loadTextureRGB(std::string path);
	GLuint loadTextureGrayScale(std::string path);
	void updateWaterHeights(std::vector<GLfloat>& heights, GLuint textureID);
	GLuint loadTexture(std::vector<GLfloat>& image, GLuint imageTexture1);
	GLuint loadTexture(std::vector<glm::vec2>& image, GLuint imageTexture1);
};