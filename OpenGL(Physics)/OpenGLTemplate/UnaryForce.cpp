#include "UnaryForce.h"
#include<glm\geometric.hpp>
#include <algorithm>
#include <iostream>
#include <glm\gtx\string_cast.hpp>
glm::vec3 GravityForce::computeForce(Particle& p) {
   return p.mass*gravityAcceleration;
}

glm::vec3 GravityForce::computeForce(Rigidbody& r) {
	for (int i = 0; i < r.geometry.mod->uniqueIndices.size(); i++) {
		r.forces[i] += 0.0f;//Gravity acts only through COM
	}
	return r.mass*gravityAcceleration;
}

glm::vec3 DragForce::computeForce(Particle& p) {
	return -DragCoefficient*p.velocity;
}

glm::vec3 DragForce::computeForce(Rigidbody& r) {
	glm::vec3 velocity = r.linearMomentum / r.mass;
	glm::vec3 angularVelocity = glm::inverse(r.inertiaTensor)*r.angularMomentum;
	r.netTorque += -angularVelocity*DragCoefficient*1.0f;
	return -DragCoefficient*(velocity);
}

glm::vec3 FanForce::computeForce(Particle& p) {
	//find the distance between fan and particle
	float distance = std::max(glm::distance(p.position, position), 0.01f);
	return normal*strength/distance;
}

glm::vec3 FanForce::computeForce(Rigidbody& r) {
	for (int i = 0; i < r.geometry.mod->uniqueIndices.size(); i++) {
		float distance = std::max(glm::distance(r.geometry.mod->vertices[r.geometry.mod->uniqueIndices[i]], position),0.01f);

		r.forces[i] += normal*strength*0.01f/distance;
	}
	float distance = std::max(glm::distance(r.position, position), 0.01f);
	return normal*strength / distance;
}

glm::vec3 RepulsiveForce::computeForce(Particle& p) {
	//calculate the vector between the particle and the point of the force
	glm::vec3 diff = p.position - position;
	glm::vec3 direction = glm::normalize(diff);
	float distance = glm::length(diff);
	return direction*strength/(distance+0.01f);
}

glm::vec3 RepulsiveForce::computeForce(Rigidbody& r) {
	for (int i = 0; i < r.geometry.mod->uniqueIndices.size(); i++) {
		glm::vec3 diff = r.geometry.mod->vertices[r.geometry.mod->uniqueIndices[i]] - position;
		glm::vec3 direction = glm::normalize(diff);
		float distance = glm::length(diff);
		r.forces[i] += direction*strength / (distance*r.mass);
	}
	glm::vec3 diff = r.position - position;
	glm::vec3 direction = glm::normalize(diff);
	float distance = glm::length(diff);
	return direction*strength / (distance*r.mass);
}

glm::vec3 ImpulsiveForce::computeForce(Particle& p) {
	//calculate the vector between the particle and the point of the force
	glm::vec3 diff = p.position - position;
	glm::vec3 direction = -glm::normalize(diff);
	float distance = glm::length(diff);
	return direction*strength / (distance*p.mass + 0.01f);
}

glm::vec3 ImpulsiveForce::computeForce(Rigidbody& r) {
	for (int i = 0; i < r.geometry.mod->uniqueIndices.size(); i++) {
		glm::vec3 diff = r.geometry.mod->vertices[r.geometry.mod->uniqueIndices[i]] - position;
		glm::vec3 direction = -glm::normalize(diff);
		float distance = glm::length(diff);
		r.forces[i] += direction*strength / (distance*r.mass+1.0f);
	}
	glm::vec3 diff = r.position - position;
	glm::vec3 direction = -glm::normalize(diff);
	float distance = glm::length(diff);
	return direction*strength / (distance*r.mass+1.0f);
}
