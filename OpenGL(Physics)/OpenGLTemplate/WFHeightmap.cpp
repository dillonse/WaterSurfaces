#include "WFHeightmap.h"
#include "GL/glew.h"
#include "instance.h"
#include "VBO.h"
WFHeightMap::WFHeightMap(glm::ivec2 dimensions, glm::vec2 offsets)
{
	nodes.resize(dimensions.x);
	for (int i = 0; i < dimensions.x; i++) {
		nodes[i].resize(dimensions.y);
	}
	size = dimensions;
	offset = offsets;
}

float WFHeightMap::getHeight(int i, int j, HeightParameters& pars)
{
	//get the i, j node and calculate the heights using airy wave equations
	float height = 0.0f;
	
	float time = ((WFHeightParameters&)pars).time;
	WFHeighMapNode& node = nodes[i][j];
	for (int k = 0; k < node.records.size(); k++) {
		height += //exp(node.records[k].travel_time -time)*
			node.records[k].amplitude*glm::sin(node.records[k].angular_frequency*10.0f*glm::radians(node.records[k].travel_time - time));
	}
	return height;
}

void WFHeightMap::calculateNormals(WFRenderParameters& Rpars)
{
	WFHeightParameters pars;
	pars.time = Rpars.time;
	//@TODO calculate normals for corner cases
	for (int i = 1; i < size.x-1; i++) {
		for (int j = 1; j < size.y-1; j++) {
			glm::vec3 center = glm::vec3(i, getHeight(i, j, pars), j);
			glm::vec3 up = glm::vec3(i, getHeight(i, j + 1, pars), j + 1);
			glm::vec3 down = glm::vec3(i, getHeight(i, j - 1, pars), j - 1);
			glm::vec3 right = glm::vec3(i+1, getHeight(i+1, j, pars), j);
			glm::vec3 left = glm::vec3(i-1, getHeight(i-1, j, pars), j);
			nodes[i][j].normal = (glm::cross(up - center, right - center)
				+glm::cross(right-center,down-center)+glm::cross(down-center,left-center)+glm::cross(left-center,up-center))*0.25f;
		}
	}
}

void WFHeightMap::Render(RenderParameters& pars)
{	
	//clear color and depth buffer 
	instance inst;
	float time = ((WFRenderParameters&)pars).time;
	WFHeightParameters parsHeight;
	parsHeight.time = time;
	glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
	//mod.vertices.resize(size.x*size.y * 6);
	//mod.normals.resize(size.x*size.y * 6);
	inst.mat = ((WFRenderParameters&)pars).mat;
	inst.mat->color = glm::vec4(0, 1, 0, 1);
	inst.mod = ((WFRenderParameters&)pars).mod;
	glColor3f(0.0f, 0.0f, 1.0f); //blue color
	inst.mod->vertices.resize(size.x*size.y * 6);
	int c = 0;
	int c2 = 0;
	for (int i = 0; i < size.x - 1; i++) {
		for (int j = 0; j < size.y - 1; j++) {
			inst.mod->vertices[c++] = glm::vec3(i* offset.x, getHeight(i,j,parsHeight), j * offset.y);
			inst.mod->vertices[c++] = glm::vec3(i* offset.x, getHeight(i,j + 1,parsHeight), (j + 1) * offset.y);
			inst.mod->vertices[c++] = glm::vec3((i + 1) * offset.x, getHeight(i + 1,j,parsHeight), j * offset.y);
			inst.mod->vertices[c++] = glm::vec3((i + 1) * offset.x, getHeight(i + 1,j + 1,parsHeight), (j + 1) * offset.y);
			inst.mod->vertices[c++] = glm::vec3((i + 1) * offset.x, getHeight(i + 1,j,parsHeight), j * offset.y);
			inst.mod->vertices[c++] = glm::vec3(i * offset.x, getHeight(i,j + 1,parsHeight), (j + 1) * offset.y);
			
			inst.mod->normals[c2++] = nodes[i][j].normal;
			inst.mod->normals[c2++] = nodes[i][j + 1].normal;
			inst.mod->normals[c2++] = nodes[i + 1][j].normal;
			inst.mod->normals[c2++] = nodes[i + 1][j + 1].normal;
			inst.mod->normals[c2++] = nodes[i + 1][j].normal;
			inst.mod->normals[c2++] = nodes[i][j + 1].normal;
			//glBegin(GL_QUADS);//start drawing a line loop
			//glVertex3f(i+offsetX,heightMap[i][j],j+offsetZ);
			//glVertex3f(i+1+offsetX, heightMap[i+1][j], j+offsetZ);
			//glVertex3f(i+1+offsetX, heightMap[i+1][j+1], j+1+offsetZ);
			//glVertex3f(i+offsetX, heightMap[i][j+1], j+1+offsetZ);
			//glEnd();
		}
	}
	buffers::Load(*inst.mod);
	inst.RenderTriangles();
	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	//float time = ((WFRenderParameters&)pars).time;
	//WFHeightParameters parsHeight;
	//parsHeight.time = time;
	//glBegin(GL_QUADS);
	//for (int i = 1; i < size.x - 1; i++) {
	//	for (int j = 1; j < size.y -1 ; j++) {
	//		//glBegin(GL_LINE_LOOP);//start drawing a line loop
	//		glm::ivec2 ind = glm::ivec2(i, j);
	//		glm::vec3 normal = glm::cross(glm::vec3(1,getHeight(ind.x+1,ind.y,parsHeight)-getHeight(ind.x,ind.y,parsHeight),0),glm::vec3(0,getHeight(ind.x,ind.y+1,parsHeight)-getHeight(ind.x,ind.y,parsHeight),1));
	//		glVertex3f(i*offset.x,getHeight(i,j,parsHeight), j*offset.y);
	//		glVertex3f((i+ 1)*offset.x, getHeight(i+1,j,parsHeight), j*offset.y);		
	//		glVertex3f((i + 1)*offset.x, getHeight(i+1,j+1,parsHeight), (j + 1)*offset.y);
	//		glVertex3f(i*offset.x, getHeight(i,j+1,parsHeight), (j + 1)*offset.y);
	//		//glEnd();
	//	}
	//}
	//glEnd();
}

void WFHeighMapNode::RemoveWorstRecord()
{
	std::vector<WFHeightMapNodeRecord>::iterator min=records.begin();
	for (std::vector<WFHeightMapNodeRecord>::iterator it = std::next(records.begin()); it != records.end(); it++) {
		if (it->amplitude < min->amplitude) {
			min = it;
		}
	}
	records.erase(min);
}
