#pragma once

#include "instance.h"
#include "Wavefront.h"
#include <vector>

class StaticObstacle
{
private:
	enum CollisionType {
		COLLISION_INSIDE, COLLISION_OUTSIDE,COLLISION_STRONGLY_OUTSIDE,COLLISION_INBETWEEN
	};
	enum Location {
		LOCATION_INSIDE,LOCATION_OUTSIDE,LOCATION_ON
	};
	CollisionType findCollisionType(Wavefront_vertex& v0,Wavefront_vertex& v1);
	Location findLocation(glm::vec2 p0,glm::vec2 p1,glm::vec2 query);
	void findCollisions(Wavefront& w,std::vector<Wavefront>& outWavefronts);
	bool findIntersections(Wavefront_vertex& v0, Wavefront_vertex& v1,Wavefront_edge& edge,Wavefront& outWavefront,Wavefront_vertex& v0out, Wavefront_vertex& v1out);

public:
	instance inst;
	std::vector<glm::vec2> immersionOutLine;
	void UpdateImmersionOutLine();
	void Update(Wavefront& w,std::vector<Wavefront>& outWavefronts);
	void Update(Wavefront& w);
	void Render();

};

