#include "WaterHeightMap.h"
#include "instance.h"
#include "VBO.h"
#include <iostream>
#include <glm\gtx\fast_trigonometry.hpp>
#include <glm\gtx\string_cast.hpp>


float getGaussianHeight(WaterHeightMap& map,int i,int j, int size,WaterHeightMapHeightParameters pars) {
	float total = 0;
	int counter = 0;
	float time = pars.time;
	//return map.getHeight(i, j, pars);
	if (map.nodes[i][j].records.size()==0) {
		//return 0;
	}
	for (int k = -size*0.5; k < size*0.5f; k++) {
		for (int l = -size*0.5; l < size*0.5f; l++) {
			if (i + k<0 || i + k>=map.size.x || j + l<0 || j + l>=map.size.y)
				continue;
			
			//std::cout << "index " << i + k << " and " << j + l << std::endl;
			//if (map.nodes[i + k][j + l].records.size() == 0)continue;
			//WaterHeightMapNodeRecord record = map.nodes[i + k][j + l].records.back();
			if (map.nodes[i + k][j + l].records.size() > 0) {
				total += map.getHeight(i+k, j+l, pars);
				counter++;
			}
			//counter++;
		}
	}
	if (counter > 0) {
		total /= counter;
	}
	return total;
}

WaterHeightMap::WaterHeightMap(glm::ivec2 dimensions, glm::vec2 offsets)
{
	size = dimensions;
	offset = offsets;
	nodes.resize(size.x);
	for (int i = 0; i < nodes.size(); i++) {
		nodes[i].resize(size.y);
	}
}

float WaterHeightMap::getHeight(int i, int j, HeightParameters & pars)
{
	float height = 0;
	if (i < 0 || i >= size.x || j < 0 || j >= size.y) {
		return 0.0f;
	}
	WaterHeightMapNode& node = nodes[i][j];
	float time = ((WaterHeightMapHeightParameters&)pars).time;

	for (int i = 0; i < node.records.size();i++) {
		
		WaterHeightMapNodeRecord& record = node.records[i];
		float phase = record.init_phase_time + record.init_phase_space;
		float distrort_phase = record.distorted_phase_time +record.distorted_phase_space;
		float phaseDiff = time - distrort_phase;
		float damping = record.dampingCoeff.linearValue(phaseDiff);
		if (phaseDiff > 0) {
			height += record.weight*damping*(record.weight)*
				record.amplitude*glm::fastSin(record.angular_frequency*glm::radians((time - distrort_phase)));
		}
	}
	return height;
}



glm::vec2 WaterHeightMap::getDirection(int i, int j, HeightParameters & pars)
{
	glm::vec2 speed = glm::vec2(0.0f);
	if (i < 0 || i >= size.x || j < 0 || j >= size.y) {
		return glm::vec2(0.0f);
	}
	WaterHeightMapNode& node = nodes[i][j];
	float time = ((WaterHeightMapHeightParameters&)pars).time;
	static int max = 0;
	int last_max = max;
	if (node.records.size() > max) {
		max = node.records.size();
	}
	for (int i = 0; i <node.records.size(); i++) {
		WaterHeightMapNodeRecord& record = node.records[i];
		if (record.isCrestWave&&time > record.dampingCoeff.endTime)continue;
		float phase = record.init_phase_time-5.0f + record.init_phase_space;
		float distrort_phase = record.distorted_phase_time + record.distorted_phase_space;
		float phaseDiff = time - phase;
		float damping = record.dampingCoeff.linearValue(phaseDiff);
		//speed += record.direction;
		if (phaseDiff > 0) {
			speed += record.direction;
		}
			//speed += record.weight*damping*(record.weight)*
			//	record.amplitude*record.direction;
		//}
	}
	//if (glm::length(speed) != 0) {
	//	speed = glm::normalize(speed);
	//}
	if(max>last_max)
	std::cout << "max is " << max << std::endl;
	return speed;
}
float WaterHeightMap::getSpeed(int i, int j, HeightParameters & pars)
{
	glm::vec2 speed = glm::vec2(0.0f);
	if (i < 0 || i >= size.x || j < 0 || j >= size.y) {
		return 0.0f;
	}
	WaterHeightMapNode& node = nodes[i][j];
	float time = ((WaterHeightMapHeightParameters&)pars).time;

	for (int i = 0; i < node.records.size(); i++) {
		WaterHeightMapNodeRecord& record = node.records[i];
		float phase = record.init_phase_time + record.init_phase_space;
		float phaseDiff = time - phase;
		float damping = record.dampingCoeff.linearValue(phaseDiff);
		if (phaseDiff > 0) {
			speed = record.weight*damping*(record.weight)*
				record.amplitude*record.direction;
		}
	}
	return glm::length(speed);
}
void WaterHeightMap::Render(RenderParameters & pars)
{
	//clear color and depth buffer 
	instance inst;
	float time = ((WaterHeightMapRenderParameters&)pars).time;
	WaterHeightMapHeightParameters parsHeight;
	parsHeight.time = time;
	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	//mod.vertices.resize(size.x*size.y * 6);
	//mod.normals.resize(size.x*size.y * 6);
	inst.mat = ((WaterHeightMapRenderParameters&)pars).mat;
	//inst.mat->color = glm::vec4(1, 1, 0, 1);
	inst.mod = ((WaterHeightMapRenderParameters&)pars).mod;
	//glColor3f(0.0f, 0.0f, 1.0f); //blue color
	int c = 0;
	int c2 = 0;
	inst.mod->vertices.resize(size.x*size.y*6);
	inst.mod->normals.resize(size.x*size.y * 6);
	int kernelsize = 2;
	//#pragma omp parallel for
	for (int i = 0; i < size.x - 1; i++) {
		for (int j = 0; j < size.y - 1; j++) {
			inst.mod->vertices[c++] = glm::vec3(i* offset.x, getGaussianHeight(*this,i, j,kernelsize, parsHeight), j * offset.y);
			inst.mod->vertices[c++] = glm::vec3(i* offset.x, getGaussianHeight(*this,i, j + 1, kernelsize,parsHeight), (j + 1) * offset.y);
			inst.mod->vertices[c++] = glm::vec3((i + 1) * offset.x, getGaussianHeight(*this,i + 1, j, kernelsize,parsHeight), j * offset.y);
			inst.mod->vertices[c++] = glm::vec3((i + 1) * offset.x, getGaussianHeight(*this,i + 1, j + 1, kernelsize, parsHeight), (j + 1) * offset.y);
			inst.mod->vertices[c++] = glm::vec3((i + 1) * offset.x, getGaussianHeight(*this,i + 1, j, kernelsize, parsHeight), j * offset.y);
			inst.mod->vertices[c++] = glm::vec3(i * offset.x, getGaussianHeight(*this,i, j + 1,kernelsize, parsHeight), (j + 1) * offset.y);

			inst.mod->normals[c2++] = nodes[i][j].normal;
			inst.mod->normals[c2++] = nodes[i][j + 1].normal;
			inst.mod->normals[c2++] = nodes[i + 1][j].normal;
			inst.mod->normals[c2++] = nodes[i + 1][j + 1].normal;
			inst.mod->normals[c2++] = nodes[i + 1][j].normal;
			inst.mod->normals[c2++] = nodes[i][j + 1].normal;
			//glBegin(GL_QUADS);//start drawing a line loop
			//glVertex3f(i+offsetX,heightMap[i][j],j+offsetZ);
			//glVertex3f(i+1+offsetX, heightMap[i+1][j], j+offsetZ);
			//glVertex3f(i+1+offsetX, heightMap[i+1][j+1], j+1+offsetZ);
			//glVertex3f(i+offsetX, heightMap[i][j+1], j+1+offsetZ);
			//glEnd();
		}
	}
	buffers::Load(*inst.mod);
	inst.RenderTriangles();
	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
}

void WaterHeightMap::PrepareInstanceHeightsForRendering(RenderParameters & pars,std::vector<GLfloat>& renderingHeights)
{
	//clear color and depth buffer
	float time = ((WaterHeightMapRenderParameters&)pars).time;
	WaterHeightMapHeightParameters parsHeight;
	parsHeight.time = time;
	if (renderingHeights.size() < size.x*size.y) {
		renderingHeights.resize(size.x*size.y);
		std::fill(renderingHeights.begin(), renderingHeights.end(), 0.0f);
	}
	//return;
	#pragma omp parallel for
	for (int i = 0; i < size.x; i++) {
		for (int j = 0; j < size.y; j++) {
			renderingHeights[i*size.x + j] += getHeight(j, i, parsHeight);
		}
	}
}

void WaterHeightMap::finalizeRenderingSpeeds(std::vector<glm::vec2>& renderingDirections,std::vector<GLfloat>& renderingSpeeds) {
	glm::vec2 padding = glm::normalize(glm::vec2(1, 1));
	if (renderingSpeeds.size() < renderingDirections.size()) {
		renderingSpeeds.resize(renderingDirections.size());
		std::fill(renderingSpeeds.begin(), renderingSpeeds.end(), 0.0f);
	}
	//#pragma omp parallel for
	for (int i = 0; i < size.x; i++) {
		for (int j = 0; j < size.y; j++) {
			if(renderingDirections[i*size.x+j]!=glm::vec2(0.0f)){
				renderingSpeeds[i*size.x + j] = glm::length(renderingDirections[i*size.x + j]);
				renderingDirections[i*size.x + j] = glm::normalize(renderingDirections[i*size.x+j]);
				renderingDirections[i*size.x + j] = renderingDirections[i*size.x + j] + padding;
			}
			else {
				renderingSpeeds[i*size.x + j] = 1.0f;
				renderingDirections[i*size.x + j] = padding;
			}
		}
	}
}

void WaterHeightMap::PrepareInstanceDirectionForRendering(RenderParameters & pars, std::vector<glm::vec2>& renderingDirections)
{
	//clear color and depth buffer
	float time = ((WaterHeightMapRenderParameters&)pars).time;
	WaterHeightMapHeightParameters parsHeight;
	parsHeight.time = time;
	if (renderingDirections.size() < size.x*size.y) {
		renderingDirections.resize(size.x*size.y);
		std::fill(renderingDirections.begin(), renderingDirections.end(), glm::vec2(0.0f));
	}
	#pragma omp parallel for
	for (int i = 0; i < size.x; i++) {
		for (int j = 0; j < size.y; j++) {
			renderingDirections[i*size.x + j] += getDirection(j, i, parsHeight);
		}
	}

}

void WaterHeightMap::PrepareInstanceNormalsForRendering(std::vector<std::vector<float>>& heights,std::vector<std::vector<glm::vec3>>& normals)
{
	if (normals.size() == 0) {
		normals.resize(size.x);
		for (int i = 0; i < normals.size(); i++) {
			normals[i].resize(size.y);
		}
	}
	for (int i = 1; i < size.x - 1; i++) {
		for (int j = 1; j < size.y - 1; j++) {
			glm::vec3 center = glm::vec3(i,heights[i][j], j);
			glm::vec3 up = glm::vec3(i,heights[i][j+1], j + 1);
			glm::vec3 down = glm::vec3(i,heights[i][j-1], j - 1);
			glm::vec3 right = glm::vec3(i + 1,heights[i+1][j], j);
			glm::vec3 left = glm::vec3(i - 1,heights[i-1][j], j);
			normals[i][j] = (glm::cross(up - center, right - center)
				+ glm::cross(right - center, down - center) + glm::cross(down - center, left - center) + glm::cross(left - center, up - center))*0.25f;
			if (normals[i][j] != glm::vec3(0.0f)) {
				normals[i][j] = glm::normalize(normals[i][j]);
			}
			//if(normal[i][j])
		}
	}

}

void WaterHeightMap::calculateNormals(WaterHeightMapRenderParameters & Rpars)
{
	int kernelsize = 2;
	WaterHeightMapHeightParameters pars;
	pars.time = Rpars.time;
	//@TODO calculate normals for corner cases
	for (int i = 1; i < size.x - 1; i++) {
		for (int j = 1; j < size.y - 1; j++) {
			glm::vec3 center = glm::vec3(i, getGaussianHeight(*this,i, j,kernelsize, pars), j);
			glm::vec3 up = glm::vec3(i, getGaussianHeight(*this,i, j + 1, kernelsize, pars), j + 1);
			glm::vec3 down = glm::vec3(i, getGaussianHeight(*this,i, j - 1, kernelsize, pars), j - 1);
			glm::vec3 right = glm::vec3(i + 1, getGaussianHeight(*this,i + 1, j, kernelsize, pars), j);
			glm::vec3 left = glm::vec3(i - 1, getGaussianHeight(*this,i - 1, j, kernelsize, pars), j);
			nodes[i][j].normal = (glm::cross(up - center, right - center)
				+ glm::cross(right - center, down - center) + glm::cross(down - center, left - center) + glm::cross(left - center, up - center))*0.25f;
		}
	}
}
int record_limit = 5;
void WaterHeightMapNode::Add(WaterHeightMapNodeRecord & record, float time)
{
	//records[0] = record;
	//must normalise weights and remove 
	if (records.size() < record_limit) {
		records.push_back(record);
		
	}
	else { //we must begin a glorious journey to find the one that must die for thie other to survive
		int best_index = 0;
		float best_magnitude = std::numeric_limits<float>::max();
		for (int i = 0; i < records.size(); i++) {
			WaterHeightMapNodeRecord& record = records[i];
			float magn = record.amplitude*record.weight;
			if (record.dampingCoeff.startTime!=0.0f&&record.dampingCoeff.endTime < time) {
				best_index = i;
				break;
			}
			if (magn < best_magnitude) {
				best_magnitude = magn;
				best_index = i;
			}
		}
		records[best_index] = record;
	}
}
