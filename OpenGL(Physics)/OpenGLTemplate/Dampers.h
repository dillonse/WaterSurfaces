#pragma once
struct DampingCoefficients {
public:
	float peakValue = 1.0f;
	float cuttoffValue = 1.0f;
	float beginValue = 1.0f;
	float endTime;
	float startTime;
	float middle;
	float linearValue(float time);
	float expValue(float time);
};