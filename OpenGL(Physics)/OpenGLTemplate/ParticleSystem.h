#pragma once
#include <glm\vec3.hpp>
#include <vector>
#include <deque>
#include "Particle.h"
#include "UnaryForce.h"
#include "instance.h"

#define DEBUG false

typedef struct ParticleSystem {
	//physics related
	float time;
	std::deque<Particle> particles;
	std::vector<UnaryForce*> unaryForces;
	void Update(float deltaTime,std::vector<instance>& colliders);
	void Reset();
	//apperance related
	instance particleInstance;
	void Render();
}ParticleSystem;

enum ResponseType
{
	FORCE,IMPULSE,POST
};