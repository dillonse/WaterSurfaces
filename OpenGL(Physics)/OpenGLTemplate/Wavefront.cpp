#include "Wavefront.h"
#include "instance.h"
#include "WavefrontSystem.h"
#include <algorithm>
#include <iostream>
#include <vector>
#include <iterator>
#include <numeric>
#include <glm\gtx\string_cast.hpp>
#include "global.h"
int Wavefront::instance_counter = 0;
Wavefront::Wavefront(Wavefront & wavefront, int begin, int end)
{
	//copy the vertices
	std::copy(std::next(wavefront.VWrite.begin(), begin), std::next(wavefront.VWrite.begin(), end - 1), std::back_inserter(VWrite));
	std::copy(std::next(wavefront.VRead.begin(), begin), std::next(wavefront.VRead.begin(), end - 1), std::back_inserter(VRead));

	//copy the edges
	std::copy(std::next(wavefront.EWrite.begin(), begin), std::next(wavefront.EWrite.begin(), end - 2), std::back_inserter(EWrite));
	std::copy(std::next(wavefront.ERead.begin(), begin), std::next(wavefront.ERead.begin(), end - 2), std::back_inserter(ERead));

	wavenum = wavefront.wavenum;
	angular_frequency = wavefront.angular_frequency;
	id = instance_counter++;
}
void Wavefront::Init(TerrainHeightMap& map,float energyDensity)
{
	std::cout << "wavenum is" <<wavenum<< std::endl;
	for (int i = 0; i < VWrite.size(); i++) {
		glm::vec2 query_position = VWrite[i].position;
		VWrite[i].Update(wavenum, map.getHeight(query_position));
	}
	VRead = std::vector<Wavefront_vertex>(VWrite);
	float averageSpeed = 0.0f;
	for (int i = 0; i < VWrite.size(); i++) {
		glm::vec2 query_position = VWrite[i].position;
		//angular_frequency += glm::length(VWrite[i].velocity);
		averageSpeed += glm::length(VWrite[i].velocity);
	}
	averageSpeed /= VWrite.size();
	wavenum = angular_frequency / averageSpeed;
	//init the edges
	EWrite.assign(VWrite.size() - 1, Wavefront_edge(energyDensity, 0.0f, 0.0f,wavenum));
	///compute the angular frequency of the wavefront. Use the phase speeds of the vertices
	//update the edges of the wavefront
	for (int i = 0; i < VWrite.size()-1; i++) {
		glm::vec2 query_position = (VWrite[i].position + VWrite[i + 1].position)*0.5f;
		EWrite[i].Update(angular_frequency, map.getHeight(query_position), wavenum);
	}
	std::cout << "phase speed must be " << angular_frequency / wavenum<<std::endl;
	ERead = std::vector<Wavefront_edge>(EWrite);
}

void Wavefront::Render()
{

	glBegin(GL_LINE_STRIP);//start drawing a line loop
	for (int i = 0; i < VRead.size(); i++) {
		glVertex3f(VWrite[i].position.x, 0.0f, VWrite[i].position.y);
	}
	glEnd();
}


float calculateAngularFreq(float wavenum, float depth) {
	float g = 9.8;
	float rho = 1.0f;
	float surface_tension = 1.0f;
	return glm::sqrt((g*wavenum + (surface_tension / rho)*wavenum*wavenum*wavenum)*glm::tanh(glm::radians(wavenum*depth)));
}

void Wavefront::Update(TerrainHeightMap&  map)
{
	if (isDestructable) {
		lifeTime -= time::deltaTime;
	}
	if (VWrite.size() == 0) {
		return;
	}
	//delete segments that do not move
	int deletions = 0;
	for (int i = 0; i < VWrite.size() - 1; i++) {
		if (VWrite[i].position == VRead[i].position&&VWrite[i + 1].position == VRead[i + 1].position) {
			VWrite.erase(std::next(VWrite.begin(), i - deletions));
			EWrite.erase(std::next(EWrite.begin(), i - deletions));
			deletions++;
		}
	}
	float g = 9.8;
	float rho = 1.0f;
	float surface_tension = 1.0f;
	//refine the wavefront curve resolution
	float max_length =map.offset.x * 5;
	float min_lengh = map.offset.x*0.00002;
	glm::vec2 start = VWrite[0].velocity;
	if (start != glm::vec2(0.0f))start = glm::normalize(start);
	glm::vec2 end = VWrite[VWrite.size() - 1].velocity;
	if (end != glm::vec2(0.0f))end = glm::normalize(end);
	glm::vec2 group = (start + end)*0.5f;
	int index = 0;
	for (std::vector<Wavefront_vertex>::iterator it = VWrite.begin(); it != VWrite.end()&&std::next(it) != VWrite.end();it=std::next(it)) {
		
		if ((glm::distance(it->position, std::next(it)->position) > max_length)){
			std::vector<Wavefront_edge>::iterator edge_it = std::next(EWrite.begin(), std::distance(VWrite.begin(), it));
				EWrite.insert(edge_it, Wavefront_edge(*edge_it));
			it = VWrite.insert(std::next(it), Wavefront_vertex((it->position + std::next(it)->position)*0.5f, (it->velocity+std::next(it)->velocity)*0.5f));
			if (it != VWrite.begin())
				it = std::prev(it, 1);
		}
		else if (glm::distance(it->position, std::next(it)->position) < min_lengh
			) {
			std::vector<Wavefront_edge>::iterator edge_it = std::next(EWrite.begin(), std::distance(VWrite.begin(), it));
			EWrite.erase(edge_it);
			it = VWrite.erase(std::next(it));
			if (it != VWrite.begin())
				it = std::prev(it, 1);
		}
		else if (it != VWrite.begin() && it != VWrite.end()) {
				glm::vec2 current = it->velocity;
				if (glm::dot(group, current) < 0) {
					//std::cout << "dist is " << std::distance(VWrite.begin(), it) << std::endl;
					std::vector<Wavefront_edge>::iterator edge_it = std::next(EWrite.begin(), std::distance(VWrite.begin(), it));
					EWrite.erase(edge_it);
					it = VWrite.erase(std::next(it));
					if (it != VWrite.begin())
						it = std::prev(it, 1);
				}
		}
	}

	//advocate the vertices of the wavefront
	VRead = std::vector<Wavefront_vertex>(VWrite);
	for (int i = 0; i < VWrite.size(); i++) {
		glm::vec2 query_position = VRead[i].position;
		//if (isDestructable) {
	//		VWrite[i].Update(wavenum, -1000.0f);
			//std::cout << "is destruct" << std::endl;
	//	}
		//else {
			VWrite[i].Update(wavenum, map.getHeight(query_position));
		//}
	//	if (map.getContourIndex(VWrite[i].position) != map.getContourIndex(VRead[i].position)) {
			//std::cout << "went from " << map.getContourIndex(VWrite[i].position) << "to " << map.getContourIndex(VRead[i].position) << std::endl;
			//correct the velocity for vertex if it is out of the map
			//change the direction according to Snell's law of refraction
			float eta = glm::min(
				glm::length(VWrite[i].velocity) / glm::length(VRead[i].velocity),
				glm::length(VRead[i].velocity) / glm::length(VWrite[i].velocity));
			//glm::vec2 normal = glm::vec2(0.0f);
			///calculate the normal based on the depths of the old position
			glm::vec2 normal = map.getRefractionNormal(VRead[i].position);
			if ((normal != glm::vec2(0.0f)) && (VRead[i].velocity != glm::vec2(0.0f))&&(!isDestructable)) {
				normal = glm::normalize(normal);
				VWrite[i].velocity = glm::refract(glm::normalize(VRead[i].velocity), normal, eta)*glm::length(VWrite[i].velocity);
				if (VWrite[i].velocity == glm::vec2(0.0f)) {
					
				}
				//std::cout << "vel was " << glm::to_string(VRead[i].velocity) << std::endl;
				//std::cout << "vel is " << glm::to_string(VWrite[i].velocity) << std::endl;
				//std::cout << "vel is " << glm::to_string(VWrite[i].velocity) << std::endl;
			}
	//	}
		if (VWrite[i].position.x <= 0||VWrite[i].position.y<=0
			||VWrite[i].position.x>=(map.size.x-1)*map.offset.x||VWrite[i].position.y>=(map.size.y-1)*map.offset.y
			) {
				VWrite[i].position = VRead[i].position;
		}
	}
	//calculate the new density on the edges of the wavefront
	ERead = std::vector<Wavefront_edge>(EWrite);
	for (int i = 0; i < EWrite.size(); i++) {
		//first calculate the wavenum and group speed
		glm::vec2 query_position = (VWrite[i].position+VWrite[i+1].position)*0.5f;
		//std::cout << "i is "<<i << std::endl;
		EWrite[i].Update(angular_frequency, map.getHeight(query_position), ERead[i].wavenum);
		//calcuate the lengths
		float oldL = glm::distance(VRead[i].position, VRead[i + 1].position);
		float newL = glm::distance(VWrite[i].position, VWrite[i + 1].position);
		//calculate the energy density
		if (EWrite[i].groupSpeed == 0) {
			EWrite[i].energyDensity = 0.0f;
			EWrite[i].amplitude = 0.0f;
		}
		else {
			
			EWrite[i].energyDensity = (ERead[i].energyDensity*ERead[i].groupSpeed*oldL) / (EWrite[i].groupSpeed*newL);
			//calculate the amplitude of the edge
			EWrite[i].amplitude = glm::sqrt((oldL / newL)*(ERead[i].groupSpeed / EWrite[i].groupSpeed)*((2 * ERead[i].energyDensity) / (rho*g + surface_tension*EWrite[i].wavenum*EWrite[i].wavenum)));
		}
	}
	//debug
	/*float energy = 0.0f;
	for (int i = 0; i < ERead->size(); i++) {
		glm::vec2 p0 = VRead[i].position;
		glm::vec2 p1 = VRead[i + 1].position;
		energy += ERead[i].energyDensity*glm::distance(p0, p1);
	}*/
	//std::cout << "energy is " << energy << std::endl;

}

int depth_threshold = 10;
void Wavefront_vertex::Update(float wavenum,float depth)
{
	if (depth > 0)
		return;
	depth *= -1;
	//calculate speed
	if (depth < depth_threshold&&wavenum>1) {
		float g = 9.8;
		float rho = 1.0f;
		float surface_tension = 1.0f;
		float speed = glm::sqrt(((g / wavenum) + (surface_tension / rho)*wavenum)*glm::tanh(glm::radians(wavenum*depth)));
		if (glm::length(velocity) > 0)
			velocity = glm::normalize(velocity)*speed;
	}
	//update velocity based on speed
	//update position
	position += time::deltaTime*velocity;
}

float calculatePhaseSpeed(float wavenum, float depth) {
	float g = 9.8;
	float rho = 1.0f;
	float surface_tension = 1.0f;
	return glm::sqrt((g/wavenum + (surface_tension / rho)*wavenum)*glm::tanh(glm::radians(wavenum*depth)));
}


void Wavefront_edge::Update(float angular_freq, float depth, float initValue)
{
	if (depth >= 0)return;
	depth = -depth;
	int it_limit = 1000;
	float wnum = initValue;
	for (int i = 0; i < it_limit; i++) {
		wnum = angular_freq / calculatePhaseSpeed(wnum,depth);
	}
	float delta_k = 1.0f/100.0f;
	groupSpeed = (calculateAngularFreq(wnum + delta_k, depth) - calculateAngularFreq(wnum, depth))/(delta_k);
}
