#version 450

uniform samplerCube skyboxMap;
uniform sampler2D diffuseMap;
uniform sampler2D normalMap;
uniform sampler2D specularMap;
uniform sampler2D extraMap1;
uniform vec4 color;
uniform float alphaValue;
uniform float uvOffset;

uniform vec2 offset;
uniform ivec2 size;

uniform mat4 MV;
uniform mat4 MVP;
uniform mat4 NM; //Normal Matrix (Transpose of invrse of view)
uniform mat4 VI;//View Inverse
uniform mat4 V; //View

layout(location = 0) out vec4 fragment_color;
in vec2 uvCoords;
in vec3 normal;
in vec3 reflection;
in float ratio;
in vec3 pos;

const float EtaR = 0.97;
const float EtaG = 0.98; // Ratio of indices of refraction
const float EtaB = 0.99;
const float fresnelPower = 5.0;
const float F = ((1.0-EtaG) * (1.0-EtaG)) / ((1.0+EtaG) * (1.0+EtaG));

float fresnelRatio=0.0f;
vec2 uvDirection = vec2(1,1);

int kernelSize=0;

int isSmall(vec2 inp){
	return int(length(inp)<0.4f);
}

int isNotBig(vec2 inp){
	return int(length(inp)<0.99f);
}

vec2 gaussianDirection(vec2 xzcoords){
	vec2 speed=vec2(0.0f,0.0f);
	float gridSize= offset.x*size.x;
	//vec2 texelSize = 1.0 / textureSize(diffuseMap, 0);
	for(int i=-kernelSize;i<=kernelSize;i++){
		for(int j=-kernelSize;j<=kernelSize;j++){
			speed+= -((texture(specularMap,(xzcoords+vec2(i,j))/gridSize).rg)-normalize(vec2(1.0f,1.0f)));
			//texture(speculatMap,(xzcoords+vec2(i,j))/gridSize).r;
		}
	}
	vec2 ssign = sign(vec2(0,0)*isSmall(speed)+(1-isSmall(speed))*speed);
	float slen = clamp(length(ssign),0.0f,1.0f);
	speed = normalize(speed)*slen+speed*(1-slen);
	//int len = int(round(length(speed)));
	//speed =len*normalize(speed)+(1-len)*speed;
	speed = speed*(1-isNotBig(speed));
	return speed/((kernelSize+1)*(kernelSize+1));
}

vec3 calculateReflection(vec3 pos, vec3 norm){
  vec4 world_pos = (MV*vec4(pos,1)); 
  vec4 world_view = world_pos-vec4(0.0f,0.0f,0.0f,1.0f);
  vec3 viewDirection = vec3(VI*world_view);
  vec3 world_normal = (NM*vec4(norm,0.0f)).xyz;
  vec3 normalDirection = normalize(vec3(vec4(world_normal,0.0f)*V));
  fresnelRatio= F+(1.0-F)*pow(1.0-dot(normalize(-viewDirection),normalize((NM*vec4(norm,0)).xyz)),fresnelPower);
  return normalize(reflect(viewDirection,normalize(normalDirection)));
}

vec2 directionMapCoordinates; //for direction 
vec2 normalMapCoordinates;// for normal mapping
float tilingFactor=1.0f;
float uvSpeed=1.0f;

mat2 calculateRotationMatrix(vec2 direction){
	vec2 up = normalize(vec2(1,1));
	float cosine = dot(up,direction);
	float sine = up.x*direction.y+direction.x*up.y;
	mat2 rotationMat;
	rotationMat[0]= vec2(cosine,sine);
	rotationMat[1]= vec2(-sine,cosine);
	return rotationMat;
}

float gridSize =offset.x*size.x;

vec2 continuousCoordinates(vec2 uvs){
	float tiling = offset.x*tilingFactor;
	vec2 nuvs = ((uvs/tiling));
	ivec2 index = ivec2(uvs/tiling);
	ivec2 mods = ivec2(mod(index,vec2(2,2)));
	ivec2 inv_mods = ivec2(1,1)-mods;
	vec2 mirror_x = vec2(nuvs.x,1-nuvs.y);
	vec2 mirror_y = vec2(1-nuvs.x,nuvs.y);
	vec2 mirror_xy = vec2(1-nuvs.x,1-nuvs.y);
	vec2 result = //nuvs 
	nuvs*inv_mods.x*inv_mods.y
	+mirror_x*inv_mods.x*mods.y
	+mirror_y*mods.x*inv_mods.y
	+mirror_xy*mods.x*mods.y
	;
	//return normalize(vec2(-1,-1)*length(uvs));
	return  mod(nuvs,vec2(1,1));
}

float sum(vec2 v){
	return v.x+v.y; 
}

void main (void){
	float gridSize=offset.x*size.x;
	directionMapCoordinates = mod(pos.xz/gridSize,vec2(1.0,1.0));
	vec2 directionTexel =gaussianDirection(pos.xz);
	//directionTexel *=(1-abs(sign(fract(length(directionTexel)))));//gaussianDirection(pos.xz);
	//directionTexel = normalize(directionTexel);
	uvSpeed = texture(extraMap1,directionMapCoordinates).r;
	//directionTexel =normalize(vec2(0,1));
	//mat2 rotationMatrix = calculateRotationMatrix(directionTexel);
	//normalMapCoordinates = inverse(rotationMatrix)*normalMapCoordinates;
	//vec3 refractColor;
	//refractColor.r = vec3(texture(skyboxMap, RefractR)).r;
	//refractColor.g = vec3(texture(skyboxMap, RefractG)).g;
	//refractColor.b = vec3(texture(skyboxMap, RefractB)).b;
	//vec2 direction = texture(specularMap,coords).rg;

	float directedDistance = length(pos)*dot((MV*vec4(pos,1.0f)).xyz,vec3(0,0,-1));
	vec2 directionSign = sign(directionTexel);
	//tilingFactor=tilingFactor+tilingFactor*directedDistance;
	vec2 uvDistance = (directionTexel*(uvOffset*uvSpeed+sum(directionTexel*pos.xz*0.5f)*tilingFactor));
	normalMapCoordinates =mod(uvDistance,vec2(1.0f,1.0f));
	//+length(pos.xz)*directionTexel,vec2(1.0f,1.0f));// continuousCoordinates(directionTexel*length(pos.xz))+uvDistance;//continuousCoordinates(length(pos.xz*directionTexel)*directionTexel+uvDistance);
	vec2 newUVCoords =(vec2(0.5,0.5)+directionSign*0.5f*normalMapCoordinates);//fract(normalMapCoordinates+uvDistance);
	vec3 newNormal = normalize(0.15f*normal+clamp(1.0f/directedDistance,0.0,0.05f)*(texture(normalMap,newUVCoords).rgb*2.0-1.0));
	//1.0f*texture(normalMap,uvCoords).rgb;
	vec3 newRelfection = calculateReflection(pos,newNormal);
	float newRatio = fresnelRatio;
	vec3 reflectColor = vec3(texture(skyboxMap, newRelfection));
	if(newRatio>0.5){
		newRelfection = calculateReflection(pos,normal);
	}
	vec4 color = mix(vec4(0,0,0.2,0.4f), vec4(reflectColor,1.0f),clamp(newRatio,0.0f,1.0f));
	//vec4 extra1 = texture(extraMap1,directionMapCoordinates);
	//if(length(directionTexel)>0.0f)
	// directionTexel = normalize(directionTexel);
//	float leng= (1-isNotBig(directionTexel));
	fragment_color = color;
}