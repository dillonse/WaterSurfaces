#version 450

uniform mat4 MV;
uniform mat4 MVP;
uniform mat4 NM; //Normal Matrix (Transpose of invrse of view)
uniform mat4 V; //View
uniform mat4 VI;//View Inverse
uniform vec3 EyePosition;
uniform vec2 offset;

layout(location = 0) in vec3 pos;
layout(location = 1) in vec3 normal;
layout(location = 2) in vec2 uv;

const float EtaR = 0.97;
const float EtaG = 0.98; // Ratio of indices of refraction
const float EtaB = 0.99;
const float fresnelPower = 5.0;
const float F = ((1.0-EtaG) * (1.0-EtaG)) / ((1.0+EtaG) * (1.0+EtaG));

//out VS_OUT{
//	vec3 Reflect_VG;
//	vec3 RefractR_VG,RefractG_VG,RefractB_VG;
//	float Ratio_VG;
//}gs_in;

out vec2 xzcoords;

void main(void){
  xzcoords = vec2(pos.x,pos.z);
  //int size_index = int(size_float);
  //evenWeight = size_index%2;
  //oddWeight = (size_index+1)%2;
 // oddWeight = int((pos.x/offset.x)+1)%2;
  gl_Position = MVP * vec4(pos,1);
  vec4 world_pos = (MV*vec4(pos,1)); 
  vec4 world_view = world_pos-vec4(0.0f,0.0f,0.0f,1.0f);
  vec3 viewDirection = vec3(VI*world_view);
  vec3 world_normal = (NM*vec4(normal,0.0f)).xyz;
  vec3 normalDirection = normalize(vec3(vec4(world_normal,0.0f)*V));

  //gs_in.Reflect_VG = reflect(viewDirection,normalize(normalDirection));
  //gs_in.RefractR_VG = vec3(0,0,0);//refract(normalize(I),normalize((NM*vec4(normal,0)).xyz),EtaR);
  //gs_in.RefractG_VG = vec3(0,0,0); //refract(normalize(I),normalize((NM*vec4(normal,0)).xyz),EtaG);
  //gs_in.RefractB_VG = vec3(0,0,0);//refract(normalize(I),normalize((NM*vec4(normal,0)).xyz),EtaB);
  //gs_in.Ratio_VG = F+(1.0-F)*pow(1.0-dot(normalize(-viewDirection),normalize((NM*vec4(normal,0)).xyz)),fresnelPower);
}