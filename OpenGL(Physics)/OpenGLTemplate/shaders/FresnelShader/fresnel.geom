#version 450

layout(points) in;
layout(triangle_strip, max_vertices = 6) out;

uniform mat4 MV;
uniform mat4 MVP;
uniform mat4 NM; //Normal Matrix (Transpose of invrse of view)
uniform mat4 VI;//View Inverse
uniform mat4 V; //View
uniform vec2 offset;
uniform ivec2 size;
uniform sampler2D diffuseMap;
uniform sampler2D normalMap;

const float EtaR = 0.97;
const float EtaG = 0.98; // Ratio of indices of refraction
const float EtaB = 0.99;
const float fresnelPower = 4.0;
const float F = ((1.0-EtaG) * (1.0-EtaG)) / ((1.0+EtaG) * (1.0+EtaG));

int kernelSize=2;
float gridSize=1.0;
float fresnelRatio=0.0f;

float gaussianHeight(vec2 xzcoords);

float rand(vec2 co){
    return fract(sin(dot(co.xy ,vec2(12.9898,78.233))) * 43758.5453);
}

vec3 calculateReflection(vec3 pos, vec3 norm){
  vec4 world_pos = (MV*vec4(pos,1)); 
  vec4 world_view = world_pos-vec4(0.0f,0.0f,0.0f,1.0f);
  vec3 viewDirection = vec3(VI*world_view);
  vec3 world_normal = (NM*vec4(norm,0.0f)).xyz;
  vec3 normalDirection = normalize(vec3(vec4(world_normal,0.0f)*V));
  fresnelRatio= F+(1.0-F)*pow(1.0-dot(normalize(-viewDirection),normalize((NM*vec4(norm,0)).xyz)),fresnelPower);
  return normalize(reflect(viewDirection,normalize(normalDirection)));
}

vec3 calculateNormal(vec2 xzcoords){
	float heightC = gaussianHeight(xzcoords); 
	float heightR = gaussianHeight(xzcoords+vec2(offset.x,0.0f));
	float heightL = gaussianHeight(xzcoords+vec2(-offset.x,0.0f));
	float heightN = gaussianHeight(xzcoords+vec2(0.0f,offset.x));
	float heightS = gaussianHeight(xzcoords+vec2(0.0f,-offset.x));
	vec3 vecN = vec3(0,heightN-heightC,offset.x);
	vec3 vecS = vec3(0,heightS-heightC,-offset.x);
	vec3 vecL = vec3(-offset.x,heightL-heightC,0);
	vec3 vecR = vec3(offset.x,heightR-heightC,0);
	vec3 crosss=(cross(vecN,vecR)+cross(vecR,vecS)+cross(vecS,vecL)+cross(vecL,vecN));
	return normalize(crosss);
}

float gaussianHeight(vec2 xzcoords){
	float height=0.0f;
	//vec2 texelSize = 1.0 / textureSize(diffuseMap, 0);
	for(int i=-kernelSize;i<=kernelSize;i++){
		for(int j=-kernelSize;j<=kernelSize;j++){
			height+=texture(diffuseMap,(xzcoords+vec2(i,j))/gridSize).r;
		}
	}
	return height/((kernelSize+1)*(kernelSize+1));
}

in vec2 xzcoords[];
out vec2 uvCoords;
out vec3 normal;
out vec3 reflection;
out float ratio;
out vec3 pos;
void main() {
  //set global vars
  gridSize=offset.x*size.x;
  //calc positions
  vec4 point1 = vec4(xzcoords[0].x,gaussianHeight(xzcoords[0]),xzcoords[0].y,1.0f);
  vec4 point1xz = vec4(point1.x,0,point1.z,1.0f);
  vec4 point2 = point1xz+vec4(offset.x,gaussianHeight((xzcoords[0]+vec2(offset.x,0))),0,0);
  vec4 point3 = point1xz+vec4(0,gaussianHeight((xzcoords[0]+vec2(0,offset.x))),offset.x,0);
  vec4 point4 = point1xz+vec4(offset.x,gaussianHeight(xzcoords[0]+vec2(offset.x,offset.x)),offset.x,0);
  //calc normals
  vec3 normal1 = calculateNormal(xzcoords[0]);
  vec3 normal2 = calculateNormal(xzcoords[0]+vec2(offset.x,0));
  vec3 normal3 = calculateNormal(xzcoords[0]+vec2(0,offset.x));
  vec3 normal4 = calculateNormal(xzcoords[0]+vec2(offset.x,offset.x));
  //calculate reflections
  vec3 refl1 = calculateReflection(point1.xyz,normal1);
  float ratio1 = fresnelRatio;
  vec3 refl2 = calculateReflection(point2.xyz,normal2);
  float ratio2 = fresnelRatio;
  vec3 refl3 = calculateReflection(point3.xyz,normal3);
  float ratio3 = fresnelRatio;
  vec3 refl4 = calculateReflection(point4.xyz,normal4);
  float ratio4 = fresnelRatio;


  gl_Position = MVP*point2;
  uvCoords = vec2(1.0f,0.0f);
  pos =point2.xyz;
  //raindropData=findRaindropCores(pos.xz);
 // uvCoords =(xzcoords[0]+vec2(offset.x,0))/gridSize;
  normal = normal2;
  reflection = refl2;
  ratio = ratio2;
  EmitVertex();
   gl_Position = MVP*point1;
  pos= point1.xyz;
 // raindropData=findRaindropCores(pos.xz);
  //uvCoords =(xzcoords[0])/gridSize;
  uvCoords=vec2(0.0f,0.0f);
  normal = normal1;
  reflection = refl1;
  ratio = ratio1;
  EmitVertex();
  gl_Position = MVP*point3;
  uvCoords = vec2(0.0f,1.0f);
  pos = point3.xyz;
  //raindropData=findRaindropCores(pos.xz);
  //uvCoords =(xzcoords[0]+vec2(0,offset.x))/gridSize;
  normal =normal3;
  reflection = refl3;
  ratio = ratio3;
  EmitVertex();
  gl_Position = MVP*point3;
  pos = point3.xyz;
 // raindropData=findRaindropCores(pos.xz);
   uvCoords = vec2(0.0f,1.0f);
 // uvCoords =(xzcoords[0]+vec2(0,offset.x))/gridSize;
  normal = normal3;
  reflection = refl3;
  ratio = ratio3;
  EmitVertex();
  gl_Position =MVP*point2;
  pos = point2.xyz;
 // raindropData=findRaindropCores(pos.xz);
  uvCoords = vec2(1.0f,0.0f);
  //uvCoords =(xzcoords[0]+vec2(offset.x,0))/gridSize;
  normal = normal2;
  reflection = refl2;
  ratio = ratio2;
  EmitVertex();
  gl_Position = MVP*point4;
  pos = point4.xyz;
 // raindropData=findRaindropCores(pos.xz);
  uvCoords = vec2(1.0f,1.0f);
  //uvCoords =(xzcoords[0]+vec2(offset.x,offset.x))/gridSize;
  normal = normal4;
  reflection = refl4;
  ratio = ratio4;
  EmitVertex();
  EndPrimitive();
}