#include "WavefrontSystem.h"
#include <algorithm>
#include <iterator>
#include <random>
#include <glm/gtx/perpendicular.hpp>
#include <glm/gtx/string_cast.hpp>
#include <iostream>
#include "instance.h"
#include "global.h"
#include "MathUtilities.h"
#include "VBO.h"
#include "textures.h"


void WavefrontSystem::Init()
{
	float energyDensity = 1000.0f;
	for (int i = 0; i < wavefronts.size(); i++) {
		wavefronts[i].Init(TMap, energyDensity);
	}
	//boat.Init();
	TMap.PrepareInstanceForRendering();
}

void WavefrontSystem::Render(material & mat, model & mod, material& debug_mat,material& sand_mat)
{
	//render the wavefronts
	instance inst;
	inst.mat = &debug_mat;
	inst.mat->color = glm::vec4(1, 0, 0, 1);
	inst.BindMaterial();
	for (int i = 0; i < wavefronts.size(); i++) {
		//wavefronts[i].Render();
	}
	//for (int i = 0; i < dynamicObstacles.size(); i++) {
	//	if (dynamicObstacles[i].wavefrontLeft) {
	//		//dynamicObstacles[i].wavefrontLeft->Render();
	//	}
	//	if (dynamicObstacles[i].wavefrontRight) {
	//		//dynamicObstacles[i].wavefrontRight->Render();
	//	}
	//}
	//glPolygonMode(GL_FRONT, GL_LINE);
	//glPolygonMode(GL_BACK, GL_LINE);
	for (int i = 0; i < staticObstacles.size(); i++) {
		staticObstacles[i].Render();
	}
	boat.Render();
	for (int i = 0; i < dynamicObstacles.size(); i++) {
		dynamicObstacles[i].Render();
	}
	//glPolygonMode(GL_FRONT, GL_FILL);
	//glPolygonMode(GL_BACK, GL_FILL);
	//render the map
	WFRenderParameters WFpars;
	TRenderParameters Tpars;
	WaterHeightMapRenderParameters Wpars;
	WFpars.time = time;
	Wpars.time = time;
	WFpars.mat = &debug_mat;
	WFpars.mod = &mod;
	Tpars.mat = &sand_mat;
	Wpars.mat = &mat;
	Wpars.mod = &mod;
	////clear color and depth buffer
	inst.mat->color = glm::vec4(0, 0, 1, 1);
	inst.BindMaterial();  
	//WFMap.calculateNormals(WFpars);
	TMap.Render(Tpars);
	//return;
	//WFMap.Render(WFpars);
	WMap.PrepareInstanceHeightsForRendering(Wpars, renderHeights);
	WMap.PrepareInstanceDirectionForRendering(Wpars, renderDirections);
	//return;
	//for (int i = 0; i < dynamicObstacles.size(); i++) {
		//dynamicObstacles[i].map.PrepareInstanceHeightsForRendering(Wpars,renderHeights);
		//dynamicObstacles[i].map.PrepareInstanceDirectionForRendering(Wpars, renderDirections);
	//}
	//return;
	WMap.finalizeRenderingSpeeds(renderDirections,renderSpeeds);
	//std::sort(renderHeights.begin(), renderHeights.end());
	//std::cout << "max is " << renderHeights.back()<<std::endl;
	mat.DiffuseMapID=textures::loadTexture(renderHeights, mat.DiffuseMapID);
	mat.SpecularMapID = textures::loadTexture(renderDirections, mat.SpecularMapID);
	mat.ExtraMap1ID = textures::loadTexture(renderSpeeds, mat.ExtraMap1ID);
	//WMap.PrepareInstanceNormalsForRendering(heights, normals);
	inst.mat = &mat;
	inst.mod = &mod;
	int c = 0;
	int c2 = 0;
	inst.mod->vertices.resize(WMap.size.x*WMap.size.y);
	for (int i = 0; i < WMap.size.x - 1; i++) {
		for (int j = 0; j < WMap.size.y - 1; j++) {
			inst.mod->vertices[c++] = glm::vec3(i* WMap.offset.x,0.0f, j * WMap.offset.y);
		}
	}

	buffers::Load(*inst.mod);
	inst.RenderPoints();

	/*for (int i = 0; i < WMap.size.x; i++) {
		std::fill(heights[i].begin(),heights[i].end(),0.0f);
		std::fill(normals[i].begin(), normals[i].end(), glm::vec3(0.0f));
	}*/
	std::fill(renderHeights.begin(), renderHeights.end(), 0.0f);
	std::fill(renderDirections.begin(), renderDirections.end(), glm::vec2(0.0f));
	std::fill(renderSpeeds.begin(), renderSpeeds.end(), 0.0f);

	return;

	inst.mat->color = glm::vec4(1, 0, 1, 1);
	inst.BindMaterial();
	for (int k = 0; k < wavefronts.size(); k++) {
		for (int l = 0; l < wavefronts[k].VRead.size() - 1; l++) { //for all edges of the wavefront
																	  //construct the min box 
			glm::vec2 p0 = wavefronts[k].VRead[l].position;
			glm::vec2 p1 = wavefronts[k].VRead[l + 1].position;
			glm::vec2 p2 = wavefronts[k].VWrite[l + 1].position;
			glm::vec2 p3 = wavefronts[k].VWrite[l].position;
			//find the AABB of p0,p1,p2,p3
			std::pair<glm::vec2, glm::vec2> AABB;
			AABB.first.x = glm::max(glm::max(p0.x, p1.x), glm::max(p2.x, p3.x));
			AABB.first.y = glm::max(glm::max(p0.y, p1.y), glm::max(p2.y, p3.y));
			AABB.second.x = glm::min(glm::min(p0.x, p1.x), glm::min(p2.x, p3.x));
			AABB.second.y = glm::min(glm::min(p0.y, p1.y), glm::min(p2.y, p3.y));

			glm::ivec2 start, end;
			start = glm::floor(AABB.second / WFMap.offset) - glm::vec2(1);
			start = glm::clamp(start, glm::ivec2(0, 0), WFMap.size);
			end = glm::ceil(AABB.first / WFMap.offset) + glm::vec2(1);
			end = glm::clamp(end, glm::ivec2(0, 0), WFMap.size);

			inst.mat->color = glm::vec4(1, 0, 1, 1);
			inst.BindMaterial();
			glBegin(GL_LINE_STRIP);

			glVertex3f(start.x*WFMap.offset.x, 0.1f, WFMap.offset.y*start.y);
			glVertex3f(start.x*WFMap.offset.x, 0.1f, WFMap.offset.y*end.y);
			glVertex3f(end.x*WFMap.offset.x, 0.1f, WFMap.offset.y*end.y);
			glVertex3f(end.x*WFMap.offset.x, 0.1f, WFMap.offset.y*start.y);
			glVertex3f(start.x*WFMap.offset.x, 0.1f, WFMap.offset.y*start.y);
			glEnd();
			inst.mat->color = glm::vec4(0, 1, 1, 1);
			inst.BindMaterial();
			glBegin(GL_LINE_STRIP);
			glVertex3f(p0.x, 0.2f, p0.y);
			glVertex3f(p1.x, 0.2f, p1.y);
			glVertex3f(p2.x, 0.2f, p2.y);
			glVertex3f(p3.x, 0.2f, p3.y);
			glVertex3f(p0.x, 0.2f, p0.y);
			glEnd();
		}
	}

}


bool hasDone = false;
void WavefrontSystem::Update() {
	//perfrom wavefront partitioning
	int limit = wavefronts.size();
	float max_angle_offset = 0.9;
	std::vector<int> delete_indices;
	for (int i = 0; i < limit; i++) {
		glm::vec2 start =((wavefronts[i].VWrite.begin())->velocity);
		if (start != glm::vec2(0.0f)) {
			start = glm::normalize(start);
		}
		glm::vec2 end =((wavefronts[i].VWrite.end() - 1)->velocity);
		if (end != glm::vec2(0.0f)) {
			end = glm::normalize(end);
		}
		//check if angle between start and end
		if (glm::dot(start, end) < max_angle_offset) {
			start = (wavefronts[i].VRead.begin())->velocity;
			if (start != glm::vec2(0.0f)) {
				start = glm::normalize(start);
			}
			end = (wavefronts[i].VRead.end() - 1)->velocity;
			if (end != glm::vec2(0.0f)) {
				end = glm::normalize(end);
			}
		}
		//create the direction vector
		std::vector<char> directions;
		directions.resize(wavefronts[i].VWrite.size());
		for (int j = 0; j < directions.size(); j++) {
			glm::vec2 current = glm::normalize(wavefronts[i].VWrite[j].velocity);
			glm::vec2 mean = (start + end)*0.5f;
			directions[j] = (glm::dot(mean,current) > max_angle_offset) ? 'p' : 'n';
		}
		//create the partition index vector
		std::vector<std::pair<int, int>> partition_indices;
		char current_char = directions[0];
		int current_dist = 1;
		for (int j = 1; j < directions.size(); j++) {
			if (directions[j] != current_char) {
				if (current_dist > 3 && (directions.size() - current_dist) > 3) {
					partition_indices.push_back(std::make_pair(j - current_dist, j+1));
				}
				current_char = directions[j];
				current_dist = 0;
			}
			else {
				current_dist++;
			}
		}
		if (partition_indices.size() > 0) {
			if(wavefronts[i].VWrite.size()-partition_indices[partition_indices.size() - 1].second>2)
				partition_indices.push_back(std::make_pair(partition_indices[partition_indices.size() - 1].second-1, wavefronts[i].VWrite.size()+1));
		}
		//partate the wavefront
		for (int j = 0; j < partition_indices.size(); j++) {
			wavefronts.push_back(Wavefront(wavefronts[i], partition_indices[j].first, partition_indices[j].second));
		}
		if (partition_indices.size() > 0 || wavefronts[i].VWrite.size() < 2||(wavefronts[i].isDestructable&&wavefronts[i].lifeTime<0.0f)){
			delete_indices.push_back(i);
		}
	}
	for (int i = 0; i < delete_indices.size(); i++) {
		wavefronts.erase(std::next(wavefronts.begin(), delete_indices[i] - i));
	}
	//advocate the vertices of the wavefront
	//#pragma omp parallel for
	for (int i = 0; i < wavefronts.size(); i++) {
		wavefronts[i].Update(TMap);
	}

	//update the maps with the wavefronts
	for (int i = 0; i < wavefronts.size(); i++) {
		UpdateMaps(wavefronts[i]);
	}
	//#pragma omp parallel for
	for (int i = 0; i < dynamicObstacles.size(); i++) {
		if (dynamicObstacles[i].wavefrontLeft) {
			dynamicObstacles[i].wavefrontLeft->Update(TMap);
			dynamicObstacles[i].UpdateMap(*dynamicObstacles[i].wavefrontLeft, WFMap, time);//UpdateMaps(*dynamicObstacles[i].wavefrontLeft);
		}
		if (dynamicObstacles[i].wavefrontRight) {
			dynamicObstacles[i].wavefrontRight->Update(TMap);
			dynamicObstacles[i].UpdateMap(*dynamicObstacles[i].wavefrontRight, WFMap, time);
		}
	}
	if (boat.obstacle.wavefrontLeft) {
		boat.obstacle.wavefrontLeft->Update(TMap);
		boat.obstacle.UpdateMap(*boat.obstacle.wavefrontLeft, WFMap, time);
	}
	if (boat.obstacle.wavefrontRight) {
		boat.obstacle.wavefrontRight->Update(TMap);
		boat.obstacle.UpdateMap(*boat.obstacle.wavefrontRight, WFMap, time);
	}
	//check for collisions with the static obstacles
	std::vector<Wavefront> reflectionWavefronts;
	for (int i = 0; i < staticObstacles.size();i++) {
		for (int j = 0; j < wavefronts.size(); j++) {
			//staticObstacles[i].Update(wavefronts[j],reflectionWavefronts);
			staticObstacles[i].Update(wavefronts[j]);
		}
	}
	WaterHeightMapHeightParameters params;
	params.time = time;
	UpdateFlowForces(params);

	for (int i = 0; i < dynamicObstacles.size(); i++) {
		//for (int j = 0; j < wavefronts.size(); j++) {
			//dynamicObstacles[i].Update(wavefronts[j]);
		//}
		if (dynamicObstacles[i].wavefrontLeft) {
			//dynamicObstacles[i].wavefrontLeft->Update(TMap);
		}
		if (dynamicObstacles[i].wavefrontRight) {
			//dynamicObstacles[i].wavefrontRight->Update(TMap);
		}
		dynamicObstacles[i].CheckWavefrontRefreshStatus();
		dynamicObstacles[i].UpdateAnchorPaths();
		if (dynamicObstacles[i].isSelfDriven) {
			if (dynamicObstacles[i].releaseWavefront) {
				if (dynamicObstacles[i].wavefrontLeft&&dynamicObstacles[i].wavefrontLeft->EWrite.size()>0) {
					dynamicObstacles[i].wavefrontLeft->lifeTime = 2.0f;
					dynamicObstacles[i].wavefrontLeft->isDestructable = true;
					wavefronts.push_back(*dynamicObstacles[i].wavefrontLeft);
				}
				if (dynamicObstacles[i].wavefrontRight&&dynamicObstacles[i].wavefrontRight->EWrite.size()>0) {
					dynamicObstacles[i].wavefrontRight->lifeTime = 2.0f;
					dynamicObstacles[i].wavefrontRight->isDestructable = true;
					wavefronts.push_back(*dynamicObstacles[i].wavefrontRight);
				}
				dynamicObstacles[i].wavefrontLeft = NULL;
				dynamicObstacles[i].wavefrontRight = NULL;
			}
			dynamicObstacles[i].PropagateWavefront();
			//if (dynamicObstacles[i].wavefrontLeft)
				//dynamicObstacles[i].wavefrontLeft->Update(TMap);
			//if (dynamicObstacles[i].wavefrontRight)
				//dynamicObstacles[i].wavefrontRight->Update(TMap);
		}
	}
	boat.obstacle.CheckWavefrontRefreshStatus();
	boat.obstacle.UpdateAnchorPaths();
	if (boat.obstacle.isSelfDriven) {
		if (boat.obstacle.releaseWavefront) {
			if (boat.obstacle.wavefrontLeft&&boat.obstacle.wavefrontLeft->EWrite.size()>0) {
				boat.obstacle.wavefrontLeft->lifeTime = 2.0f;
				boat.obstacle.wavefrontLeft->isDestructable = true;
				wavefronts.push_back(*boat.obstacle.wavefrontLeft);
			}
			if (boat.obstacle.wavefrontRight&&boat.obstacle.wavefrontRight->EWrite.size()>0) {
				boat.obstacle.wavefrontRight->lifeTime = 2.0f;
				boat.obstacle.wavefrontRight->isDestructable = true;
				wavefronts.push_back(*boat.obstacle.wavefrontRight);
			}
			boat.obstacle.wavefrontLeft = NULL;
			boat.obstacle.wavefrontRight = NULL;
		}
		boat.obstacle.PropagateWavefront();
		//if (dynamicObstacles[i].wavefrontLeft)
		//dynamicObstacles[i].wavefrontLeft->Update(TMap);
		//if (dynamicObstacles[i].wavefrontRight)
		//dynamicObstacles[i].wavefrontRight->Update(TMap);
	}
	time += time::deltaTime;
}

void WavefrontSystem::UpdateFlowForces(WaterHeightMapHeightParameters& params)
{
	for (int i = 0; i < dynamicObstacles.size(); i++) {//for each dynamic object
		if (dynamicObstacles[i].isSelfDriven)continue;
		Rigidbody* rigid = dynamicObstacles[i].rigidbody;
		std::vector<glm::vec2>& outline = dynamicObstacles[i].immersionOutLine;
		std::vector<glm::vec2>& normals = dynamicObstacles[i].vertexNormals;
		rigid->forces.clear();
		//rigid->netForce = glm::vec3(0.0f);
		//rigid->netTorque = glm::vec3(0.0f);
		for (int j = 0; j < outline.size(); j++) {//for each point of the outline
			//query the sample map
			glm::vec3 point3D = rigid->geometry.mod->vertices[j];
			glm::vec2 point = glm::vec2(point3D.x, point3D.z);
			glm::vec3 normal3D = rigid->orientation*rigid->bodyspaceGeometry.normals[j];
			glm::vec2 normal = glm::vec2(normal3D.x, normal3D.z);
			if (point.x <= 0 || point.y <= 0 || point.x >= WFMap.size.x*WFMap.offset.x || point.y >= WFMap.size.y*WFMap.offset.y) {
				continue;
			}
			///find the four sampling points
			glm::ivec2 start = glm::floor(point / WMap.offset);
			glm::ivec2 end = start + glm::ivec2(1, 1);
			///calculate total velocity for each of the sampling points
			glm::vec2 velocity0 = glm::vec2(0.0f);
			glm::vec2 velocity1 = glm::vec2(0.0f);
			glm::vec2 velocity2 = glm::vec2(0.0f);
			glm::vec2 velocity3 = glm::vec2(0.0f);
			///point 0
			float totalAmplitude = 0.0f;
			for (int k = 0; k < WMap.nodes[start.x][start.y].records.size(); k++) {
				float weight = WMap.nodes[start.x][start.y].records[k].weight;
				float phase = WMap.nodes[start.x][start.y].records[k].distorted_phase_time+ glm::abs(WMap.nodes[start.x][start.y].records[k].distorted_phase_space);
				if (time - phase < 0)weight = 0.0f;
				float amplitude = WMap.nodes[start.x][start.y].records[k].amplitude;
				totalAmplitude += amplitude*weight;
				velocity0 += WMap.nodes[start.x][start.y].records[k].direction*amplitude*weight;
			}
			if (totalAmplitude > 0) {
				velocity0 /= totalAmplitude;
			}
			///point 1
			totalAmplitude = 0.0f;
			for (int k = 0; k < WMap.nodes[end.x][start.y].records.size(); k++) {
				float weight = WMap.nodes[end.x][start.y].records[k].weight;
				float phase = WMap.nodes[end.x][start.y].records[k].distorted_phase_time+ glm::abs(WMap.nodes[end.x][start.y].records[k].distorted_phase_space);
				if (time - phase < 0)weight = 0.0f;
				float amplitude = WMap.nodes[end.x][start.y].records[k].amplitude;
				totalAmplitude += amplitude*weight;
				velocity1 += WMap.nodes[end.x][start.y].records[k].direction*weight*amplitude;
			}
			if (totalAmplitude > 0) {
				velocity1 /= totalAmplitude;
			}
			///point 2
			totalAmplitude = 0.0f;
			for (int k = 0; k < WMap.nodes[end.x][end.y].records.size(); k++) {
				float weight = WMap.nodes[end.x][end.y].records[k].weight;
				float phase = WMap.nodes[end.x][end.y].records[k].distorted_phase_time+glm::abs(WMap.nodes[end.x][end.y].records[k].distorted_phase_space);
				if (time - phase <= 0)weight = 0.0f;
				float amplitude = WMap.nodes[end.x][end.y].records[k].amplitude;
				totalAmplitude += amplitude*weight;
				velocity2 += WMap.nodes[end.x][end.y].records[k].direction*amplitude*weight;
			}
			if (totalAmplitude > 0) {
				velocity2 /= totalAmplitude;
			}
			///point 3
			totalAmplitude = 0.0f;
			for (int k = 0; k < WMap.nodes[start.x][end.y].records.size(); k++) {
				float weight = WMap.nodes[start.x][end.y].records[k].weight;
				float phase = WMap.nodes[start.x][end.y].records[k].distorted_phase_time+glm::abs(WMap.nodes[start.x][end.y].records[k].distorted_phase_space);
				if (time - phase < 0)weight = 0.0f;
				float amplitude = WMap.nodes[start.x][end.y].records[k].amplitude;
				totalAmplitude += amplitude*weight;
				velocity3 += WMap.nodes[start.x][end.y].records[k].direction*amplitude*weight;
			}
			if (totalAmplitude > 0) {
				velocity3 /= totalAmplitude;
			}

			///interpolate using IDW
			glm::vec2 velocity = MathUtilities::IDW(start, start + glm::ivec2(1, 0), end, end - glm::ivec2(-1, 0), point, velocity0, velocity1, velocity2, velocity3);
			//calculate the force based on the velocity and the normal vector of the vertex
			if (velocity!=glm::vec2(0.0f)) {
				velocity = glm::normalize(velocity);
			}
			glm::vec2 force = 10.0f*glm::dot(velocity, normal)*normal;
			rigid->forces.push_back(glm::vec3(force.x,0,force.y));
			rigid->netForce += glm::vec3(force.x,0,force.y);
			//calculate the height of the com
		}
		rigid->netForce /=(float)outline.size();
		glm::vec2 point = glm::vec2(rigid->position.x, rigid->position.z);
		glm::ivec2 start = glm::floor(point / WMap.offset);
		glm::ivec2 end = start + glm::ivec2(1, 1);
		float height0 = WMap.getHeight(start.x, start.y, params);
		float height1 = WMap.getHeight(start.x + 1, start.y, params);
		float height2 = WMap.getHeight(end.x, end.y, params);
		float height3 = WMap.getHeight(end.x - 1, end.y, params);
		dynamicObstacles[i].oldRenderingHeight = dynamicObstacles[i].renderingHeight;
		dynamicObstacles[i].renderingHeight = MathUtilities::IDW(start, start + glm::ivec2(1, 0), end, end - glm::ivec2(1, 0), point, height0, height1, height2, height3);
		float oldHeight = dynamicObstacles[i].oldRenderingHeight;
		float newHeight = dynamicObstacles[i].renderingHeight;
		if (glm::distance(oldHeight, newHeight) > 0.1) {
			rigid->netForce.y = (dynamicObstacles[i].renderingHeight - dynamicObstacles[i].oldRenderingHeight);
			//std::cout << "diff is " << rigid->netForce.y << std::endl;
		}
		else if (glm::abs(rigid->position.y)> 0.1f) {
			rigid->netForce.y = -rigid->mass*9.8*glm::sign(rigid->position.y);//gravity
			//std::cout << "netf is " << rigid->netForce.y << std::endl;
		}
		rigid->forces.push_back(glm::vec3(0.0f, rigid->netForce.y, 0.0f));
		//std::cout << "y is " << rigid->netForce.y << std::endl;
	}
	//if (!boat.obstacle.isSelfDriven) {
		Rigidbody* rigid = boat.obstacle.rigidbody;
		std::vector<glm::vec2>& outline =boat.obstacle.immersionOutLine;
		std::vector<glm::vec2>& normals = boat.obstacle.vertexNormals;
		rigid->forces.clear();
		//rigid->netForce = glm::vec3(0.0f);
		//rigid->netTorque = glm::vec3(0.0f);
		for (int j = 0; j < outline.size(); j++) {//for each point of the outline
												  //query the sample map
			glm::vec3 point3D = rigid->geometry.mod->vertices[j];
			glm::vec2 point = glm::vec2(point3D.x, point3D.z);
			glm::vec3 normal3D = rigid->orientation*rigid->bodyspaceGeometry.normals[j];
			glm::vec2 normal = glm::vec2(normal3D.x, normal3D.z);
			if (point.x <= 0 || point.y <= 0 || point.x >= WFMap.size.x*WFMap.offset.x || point.y >= WFMap.size.y*WFMap.offset.y) {
				continue;
			}
			///find the four sampling points
			glm::ivec2 start = glm::floor(point / WMap.offset);
			glm::ivec2 end = start + glm::ivec2(1, 1);
			///calculate total velocity for each of the sampling points
			glm::vec2 velocity0 = glm::vec2(0.0f);
			glm::vec2 velocity1 = glm::vec2(0.0f);
			glm::vec2 velocity2 = glm::vec2(0.0f);
			glm::vec2 velocity3 = glm::vec2(0.0f);
			///point 0
			float totalAmplitude = 0.0f;
			if(start.x>0&&start.x<WMap.size.x&&start.y>0&&start.y<WMap.size.y)
			for (int k = 0; k < WMap.nodes[start.x][start.y].records.size(); k++) {
				float weight = WMap.nodes[start.x][start.y].records[k].weight;
				float phase = WMap.nodes[start.x][start.y].records[k].distorted_phase_time + glm::abs(WMap.nodes[start.x][start.y].records[k].distorted_phase_space);
				if (time - phase < 0)weight = 0.0f;
				float amplitude = WMap.nodes[start.x][start.y].records[k].amplitude;
				totalAmplitude += amplitude*weight;
				velocity0 += WMap.nodes[start.x][start.y].records[k].direction*amplitude*weight;
			}
			if (totalAmplitude > 0) {
				velocity0 /= totalAmplitude;
			}
			///point 1
			totalAmplitude = 0.0f;
			if (end.x>0 && end.x<WMap.size.x&&start.y>0 && start.y<WMap.size.y)
			for (int k = 0; k < WMap.nodes[end.x][start.y].records.size(); k++) {
				float weight = WMap.nodes[end.x][start.y].records[k].weight;
				float phase = WMap.nodes[end.x][start.y].records[k].distorted_phase_time + glm::abs(WMap.nodes[end.x][start.y].records[k].distorted_phase_space);
				if (time - phase < 0)weight = 0.0f;
				float amplitude = WMap.nodes[end.x][start.y].records[k].amplitude;
				totalAmplitude += amplitude*weight;
				velocity1 += WMap.nodes[end.x][start.y].records[k].direction*weight*amplitude;
			}
			if (totalAmplitude > 0) {
				velocity1 /= totalAmplitude;
			}
			///point 2
			totalAmplitude = 0.0f;
			if (end.x>0 && end.x<WMap.size.x&&end.y>0 && end.y<WMap.size.y)
			for (int k = 0; k < WMap.nodes[end.x][end.y].records.size(); k++) {
				float weight = WMap.nodes[end.x][end.y].records[k].weight;
				float phase = WMap.nodes[end.x][end.y].records[k].distorted_phase_time + glm::abs(WMap.nodes[end.x][end.y].records[k].distorted_phase_space);
				if (time - phase <= 0)weight = 0.0f;
				float amplitude = WMap.nodes[end.x][end.y].records[k].amplitude;
				totalAmplitude += amplitude*weight;
				velocity2 += WMap.nodes[end.x][end.y].records[k].direction*amplitude*weight;
			}
			if (totalAmplitude > 0) {
				velocity2 /= totalAmplitude;
			}
			///point 3
			totalAmplitude = 0.0f;
			if (start.x>0 && start.x<WMap.size.x&&end.y>0 && end.y<WMap.size.y)
			for (int k = 0; k < WMap.nodes[start.x][end.y].records.size(); k++) {
				float weight = WMap.nodes[start.x][end.y].records[k].weight;
				float phase = WMap.nodes[start.x][end.y].records[k].distorted_phase_time + glm::abs(WMap.nodes[start.x][end.y].records[k].distorted_phase_space);
				if (time - phase < 0)weight = 0.0f;
				float amplitude = WMap.nodes[start.x][end.y].records[k].amplitude;
				totalAmplitude += amplitude*weight;
				velocity3 += WMap.nodes[start.x][end.y].records[k].direction*amplitude*weight;
			}
			if (totalAmplitude > 0) {
				velocity3 /= totalAmplitude;
			}

			///interpolate using IDW
			glm::vec2 velocity = MathUtilities::IDW(start, start + glm::ivec2(1, 0), end, end - glm::ivec2(-1, 0), point, velocity0, velocity1, velocity2, velocity3);
			//calculate the force based on the velocity and the normal vector of the vertex
			if (velocity != glm::vec2(0.0f)) {
				velocity = glm::normalize(velocity);
			}
			//std::cout << "vel is " << glm::to_string(velocity) << std::endl;
			if (velocity == velocity) {
				glm::vec2 force = (10.0f / rigid->mass)*glm::dot(velocity, normal)*normal;
				rigid->forces.push_back(glm::vec3(force.x, 0, force.y));
				rigid->netForce += glm::vec3(force.x, 0, force.y);
			}
			//calculate the height of the com
		}
		rigid->netForce /= (float)outline.size();
		glm::vec2 point = glm::vec2(rigid->position.x, rigid->position.z);
		glm::ivec2 start = glm::floor(point / WMap.offset);
		glm::ivec2 end = start + glm::ivec2(1, 1);
		float height0 = WMap.getHeight(start.x, start.y, params);
		float height1 = WMap.getHeight(start.x + 1, start.y, params);
		float height2 = WMap.getHeight(end.x, end.y, params);
		float height3 = WMap.getHeight(end.x - 1, end.y, params);
		boat.obstacle.oldRenderingHeight = boat.obstacle.renderingHeight;
		boat.obstacle.renderingHeight = MathUtilities::IDW(start, start + glm::ivec2(1, 0), end, end - glm::ivec2(1, 0), point, height0, height1, height2, height3);
		float oldHeight = boat.obstacle.oldRenderingHeight;
		float newHeight = boat.obstacle.renderingHeight;  
		boat.obstacle.canMoveXZ = true;
		
		if (glm::abs(rigid->position.y) > glm::abs(newHeight)) {
			rigid->netForce.y = -0.1f*rigid->mass*9.8*glm::sign(rigid->position.y);//gravity
			if (rigid->position.y > 0.1f) {
				boat.obstacle.canMoveXZ =false;
			}
			//std::cout << "netf is " << rigid->netForce.y << std::endl;
		}
		else if (glm::distance(oldHeight, newHeight) > 0.1) {
			rigid->netForce.y = 0.1f*(boat.obstacle.renderingHeight - boat.obstacle.oldRenderingHeight);
			//std::cout << "diff is " << rigid->netForce.y << std::endl;
		}
		rigid->forces.push_back(glm::vec3(0.0f, rigid->netForce.y, 0.0f));
	//}
	//	std::cout << "pos is " << glm::to_string(rigid->position) << std::endl;
	//std::cout << "y is " << rigid->netForce.y << std::endl;
}

void WavefrontSystem::UpdateMaps(Wavefront& wavefront)
{
	//for (int k = 0; k < wavefronts.size(); k++) {//for all wavefronts
												 //trapezoid coverage against regular grid implementation
	if (wavefront.VWrite.size() == 0) {
		return;
	}
	for (int l = 0; l < wavefront.VRead.size() - 1; l++) { //for all edges of the wavefront
		glm::vec2 p0 = wavefront.VRead[l].position;
		glm::vec2 p1 = wavefront.VRead[l + 1].position;
		glm::vec2 p2 = wavefront.VWrite[l + 1].position;
		glm::vec2 p3 = wavefront.VWrite[l].position;
		//find the AABB of p0,p1,p2,p3
		std::pair<glm::vec2, glm::vec2> AABB;
		AABB.first.x = glm::max(glm::max(p0.x, p1.x), glm::max(p2.x, p3.x));
		AABB.first.y = glm::max(glm::max(p0.y, p1.y), glm::max(p2.y, p3.y));
		AABB.second.x = glm::min(glm::min(p0.x, p1.x), glm::min(p2.x, p3.x));
		AABB.second.y = glm::min(glm::min(p0.y, p1.y), glm::min(p2.y, p3.y));
		//get the start and end indices on x dimension
		glm::ivec2 start, end;
		start = glm::floor(AABB.second / WFMap.offset) - glm::vec2(1);
		start = glm::clamp(start, glm::ivec2(0, 0), WFMap.size);
		end = glm::ceil(AABB.first / WFMap.offset) + glm::vec2(1);;
		end = glm::clamp(end, glm::ivec2(0, 0), WFMap.size);
		//iterate from start to end

		for (int i = start.x; i < end.x; i++) {
			for (int j = start.y; j < end.y; j++) {
				int wf_id = wavefront.id;

				std::vector<WFHeightMapNodeRecord>::iterator it =
					std::find_if(WFMap.nodes[i][j].records.begin(), WFMap.nodes[i][j].records.end(), [wf_id](auto i) {return i.wavefront_id == wf_id; });
				if (it != WFMap.nodes[i][j].records.end()) {
					continue;
				}

				glm::vec2 query(i, j);
				query = query*WFMap.offset;
				bool lline1 = MathUtilities::isLeft(p0, p1, query);
				bool lline2 = MathUtilities::isLeft(p1, p2, query);
				bool lline3 = MathUtilities::isLeft(p2, p3, query);
				bool lline4 = MathUtilities::isLeft(p3, p0, query);
				bool rline1 = MathUtilities::isRight(p0, p1, query);
				bool rline2 = MathUtilities::isRight(p1, p2, query);
				bool rline3 = MathUtilities::isRight(p2, p3, query);
				bool rline4 = MathUtilities::isRight(p3, p0, query);

				//if(!hasDone){
				//	hasDone = true;
				//	i = 1; j =1 ;
				if ((lline1&&lline2&&lline3&&lline4) || (rline1&&rline2&&rline3&&rline4)) {
					//it is inside the trapezoid
					WFHeightMapNodeRecord record;
					int wavefrontSize = wavefront.ERead.size();
					float amp1 = wavefront.ERead[l].amplitude;
					if (l > 0)amp1 = (amp1 + wavefront.ERead[l - 1].amplitude)*0.5f;
					float amp2 = wavefront.ERead[l].amplitude;
					if (l < wavefrontSize - 1)amp2 = (amp2 + wavefront.ERead[l + 1].amplitude)*0.5f;
					float amp3 = wavefront.EWrite[l].amplitude;
					if (l < wavefrontSize - 1)amp3 = (amp3 + wavefront.EWrite[l + 1].amplitude)*0.5f;
					float amp4 = wavefront.EWrite[l].amplitude;
					if (l > 0)amp4 = (amp4 + wavefront.EWrite[l - 1].amplitude)*0.5f;
					record.amplitude = //bilin_inter
						MathUtilities::IDW
						(p0, p1, p2, p3, query,
							amp1, amp2, amp3, amp4);
					//wavefront.ERead[l].amplitude , wavefront.ERead[l].amplitude, wavefront.EWrite[l].amplitude, wavefront.EWrite[l].amplitude);

					record.travel_time = //bilin_inter
						MathUtilities::IDW
						(p0, p1, p2, p3, query, time - time::deltaTime, time - time::deltaTime, time, time);
					glm::vec2 velocity =
						//bilin_inter
						MathUtilities::IDW
						(p0, p1, p2, p3, query,
							wavefront.VRead[l].velocity, wavefront.VRead[l + 1].velocity, wavefront.VWrite[l + 1].velocity, wavefront.VWrite[l].velocity);
					//interpolate the velocity and position in the VRead and VWrite nodes for l
					record.angular_frequency = wavefront.angular_frequency;

					record.velocity = velocity;
					record.wavefront_id = wavefront.id;
					WFMap.nodes[i][j].records.push_back(record);
					if (WFMap.nodes[i][j].records.size() > 5) {
						WFMap.nodes[i][j].RemoveWorstRecord();
					}
					//update the underlying map
					//////////////////////
					glm::vec2 direction = glm::normalize(velocity);
					glm::vec2 normVelocity = direction;
					if (glm::abs(direction.x) >= glm::abs(direction.y)) {
						direction = glm::vec2(glm::sign(direction.x), glm::sign(direction.y)*glm::abs(direction.y / direction.x));
					}
					else {
						//direction = glm::vec2(glm::sign(direction.y), glm::sign(direction.x)*glm::abs(direction.x / direction.y));
						direction = glm::vec2(glm::sign(direction.x)*glm::abs(direction.x / direction.y), glm::sign(direction.y));
					}
					glm::vec2 direction_abs = glm::abs(direction);
					//std::cout << "direction is " <<glm::to_string(direction)<< std::endl;
					//glm::vec2 direction_abs = direction;
					glm::vec2 perpedicular = glm::vec2(-direction.y, direction.x);
					glm::ivec2 limit = WMap.size / WFMap.size;
					glm::vec2 current = glm::ivec2(i*limit.x, j*limit.y);
					glm::vec2 begin = glm::ivec2(i*limit.x, j*limit.y);
					glm::vec2 end = current + direction*(float)(limit.x - 1);
					//std::cout << "begin is " << glm::to_string(begin) << " and end is " << glm::to_string(end) << std::endl;
					bool isDiagonal = (glm::distance(current, current - direction) > 1);
					glm::vec2 nextCurrent = current;
					float directionsCrossPos = MathUtilities::cross(direction, -perpedicular);
					float directionsCrossNeg = MathUtilities::cross(direction, perpedicular);
					float space_phase_step = (direction_abs.x*WMap.offset.x + direction_abs.y*WMap.offset.y) / glm::length(velocity);
					float startingPhase = 0.0f;
					float endingPhase = space_phase_step*(limit.x - 1);

					for (int k = 0; k < limit.x; k++) {//assumming that the grid is rectangular
													   //std::cout << "current is " << glm::to_string(current) << std::endl;

													   //	std::cout << "distance is " << glm::distance(current, oldCurrent) << std::endl;
													   //std::cout << "is diagonal is " << isDiagonal << std::endl;
						nextCurrent = current + direction;
						isDiagonal = (glm::distance(glm::floor(current), glm::floor(nextCurrent)) > 1);
						WaterHeightMapNodeRecord wrecord;
						wrecord.direction = direction;
						wrecord.dampingCoeff = wavefront.dampingCoeff;
						wrecord.amplitude = record.amplitude;
						wrecord.angular_frequency = record.angular_frequency;
						wrecord.init_phase_space = endingPhase*glm::distance(begin, current) / (glm::distance(begin, end)) +
							startingPhase*(glm::distance(end, current) / (glm::distance(begin, end)));
						//0.0f;//(k)*space_phase_step;
						//std::cout << "phase space is "<< wrecord.init_phase_space << std::endl;
						wrecord.noiseMagnitude = 0.0f;
						wrecord.isCrestWave = wavefront.isCrestWave;
						if(!wavefront.isDestructable)
							wrecord.noiseMagnitude = 10.0f*glm::length(record.velocity);
						wrecord.init_phase_time = record.travel_time;
						wrecord.distorted_phase_time = wrecord.init_phase_time;
						
						wrecord.distorted_phase_space = wrecord.init_phase_space*MathUtilities::normalizedNoise(wrecord.noiseMagnitude);
						glm::vec2 currentPerpendicular = glm::vec2(0, 0);
						wrecord.weight = 1.0f;
						if (current.x >= 0 && current.x < WMap.size.x&&current.y >= 0 && current.y < WMap.size.y) {
							WMap.nodes[glm::floor(current.x)][glm::floor(current.y)].Add(wrecord,time::currentFrameTime);
							WMap.nodes[glm::floor(current.x)][glm::floor(current.y)].weight_sum += wrecord.weight;
						}
						glm::vec2 previousPerpendicular;
						bool isDiagonalPer;
						for (int l = 0; l < limit.y; l++) {
							previousPerpendicular = currentPerpendicular;
							currentPerpendicular += perpedicular;
							float uPos = MathUtilities::cross((current + currentPerpendicular) - (current - direction), -perpedicular) / (directionsCrossPos);
							//std::cout << "uPos is " << uPos << std::endl;
							glm::vec2 intersectionPos = (current - direction) + direction*uPos;
							float uNeg = MathUtilities::cross((current - currentPerpendicular) - (current - direction), perpedicular) / (directionsCrossNeg);
							glm::vec2 intersectionNeg = (current - direction) + direction*uNeg;
							//std::cout << "direction is " << glm::to_string(direction) << " and perpendicular is " << glm::to_string(perpedicular) << std::endl;
							//std::cout << "current is " << glm::to_string(current-direction) << " and perpendicular is " << glm::to_string(current+currentPerpendicular) << std::endl;
							//std::cout << "intersection is " << glm::to_string(intersection) << std::endl;
							//std::cout << "is diagonal is " << isDiagonalPer << " and indices are " << glm::to_string(currentPerpendicular) << " vs "<<glm::to_string(previousPerpendicular) << std::endl;


							glm::vec2 indexPos = current + currentPerpendicular;
							glm::vec2 indexNeg = current - currentPerpendicular;
							wrecord.weight -= 0.5f / ((float)limit.x);
							//wrecord.init_phase_space = 0;//(k+0.5)*space_phase_step;
							wrecord.noiseMagnitude = 0.0f;
							wrecord.isCrestWave = wavefront.isCrestWave;
							if (!wavefront.isDestructable)
							wrecord.noiseMagnitude = 10.0f*glm::length(record.velocity);
							wrecord.distorted_phase_space = wrecord.init_phase_space*MathUtilities::normalizedNoise(wrecord.noiseMagnitude);
							isDiagonalPer = (glm::distance(glm::floor(current - currentPerpendicular), glm::floor(current - previousPerpendicular)) > 1);
							if (isDiagonal&&isDiagonalPer) {

								glm::vec2 indexMiddleNeg = glm::round((current - currentPerpendicular + current - previousPerpendicular)*0.5f + normVelocity*1.4114f*0.5f);
								uNeg = MathUtilities::cross((indexMiddleNeg)-(current - direction), perpedicular) / (directionsCrossNeg);
								intersectionNeg = (current - direction) + direction*uNeg;
								float newWeight = 1 - glm::distance(indexMiddleNeg, intersectionNeg) / glm::distance(intersectionNeg, current + (float)limit.x*direction);
								//std::cout << "Neg weight is " << wrecord.weight << " accurate is " << newWeight << std::endl;
								wrecord.weight = newWeight;
								wrecord.init_phase_space = endingPhase*glm::distance(begin, intersectionNeg) / (glm::distance(begin, end)) +
									startingPhase*(glm::distance(end, intersectionNeg) / (glm::distance(begin, end)));
								wrecord.noiseMagnitude = 0.0f;
								wrecord.isCrestWave = wavefront.isCrestWave;
								if (!wavefront.isDestructable)
								wrecord.noiseMagnitude = 10.0f*glm::length(record.velocity);
								wrecord.distorted_phase_space = wrecord.init_phase_space*MathUtilities::normalizedNoise(wrecord.noiseMagnitude);

								//std::cout<<"current "
								//std::cout << "points are " << glm::to_string(glm::floor(current) - glm::floor(currentPerpendicular)) << " vs " << glm::to_string(glm::floor(current - previousPerpendicular)) << std::endl;
								//std::cout << "midpoint " << glm::to_string((glm::floor(current) - glm::floor(currentPerpendicular) + glm::floor(current) - glm::floor(previousPerpendicular))*0.5f) << std::endl;
								if (indexMiddleNeg.x > 0 && indexMiddleNeg.x < WMap.size.x&&indexMiddleNeg.y>0 && indexMiddleNeg.y < WMap.size.y) {
									WMap.nodes[indexMiddleNeg.x][indexMiddleNeg.y].Add(wrecord,time::currentFrameTime);

									WMap.nodes[indexMiddleNeg.x][indexMiddleNeg.y].weight_sum = -1.0f;
									//std::cout << "adding to " << glm::to_string(indexMiddleNeg) << "(diagonal case)" << std::endl;
									//std::cout << "perp current+ is " << glm::to_string(indexNeg) << std::endl;
									//std::cout << "diagonal:intersection point is " << glm::to_string(intersectionNeg) << std::endl;
								}
							}
							isDiagonalPer = (glm::distance(glm::floor(current + currentPerpendicular), glm::floor(current + previousPerpendicular)) > 1);
							if (isDiagonal&&isDiagonalPer) {
								//std::cout << "current is " << glm::to_string(current) << " and current pep is " << glm::to_string(currentPerpendicular) << "a nd previous pep is" << glm::to_string(previousPerpendicular) << std::endl;
								glm::vec2 indexMiddlePos = glm::round((current + currentPerpendicular + current + previousPerpendicular)*0.5f + normVelocity*1.4114f*0.5f);
								uPos = MathUtilities::cross((indexMiddlePos)-(current - direction), perpedicular) / (directionsCrossPos);
								intersectionPos = (current - direction) - direction*uPos;
								float newWeight = 1 - glm::distance(indexMiddlePos, intersectionPos) / glm::distance(intersectionPos, current + (float)limit.x*direction);
								//std::cout << "Pos weight is " << wrecord.weight << " accurate is " << newWeight << std::endl;
								wrecord.weight = newWeight;
								wrecord.init_phase_space = endingPhase*glm::distance(begin, intersectionPos) / (glm::distance(begin, end)) +
									startingPhase*(glm::distance(end, intersectionPos) / (glm::distance(begin, end)));
								wrecord.noiseMagnitude = 0.0f;
								wrecord.isCrestWave = wavefront.isCrestWave;
								if (!wavefront.isDestructable)
								wrecord.noiseMagnitude = 10.0f*glm::length(record.velocity);
								wrecord.distorted_phase_space = wrecord.init_phase_space*MathUtilities::normalizedNoise(wrecord.noiseMagnitude);
								//std::cout<<"current "
								//std::cout << "points are " << glm::to_string(glm::floor(current) + glm::floor(currentPerpendicular)) << " vs " << glm::to_string(glm::floor(current + previousPerpendicular)) << std::endl;
								//std::cout << "midpoint " << glm::to_string((glm::floor(current) + glm::floor(currentPerpendicular) + glm::floor(current) + glm::floor(previousPerpendicular))*0.5f) << std::endl;
								if (indexMiddlePos.x > 0 && indexMiddlePos.x < WMap.size.x&&indexMiddlePos.y>0 && indexMiddlePos.y < WMap.size.y) {
									WMap.nodes[indexMiddlePos.x][indexMiddlePos.y].Add(wrecord,time::currentFrameTime);
									WMap.nodes[indexMiddlePos.x][indexMiddlePos.y].weight_sum = -1.0f;
									//std::cout << "diagonal:phase is " << wrecord.init_phase_space << std::endl;
									//std::cout << "diagonal:intersection point is " << glm::to_string(intersectionPos) << std::endl;
								}
							}


							wrecord.init_phase_space = 0.0f;// k*space_phase_step;
							wrecord.noiseMagnitude = 0.0f;
							wrecord.isCrestWave = wavefront.isCrestWave;
							if (!wavefront.isDestructable)
							wrecord.noiseMagnitude = 10.0f*glm::length(record.velocity);
							wrecord.distorted_phase_space = wrecord.init_phase_space*MathUtilities::normalizedNoise(wrecord.noiseMagnitude);
							wrecord.weight -= 0.5f / ((float)limit.x);
							if (indexPos.x > 0 && indexPos.x < WMap.size.x
								&&indexPos.y>0 && indexPos.y < WMap.size.y) {
								//indexPos = glm::round(indexPos);
								//std::cout << "adding to " << glm::to_string(current + currentPerpendicular) << std::endl;
								uPos = MathUtilities::cross((indexPos)-(current - direction), perpedicular) / (directionsCrossPos);
								intersectionPos = (current - direction) - direction*uPos;
								float newWeight = 1 - glm::distance(indexPos, intersectionPos) / glm::distance(intersectionPos, current + (float)limit.x*direction);
								wrecord.weight = newWeight;
								wrecord.init_phase_space = endingPhase*glm::distance(begin, intersectionPos) / (glm::distance(begin, end)) +
									startingPhase*(glm::distance(end, intersectionPos) / (glm::distance(begin, end)));
								wrecord.noiseMagnitude = 0.0f;
								wrecord.isCrestWave = wavefront.isCrestWave;
								if (!wavefront.isDestructable)
								wrecord.noiseMagnitude = 10.0f*glm::length(record.velocity);
								wrecord.distorted_phase_space = wrecord.init_phase_space*MathUtilities::normalizedNoise(wrecord.noiseMagnitude);
								//std::cout << "PosNorm weight is " << wrecord.weight << " accurate is " << newWeight << std::endl;
								WMap.nodes[current.x + currentPerpendicular.x][current.y + currentPerpendicular.y].Add(wrecord,time::currentFrameTime);
								WMap.nodes[current.x + currentPerpendicular.x][current.y + currentPerpendicular.y].weight_sum += wrecord.weight;
								//std::cout << "normal:phase is " << wrecord.init_phase_space << std::endl;
								//std::cout << "normal:intersection point is " << glm::to_string(intersectionPos) << std::endl;
							}
							if (indexNeg.x > 0 && indexNeg.x < WMap.size.x
								&&indexNeg.y>0 && indexNeg.y < WMap.size.y) {
								uNeg = MathUtilities::cross((indexNeg)-(current - direction), perpedicular) / (directionsCrossNeg);
								intersectionNeg = (current - direction) + direction*uNeg;
								float newWeight = 1 - glm::distance(indexNeg, intersectionNeg) / glm::distance(intersectionNeg, current + (float)limit.x*direction);
								wrecord.weight = newWeight;
								wrecord.init_phase_space = endingPhase*glm::distance(begin, intersectionNeg) / (glm::distance(begin, end)) +
									startingPhase*(glm::distance(end, intersectionNeg) / (glm::distance(begin, end)));
								wrecord.noiseMagnitude = 0.0f;
								wrecord.isCrestWave = wavefront.isCrestWave;
								if (!wavefront.isDestructable)
								wrecord.noiseMagnitude = 10.0f*glm::length(record.velocity);
								wrecord.distorted_phase_space = wrecord.init_phase_space*MathUtilities::normalizedNoise(wrecord.noiseMagnitude);
								//std::cout << "NegNorm weight is " << wrecord.weight << " accurate is " << newWeight << std::endl;
								//	std::cout << "normal:intersection point is " << glm::to_string(intersectionNeg) << std::endl;
								WMap.nodes[current.x - currentPerpendicular.x][current.y - currentPerpendicular.y].Add(wrecord,time::currentFrameTime);
								WMap.nodes[current.x - currentPerpendicular.x][current.y - currentPerpendicular.y].weight_sum += wrecord.weight;
								//std::cout << "adding to " << glm::to_string(current-currentPerpendicular) << "(normal case)" << std::endl;
							}

						}

						current += direction;
					}

				}
			}
		}
	}
}
