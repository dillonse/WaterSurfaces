#pragma once
#include "Particle.h"
#include "Rigidbody.h"

class UnaryForce
{
public:
	 virtual glm::vec3 computeForce(Particle& p)=0;
	 virtual glm::vec3 computeForce(Rigidbody& r) = 0;
};

class GravityForce : public UnaryForce {
public: 
	glm::vec3 gravityAcceleration = glm::vec3(0, -1, 0)*9.8f;
private:
	glm::vec3 computeForce(Particle & p);
	glm::vec3 computeForce(Rigidbody& r);
};

class DragForce : public UnaryForce {
	float DragCoefficient=1.0f;
	glm::vec3 computeForce(Particle& p);
	glm::vec3 computeForce(Rigidbody& r);
};

class FanForce : public UnaryForce {
	glm::vec3 position;
	glm::vec3 normal;
	float strength;
	glm::vec3 computeForce(Particle& p);
	glm::vec3 computeForce(Rigidbody& r);
public:
	FanForce(glm::vec3 position, glm::vec3 normal,float strength) :position(position), normal(normal),strength(strength) { ; }
};

class RepulsiveForce : public UnaryForce {
	float strength;
	glm::vec3 position;
	glm::vec3 computeForce(Particle& p);
	glm::vec3 computeForce(Rigidbody& r);
public:
	RepulsiveForce(glm::vec3 position, float strength) : position(position),strength(strength) {;}
};

class ImpulsiveForce : public UnaryForce {
	float strength;
	glm::vec3 position;
	glm::vec3 computeForce(Particle& p);
	glm::vec3 computeForce(Rigidbody& r);
public:
	ImpulsiveForce(glm::vec3 position, float strength) : position(position), strength(strength) { ; }
};

