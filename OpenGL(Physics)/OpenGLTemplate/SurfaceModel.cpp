#include "SurfaceModel.h"
#include <iostream>
#include <random>
//random generators
std::default_random_engine generatorr;
std::uniform_real_distribution<float> distributionn(-1.0f,1.0f);

SurfaceModel::SurfaceModel(VolumeModel & volumeModel)
{
	//allocate the heightmap
	heightMap.resize(volumeModel.dimension -1);
	forces.resize(volumeModel.dimension - 1);
	verticalVelocities.resize(volumeModel.dimension - 1);
	horizontalVelocities.resize(volumeModel.dimension - 1);
	for (int i = 0; i < heightMap.size(); i++) {
		heightMap[i].resize(volumeModel.dimension -1);
		forces[i].resize(volumeModel.dimension - 1);
		verticalVelocities[i].resize(volumeModel.dimension - 1);
		horizontalVelocities[i].resize(volumeModel.dimension - 1);
	}
	//set the dimension
	dimension = volumeModel.dimension -1;
	//set the volume model
	this->volumeModel = &volumeModel;
	//set the heights of the heightMap
	updateHeights();
}

void SurfaceModel::updateHeights()
{
	for (int i = 0; i < dimension; i++) {
		for (int j = 0; j < dimension; j++) {
			heightMap[i][j] = 
			volumeModel->columns[i][j].calculateHeight(volumeModel->dx,volumeModel->dy)+
			volumeModel->columns[i][j+1].calculateHeight(volumeModel->dx, volumeModel->dy)+
			volumeModel->columns[i+1][j].calculateHeight(volumeModel->dx, volumeModel->dy)+
			volumeModel->columns[i+1][j+1].calculateHeight(volumeModel->dx, volumeModel->dy);
			heightMap[i][j] *= 0.25;
		}
	}
}

void SurfaceModel::updateExternalPressures()
{
	for (int i = 0; i < volumeModel->dimension; i++) {
		for (int j = 0; j < volumeModel->dimension; j++) {
			volumeModel->columns[i][j].externalPressure = 0.0f;
		}
	}
	for (int i = 0; i <dimension; i++) {
		for (int j = 0; j <dimension; j++) {
			volumeModel->columns[i][j].externalPressure += -(forces[i][j] / (4.0f*volumeModel->dx*volumeModel->dy));
			volumeModel->columns[i+1][j].externalPressure += -(forces[i][j] / (4.0f*volumeModel->dx*volumeModel->dy));
			volumeModel->columns[i][j+1].externalPressure += -(forces[i][j] / (4.0f*volumeModel->dx*volumeModel->dy));
			volumeModel->columns[i + 1][j+1].externalPressure += -(forces[i][j] / (4.0f*volumeModel->dx*volumeModel->dy));
		}
	}
}

void SurfaceModel::updateVerticalVelocities()
{
	for (int i = 0; i < dimension; i++) {
		for (int j = 0; j < dimension; j++) {
			verticalVelocities[i][j] = volumeModel->calculateColumnVerticalVelocity(glm::ivec2(i, j)) +
				volumeModel->calculateColumnVerticalVelocity(glm::ivec2(i, j + 1)) +
				volumeModel->calculateColumnVerticalVelocity(glm::ivec2(i + 1, j)) +
				volumeModel->calculateColumnVerticalVelocity(glm::ivec2(i + 1, j + 1));
			verticalVelocities[i][j] *= 0.25f;
		}
	}
}

void SurfaceModel::updateHorizontalVelocities()
{
	for (int i = 0; i < dimension; i++) {
		for (int j = 0; j < dimension; j++) {
			horizontalVelocities[i][j] = volumeModel->calculateColumnHorizontalVelocity(glm::ivec2(i, j));
		}
	}

}

float linearInterpolation(glm::vec4 p1, glm::vec4 p2, glm::vec3 p) {
	//get the distance between p1 and p2
	float p1p2 = glm::distance(p1, p2);
	//get the distance between p1 and p
	float p1p = glm::distance(glm::vec3(p1), p);
	//set alpha as the normalized p1p
	float a = p1p / p1p2;
	return p2.w*a + (1 - a)*p1.w;
}

float bilinearInterpolation(glm::vec4 p1, glm::vec4 p2, glm::vec4 p3, glm::vec4 p4, glm::vec3 p) {
	//get the point p12 which is the projection of p on line p1p2
	glm::vec4 p12 =glm::vec4(glm::vec3(p1) + (glm::dot(p-glm::vec3(p1),glm::vec3(p2-p1))/glm::dot(glm::vec3(p2 - p1), glm::vec3(p2 - p1)))*glm::vec3(p2-p1),0.0f);
	p12.w = linearInterpolation(p1, p2, glm::vec3(p12));
	//get the point p43 which is the projection of p on line p4p3
	glm::vec4 p43 = glm::vec4(glm::vec3(p4) + (glm::dot(p - glm::vec3(p4), glm::vec3(p3 - p4)) / glm::dot(glm::vec3(p3 - p4), glm::vec3(p3 - p4)))*glm::vec3(p3 - p4), 0.0f);
	p43.w = linearInterpolation(p4, p3, glm::vec3(p43));
	//interpolate between p12 and p43
	return linearInterpolation(p12, p43, p);

}

void SurfaceModel::updateParticleSystem(ParticleSystem & system)
{
	float verticalSpeedThreshold = 0.01;
	int particle_num_limit = 5000;
	float splash_radius = 0.25;
	//@TODO check for collision with water surface and remove particles
	//instantiate particles using a threshold
	for (int i = 0; i < dimension; i++) {
		for (int j = 0; j < dimension; j++) {
			if (verticalVelocities[i][j] > verticalSpeedThreshold) {
				//create a uniform distribution for the positioning of the particle
				float x = splash_radius*distributionn(generatorr);
				float z = splash_radius*distributionn(generatorr);
				if ((i == 0 && x < 0) || (i == dimension - 1 && x > 0)) {
					x = -x;
				}
				if ((j==0&&z<0) || (j==dimension-1&&z>0)) {
					z = -z;
				}
				glm::vec4 p1, p2, p3, p4;
				glm::vec3 p;
				glm::vec2 hv1, hv2, hv3, hv4;
				p1 = glm::vec4(0.0, 0.0, 0.0, 0.0f);
				p2 = glm::vec4(1.0, 0.0, 0.0, 0.0f);
				p3 = glm::vec4(1.0, 0.0, 1.0, 0.0f);
				p4 = glm::vec4(0.0, 0.0, 1.0, 0.0f);
				
				//interpolate with the appropriate set of control points
				//interpolate the velocity
				if (x < 0 && z < 0) {
					p1.w = verticalVelocities[i - 1][j - 1];
					p2.w = verticalVelocities[i][ j - 1];
					p3.w = verticalVelocities[i][j];
					p4.w = verticalVelocities[i - 1][j];
					p = glm::vec3(1 - x, 0, 1 - z);
					hv1 = horizontalVelocities[i - 1][j - 1];
					hv2 = horizontalVelocities[i][j - 1];
					hv3 = horizontalVelocities[i][j];
					hv4 = horizontalVelocities[i - 1][j];
				}
				else if (x > 0 && z < 0) {
					p1.w = verticalVelocities[i][j - 1];
					p2.w = verticalVelocities[i + 1][j - 1];
					p3.w = verticalVelocities[i + 1][j];
					p4.w = verticalVelocities[i][j];
					p = glm::vec3(x, 0, 1 - z);
					hv1 = horizontalVelocities[i][j - 1];
					hv2 = horizontalVelocities[i+1][j - 1];
					hv3 = horizontalVelocities[i+1][j];
					hv4 = horizontalVelocities[i][j];
				}
				else if (x < 0 && z > 0) {
					p1.w = verticalVelocities[i - 1][j];
					p2.w = verticalVelocities[i][j];
					p3.w = verticalVelocities[i][j + 1];
					p4.w = verticalVelocities[i - 1][j + 1];
					p = glm::vec3(1-x, 0,z);
					hv1 = horizontalVelocities[i - 1][j];
					hv2 = horizontalVelocities[i][j];
					hv3 = horizontalVelocities[i][j+1];
					hv4 = horizontalVelocities[i - 1][j+1];
				}
				else if (x > 0 && z > 0) {
					p1.w = verticalVelocities[i][j];
					p2.w = verticalVelocities[i + 1][j];
					p3.w = verticalVelocities[i + 1][j + 1];
					p4.w = verticalVelocities[i][j + 1];
					p = glm::vec3(x, 0,z);
					hv1 = horizontalVelocities[i][j];
					hv2 = horizontalVelocities[i+1][j];
					hv3 = horizontalVelocities[i+1][j+1];
					hv4 = horizontalVelocities[i][j+1];
				}
				float verticalSpeed = bilinearInterpolation(p1, p2, p3, p4,p);
				p1.w = hv1.x; p2.w = hv2.x; p3.w = hv3.x; p4.w = hv4.x;
				float horizontalSpeedX = bilinearInterpolation(p1, p2, p3, p4, p);
				p1.w = hv1.y; p2.w = hv2.y; p3.w = hv3.y; p4.w = hv4.y;
				float horizontalSpeedZ = bilinearInterpolation(p1, p2, p3, p4, p);
				system.particles.push_back(Particle(glm::vec3(i+x, heightMap[i][j], j+z),50.0f*glm::vec3(horizontalSpeedX,verticalSpeed,horizontalSpeedZ), 10000.0f, 100000.0f));
			}
		}
	}
	if (system.particles.size() > particle_num_limit) {
		system.particles.erase(system.particles.begin(), system.particles.begin() + (system.particles.size() - particle_num_limit));
	}
	//std::cout << "size is " << system.particles.size() << std::endl;
	
}

float triangleArea(glm::vec3 p1, glm::vec3 p2, glm::vec3 p3) {
	return glm::length(glm::cross(p2 - p1,p3 - p1))*0.5;
}

bool isInsideRect(glm::vec3 p1, glm::vec3 p2, glm::vec3 p3, glm::vec3 p4,glm::vec3 p) {
	//get the area of p1p2p3p4
	float p1p2p3p4 = triangleArea(p1, p2, p3) + triangleArea(p3, p4, p1);
	//get the areas p1p2p, p2p3p, p3p4p, p4p1p
	float p1p2 = triangleArea(p1, p2, p);
	float p2p3 = triangleArea(p2, p3, p);
	float p3p4 = triangleArea(p3, p4, p);
	float p4p1 = triangleArea(p4, p1, p);
	return glm::abs(p1p2p3p4 - (p1p2 + p2p3 + p3p4 + p4p1)) < 0.0001;
}

void calculateWeights(glm::vec4& p1, glm::vec4& p2, glm::vec4& p3, glm::vec4& p4, glm::vec4 p) {
	//get the projection of p on p12
	glm::vec4 p12 = glm::vec4(glm::vec3(p1) + (glm::dot(glm::vec3(p) - glm::vec3(p1), glm::vec3(p2 - p1)) / glm::dot(glm::vec3(p2 - p1), glm::vec3(p2 - p1)))*glm::vec3(p2 - p1), 0.0f);
	//get the projection of p on p23
	glm::vec4 p23 = glm::vec4(glm::vec3(p2) + (glm::dot(glm::vec3(p) - glm::vec3(p2), glm::vec3(p3 - p2)) / glm::dot(glm::vec3(p3 - p2), glm::vec3(p3 - p2)))*glm::vec3(p3 - p2), 0.0f);
	//get the projection of p on p34
	glm::vec4 p34 = glm::vec4(glm::vec3(p3) + (glm::dot(glm::vec3(p) - glm::vec3(p3), glm::vec3(p4 - p3)) / glm::dot(glm::vec3(p4 - p3), glm::vec3(p4 - p3)))*glm::vec3(p4 - p3), 0.0f);
	//get the projection of p on p41
	glm::vec4 p41 = glm::vec4(glm::vec3(p4) + (glm::dot(glm::vec3(p) - glm::vec3(p4), glm::vec3(p1 - p4)) / glm::dot(glm::vec3(p1 - p4), glm::vec3(p1 - p4)))*glm::vec3(p1 - p4), 0.0f);
	
	//get the area of p1-p12-p-p41
	float w1 = triangleArea(p1, p12, p) + triangleArea(p, p41, p1);
	//get the area of p12-p2-p23-p
	float w2 = triangleArea(p12, p2, p23) + triangleArea(p23, p, p12);
	//get the area of p23-p3-p34-p
	float w3 = triangleArea(p23, p3, p) + triangleArea(p3, p34, p);
	//get the area of p34-p4-p41-p
	float w4 = triangleArea(p34, p4, p) + triangleArea(p4, p41, p);
	//get the area of p1p2p3p4
	float w = triangleArea(p1, p2, p3)+triangleArea(p3,p4,p1);
 	//std::cout << "w1:" << w1 << ", w2:" << w2 << ", w3:" << w3 << " ,w4:" << w4 << std::endl;
	w1 = w1 / w;
	w2 = w2 / w;
	w3 = w3 / w;
	w4 = w4 / w;
	p1.w = w3; p2.w = w4; p3.w = w1; p4.w = w2;
	//std::cout << "w1:" << w1 << ", w2:" << w2 << ", w3:" << w3 << " ,w4:" << w4 << std::endl;
	
}

void SurfaceModel::updateCollisions(RigidbodySystem & system)
{
	//clean up the forces
	for (int i = 0; i < dimension; i++) {
		for (int j = 0; j < dimension; j++) {
			forces[i][j] = 0.0f;
		}
	}
	//dummy way, compare each rect with each center of mass
	for (int i = 0; i < dimension - 1; i++) {
		for (int j = 0; j < dimension - 1; j++) {
			glm::vec4 p1 = glm::vec4(volumeModel->dx*i,0,volumeModel->dy*j,0.0f);
			glm::vec4 p2 = glm::vec4(volumeModel->dx*(i + 1),0, volumeModel->dy*(j),0.0f);
			glm::vec4 p3 = glm::vec4(volumeModel->dx*(i + 1),0, volumeModel->dy*(j + 1),0.0f);
			glm::vec4 p4 = glm::vec4(volumeModel->dx*(i),0, volumeModel->dy*(j + 1),0.0f);
			//check each rigidbody's c.o.m.
			for (int k = 0; k < system.rigidBodies.size(); k++) {
				glm::vec4 p = glm::vec4(system.rigidBodies[k].position,0.0f);
				float maxHeight = glm::max(p1.y, glm::max(p2.y, glm::max(p3.y, p4.y)));
				float minHeight = glm::min(p1.y, glm::min(p2.y, glm::min(p3.y, p4.y)));
				if (p.y < maxHeight&&p.y&&minHeight-p.y<1.0f) {
					p.y = 0;
					if (isInsideRect(p1, p2, p3, p4, p)) {
						//handle collision
						calculateWeights(p1, p2, p3, p4, p);
						//distribute the force of c.o.m accordingly
						forces[i][j] += -p1.w*system.rigidBodies[k].netForce.y;
						forces[i+1][j] += -p2.w*system.rigidBodies[k].netForce.y;
						forces[i + 1][j+1] += -p3.w*system.rigidBodies[k].netForce.y;
						forces[i][j+1] += -p4.w*system.rigidBodies[k].netForce.y;
					}
				}
			}
			//check for collision in the x-z projection
			
		}
	}
}

void SurfaceModel::Render(material mat)
{
	//clear color and depth buffer 
	instance inst;
	inst.mat = &mat;
	inst.BindMaterial();
	glColor3f(0.0f, 0.0f, 1.0f); //blue color

	
	for (int i = 0; i < dimension - 1; i++) {
		for (int j = 0; j < dimension - 1; j++) {
			glBegin(GL_LINE_LOOP);//start drawing a line loop
			glVertex3f(i,heightMap[i][j],j);
			glVertex3f(i+1, heightMap[i+1][j], j);
			glVertex3f(i+1, heightMap[i+1][j+1], j+1);
			glVertex3f(i, heightMap[i][j+1], j+1);
			glEnd();
		}
	}
}
