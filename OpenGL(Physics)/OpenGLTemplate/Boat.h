#pragma once
#include "instance.h"
#include "DynamicObstacle.h"
class Boat
{
public:
	Boat(WaterHeightMap map):obstacle(map){};
	instance otherInst;
	instance mainBody;
	DynamicObstacle obstacle;
	void Init();
	void Render();
};