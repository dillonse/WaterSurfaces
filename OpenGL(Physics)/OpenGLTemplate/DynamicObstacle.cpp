#include "DynamicObstacle.h"
#include "Collider.h"
#include <iostream>
#include <glm\gtx\string_cast.hpp>
#include <algorithm>
#include <glm\gtc\matrix_transform.hpp>
#include "MathUtilities.h"
#include "WFHeightmap.h"
#include "global.h"

int DynamicObstacle::instance_counter = 0;

DynamicObstacle::DynamicObstacle(WaterHeightMap outMap) :map(outMap) {
	Obstacle_id = instance_counter++;
	/*for (int i = 0; i < map.size.x; i++) {
		for (int j = 0; j < map.size.y; j++) {
			map.nodes[i][j].records.resize(1);
		}
	}*/
}

glm::vec2 ffindCrossingPoint(glm::vec2 p0, glm::vec2 p1, glm::vec2 q0, glm::vec2 q1) {
	glm::vec2 p = p0;
	glm::vec2 q = q0;
	glm::vec2 direction_p = glm::normalize(p1 - p0);
	glm::vec2 direction_q = glm::normalize(q1 - q0);
	float scalar_q = MathUtilities::cross(q - p, direction_p) / MathUtilities::cross(direction_p, direction_q);
	return q + direction_q*scalar_q;
}

DynamicObstacle::Location DynamicObstacle::findLocation(glm::vec2 p0, glm::vec2 p1, glm::vec2 query)
{
	Location location;
	float sign = (p1.x - p0.x)*(query.y - p0.y) - (query.x - p0.x)*(p1.y - p0.y);
	if (sign > 0) {
		location = LOCATION_OUTSIDE;
	}
	else if (sign == 0) {
		location = LOCATION_ON;
	}
	else {
		location = LOCATION_INSIDE;
	}
	return location;
}

void DynamicObstacle::UpdateMap(Wavefront& wavefront,WFHeightMap& wfmap,float time)
{
	//for (int k = 0; k < wavefronts.size(); k++) {//for all wavefronts
	//trapezoid coverage against regular grid implementation
	if (wavefront.VWrite.size() == 0) {
		return;
	}
	for (int l = 0; l < wavefront.VRead.size() - 1; l++) { //for all edges of the wavefront
		glm::vec2 p0 = wavefront.VRead[l].position;
		glm::vec2 p1 = wavefront.VRead[l + 1].position;
		glm::vec2 p2 = wavefront.VWrite[l + 1].position;
		glm::vec2 p3 = wavefront.VWrite[l].position;
		//find the AABB of p0,p1,p2,p3
		std::pair<glm::vec2, glm::vec2> AABB;
		AABB.first.x = glm::max(glm::max(p0.x, p1.x), glm::max(p2.x, p3.x));
		AABB.first.y = glm::max(glm::max(p0.y, p1.y), glm::max(p2.y, p3.y));
		AABB.second.x = glm::min(glm::min(p0.x, p1.x), glm::min(p2.x, p3.x));
		AABB.second.y = glm::min(glm::min(p0.y, p1.y), glm::min(p2.y, p3.y));
		//get the start and end indices on x dimension
		glm::ivec2 start, end;
		start = glm::floor(AABB.second / wfmap.offset) - glm::vec2(1);
		start = glm::clamp(start, glm::ivec2(0, 0), wfmap.size);
		end = glm::ceil(AABB.first / wfmap.offset) + glm::vec2(1);;
		end = glm::clamp(end, glm::ivec2(0, 0), wfmap.size);
		//iterate from start to end

		for (int i = start.x; i < end.x; i++) {
			for (int j = start.y; j < end.y; j++) {
				int wf_id = wavefront.id;

				std::vector<WFHeightMapNodeRecord>::iterator it =
					std::find_if(wfmap.nodes[i][j].records.begin(), wfmap.nodes[i][j].records.end(), [wf_id](auto i) {return i.wavefront_id == wf_id; });
				if (it != wfmap.nodes[i][j].records.end()) {
					//check if the damper has finished
					float phaseDiff = time-it->travel_time;
					if (phaseDiff < it->damper.endTime) {
						continue;
					}
					else {
						wfmap.nodes[i][j].records.erase(it);
					}
				}

				glm::vec2 query(i, j);
				query = query*wfmap.offset;
				bool lline1 = MathUtilities::isLeft(p0, p1, query);
				bool lline2 = MathUtilities::isLeft(p1, p2, query);
				bool lline3 = MathUtilities::isLeft(p2, p3, query);
				bool lline4 = MathUtilities::isLeft(p3, p0, query);
				bool rline1 = MathUtilities::isRight(p0, p1, query);
				bool rline2 = MathUtilities::isRight(p1, p2, query);
				bool rline3 = MathUtilities::isRight(p2, p3, query);
				bool rline4 = MathUtilities::isRight(p3, p0, query);

				//if(!hasDone){
				//	hasDone = true;
				//	i = 1; j =1 ;
				if ((lline1&&lline2&&lline3&&lline4) || (rline1&&rline2&&rline3&&rline4)) {
					//it is inside the trapezoid
					WFHeightMapNodeRecord record;
					int wavefrontSize = wavefront.ERead.size();
					float amp1 = wavefront.ERead[l].amplitude;
					if (l > 0)amp1 = (amp1 + wavefront.ERead[l - 1].amplitude)*0.5f;
					float amp2 = wavefront.ERead[l].amplitude;
					if (l < wavefrontSize - 1)amp2 = (amp2 + wavefront.ERead[l + 1].amplitude)*0.5f;
					float amp3 = wavefront.EWrite[l].amplitude;
					if (l < wavefrontSize - 1)amp3 = (amp3 + wavefront.EWrite[l + 1].amplitude)*0.5f;
					float amp4 = wavefront.EWrite[l].amplitude;
					if (l > 0)amp4 = (amp4 + wavefront.EWrite[l - 1].amplitude)*0.5f;
					record.amplitude = //bilin_inter
						MathUtilities::IDW
						(p0, p1, p2, p3, query,
							amp1, amp2, amp3, amp4);
					//wavefront.ERead[l].amplitude , wavefront.ERead[l].amplitude, wavefront.EWrite[l].amplitude, wavefront.EWrite[l].amplitude);

					record.travel_time = //bilin_inter
						MathUtilities::IDW
						(p0, p1, p2, p3, query, time - time::deltaTime, time - time::deltaTime, time, time);
					glm::vec2 velocity =
						//bilin_inter
						MathUtilities::IDW
						(p0, p1, p2, p3, query,
							wavefront.VRead[l].velocity, wavefront.VRead[l + 1].velocity, wavefront.VWrite[l + 1].velocity, wavefront.VWrite[l].velocity);
					//interpolate the velocity and position in the VRead and VWrite nodes for l
					record.angular_frequency = wavefront.angular_frequency;

					record.velocity = velocity;
					record.wavefront_id = wavefront.id;
					record.damper = wavefront.dampingCoeff;
					wfmap.nodes[i][j].records.push_back(record);
					if (wfmap.nodes[i][j].records.size() > 5) {
						wfmap.nodes[i][j].RemoveWorstRecord();
					}
					//update the underlying map
					//////////////////////
					glm::vec2 direction = glm::normalize(velocity);
					glm::vec2 normVelocity = direction;
					if (glm::abs(direction.x) >= glm::abs(direction.y)) {
						direction = glm::vec2(glm::sign(direction.x), glm::sign(direction.y)*glm::abs(direction.y / direction.x));
					}
					else {
						//direction = glm::vec2(glm::sign(direction.y), glm::sign(direction.x)*glm::abs(direction.x / direction.y));
						direction = glm::vec2(glm::sign(direction.x)*glm::abs(direction.x / direction.y), glm::sign(direction.y));
					}
					glm::vec2 direction_abs = glm::abs(direction);
					//std::cout << "direction is " <<glm::to_string(direction)<< std::endl;
					//glm::vec2 direction_abs = direction;
					glm::vec2 perpedicular = glm::vec2(-direction.y, direction.x);
					glm::ivec2 limit = map.size / wfmap.size;
					glm::vec2 current = glm::ivec2(i*limit.x, j*limit.y);
					glm::vec2 begin = glm::ivec2(i*limit.x, j*limit.y);
					glm::vec2 end = current + direction*(float)(limit.x - 1);
					//std::cout << "begin is " << glm::to_string(begin) << " and end is " << glm::to_string(end) << std::endl;
					bool isDiagonal = (glm::distance(current, current - direction) > 1);
					glm::vec2 nextCurrent = current;
					float directionsCrossPos = MathUtilities::cross(direction, -perpedicular);
					float directionsCrossNeg = MathUtilities::cross(direction, perpedicular);
					float space_phase_step = (direction_abs.x*map.offset.x + direction_abs.y*map.offset.y) / glm::length(velocity);
					float startingPhase = 0.0f;
					float endingPhase = space_phase_step*(limit.x - 1);

					for (int k = 0; k < limit.x; k++) {//assumming that the grid is rectangular
													   //std::cout << "current is " << glm::to_string(current) << std::endl;

													   //	std::cout << "distance is " << glm::distance(current, oldCurrent) << std::endl;
													   //std::cout << "is diagonal is " << isDiagonal << std::endl;
						nextCurrent = current + direction;
						isDiagonal = (glm::distance(glm::floor(current), glm::floor(nextCurrent)) > 1);
						WaterHeightMapNodeRecord wrecord;
						wrecord.isCrestWave = true;
						wrecord.direction = direction;
						wrecord.dampingCoeff = wavefront.dampingCoeff;
						wrecord.amplitude = record.amplitude;
						wrecord.angular_frequency = record.angular_frequency;
						wrecord.init_phase_space = endingPhase*glm::distance(begin, current) / (glm::distance(begin, end)) +
							startingPhase*(glm::distance(end, current) / (glm::distance(begin, end)));
						//0.0f;//(k)*space_phase_step;
						//std::cout << "phase space is "<< wrecord.init_phase_space << std::endl;
						wrecord.init_phase_time = record.travel_time;
						wrecord.distorted_phase_time = wrecord.init_phase_time;
						wrecord.distorted_phase_space = wrecord.init_phase_space;//*MathUtilities::normalizedNoiseTiny();
						glm::vec2 currentPerpendicular = glm::vec2(0, 0);
						wrecord.weight = 1.0f;
						if (current.x >= 0 && current.x < map.size.x&&current.y >= 0 && current.y < map.size.y) {
							map.nodes[glm::floor(current.x)][glm::floor(current.y)].Add(wrecord,time::currentFrameTime);
							map.nodes[glm::floor(current.x)][glm::floor(current.y)].weight_sum += wrecord.weight;
						}
						glm::vec2 previousPerpendicular;
						bool isDiagonalPer;
						for (int l = 0; l < limit.y; l++) {
							previousPerpendicular = currentPerpendicular;
							currentPerpendicular += perpedicular;
							float uPos = MathUtilities::cross((current + currentPerpendicular) - (current - direction), -perpedicular) / (directionsCrossPos);
							//std::cout << "uPos is " << uPos << std::endl;
							glm::vec2 intersectionPos = (current - direction) + direction*uPos;
							float uNeg = MathUtilities::cross((current - currentPerpendicular) - (current - direction), perpedicular) / (directionsCrossNeg);
							glm::vec2 intersectionNeg = (current - direction) + direction*uNeg;
							//std::cout << "direction is " << glm::to_string(direction) << " and perpendicular is " << glm::to_string(perpedicular) << std::endl;
							//std::cout << "current is " << glm::to_string(current-direction) << " and perpendicular is " << glm::to_string(current+currentPerpendicular) << std::endl;
							//std::cout << "intersection is " << glm::to_string(intersection) << std::endl;
							//std::cout << "is diagonal is " << isDiagonalPer << " and indices are " << glm::to_string(currentPerpendicular) << " vs "<<glm::to_string(previousPerpendicular) << std::endl;


							glm::vec2 indexPos = current + currentPerpendicular;
							glm::vec2 indexNeg = current - currentPerpendicular;
							wrecord.weight -= 0.5f / ((float)limit.x);
							//wrecord.init_phase_space = 0;//(k+0.5)*space_phase_step;
							wrecord.distorted_phase_space = wrecord.init_phase_space;//*MathUtilities::normalizedNoiseTiny();
							isDiagonalPer = (glm::distance(glm::floor(current - currentPerpendicular), glm::floor(current - previousPerpendicular)) > 1);
							if (isDiagonal&&isDiagonalPer) {

								glm::vec2 indexMiddleNeg = glm::round((current - currentPerpendicular + current - previousPerpendicular)*0.5f + normVelocity*1.4114f*0.5f);
								uNeg = MathUtilities::cross((indexMiddleNeg)-(current - direction), perpedicular) / (directionsCrossNeg);
								intersectionNeg = (current - direction) + direction*uNeg;
								float newWeight = 1 - glm::distance(indexMiddleNeg, intersectionNeg) / glm::distance(intersectionNeg, current + (float)limit.x*direction);
								//std::cout << "Neg weight is " << wrecord.weight << " accurate is " << newWeight << std::endl;
								wrecord.weight = newWeight;
								wrecord.init_phase_space = endingPhase*glm::distance(begin, intersectionNeg) / (glm::distance(begin, end)) +
									startingPhase*(glm::distance(end, intersectionNeg) / (glm::distance(begin, end)));
								wrecord.distorted_phase_space = wrecord.init_phase_space;//*MathUtilities::normalizedNoiseTiny();

								//std::cout<<"current "
								//std::cout << "points are " << glm::to_string(glm::floor(current) - glm::floor(currentPerpendicular)) << " vs " << glm::to_string(glm::floor(current - previousPerpendicular)) << std::endl;
								//std::cout << "midpoint " << glm::to_string((glm::floor(current) - glm::floor(currentPerpendicular) + glm::floor(current) - glm::floor(previousPerpendicular))*0.5f) << std::endl;
								if (indexMiddleNeg.x > 0 && indexMiddleNeg.x < map.size.x&&indexMiddleNeg.y>0 && indexMiddleNeg.y < map.size.y) {
									map.nodes[indexMiddleNeg.x][indexMiddleNeg.y].Add(wrecord,time::currentFrameTime);

									map.nodes[indexMiddleNeg.x][indexMiddleNeg.y].weight_sum = -1.0f;
									//std::cout << "adding to " << glm::to_string(indexMiddleNeg) << "(diagonal case)" << std::endl;
									//std::cout << "perp current+ is " << glm::to_string(indexNeg) << std::endl;
									//std::cout << "diagonal:intersection point is " << glm::to_string(intersectionNeg) << std::endl;
								}
							}
							isDiagonalPer = (glm::distance(glm::floor(current + currentPerpendicular), glm::floor(current + previousPerpendicular)) > 1);
							if (isDiagonal&&isDiagonalPer) {
								//std::cout << "current is " << glm::to_string(current) << " and current pep is " << glm::to_string(currentPerpendicular) << "a nd previous pep is" << glm::to_string(previousPerpendicular) << std::endl;
								glm::vec2 indexMiddlePos = glm::round((current + currentPerpendicular + current + previousPerpendicular)*0.5f + normVelocity*1.4114f*0.5f);
								uPos = MathUtilities::cross((indexMiddlePos)-(current - direction), perpedicular) / (directionsCrossPos);
								intersectionPos = (current - direction) - direction*uPos;
								float newWeight = 1 - glm::distance(indexMiddlePos, intersectionPos) / glm::distance(intersectionPos, current + (float)limit.x*direction);
								//std::cout << "Pos weight is " << wrecord.weight << " accurate is " << newWeight << std::endl;
								wrecord.weight = newWeight;
								wrecord.init_phase_space = endingPhase*glm::distance(begin, intersectionPos) / (glm::distance(begin, end)) +
									startingPhase*(glm::distance(end, intersectionPos) / (glm::distance(begin, end)));
								wrecord.distorted_phase_space = wrecord.init_phase_space;//*MathUtilities::normalizedNoiseTiny();
								//std::cout<<"current "
								//std::cout << "points are " << glm::to_string(glm::floor(current) + glm::floor(currentPerpendicular)) << " vs " << glm::to_string(glm::floor(current + previousPerpendicular)) << std::endl;
								//std::cout << "midpoint " << glm::to_string((glm::floor(current) + glm::floor(currentPerpendicular) + glm::floor(current) + glm::floor(previousPerpendicular))*0.5f) << std::endl;
								if (indexMiddlePos.x > 0 && indexMiddlePos.x < map.size.x&&indexMiddlePos.y>0 && indexMiddlePos.y < map.size.y) {
									map.nodes[indexMiddlePos.x][indexMiddlePos.y].Add(wrecord,time::currentFrameTime);
									map.nodes[indexMiddlePos.x][indexMiddlePos.y].weight_sum = -1.0f;
									//std::cout << "diagonal:phase is " << wrecord.init_phase_space << std::endl;
									//std::cout << "diagonal:intersection point is " << glm::to_string(intersectionPos) << std::endl;
								}
							}


							wrecord.init_phase_space = 0.0f;// k*space_phase_step;
							wrecord.distorted_phase_space = wrecord.init_phase_space;//*MathUtilities::normalizedNoiseTiny();
							wrecord.weight -= 0.5f / ((float)limit.x);
							if (indexPos.x > 0 && indexPos.x < map.size.x
								&&indexPos.y>0 && indexPos.y < map.size.y) {
								//indexPos = glm::round(indexPos);
								//std::cout << "adding to " << glm::to_string(current + currentPerpendicular) << std::endl;
								uPos = MathUtilities::cross((indexPos)-(current - direction), perpedicular) / (directionsCrossPos);
								intersectionPos = (current - direction) - direction*uPos;
								float newWeight = 1 - glm::distance(indexPos, intersectionPos) / glm::distance(intersectionPos, current + (float)limit.x*direction);
								wrecord.weight = newWeight;
								wrecord.init_phase_space = endingPhase*glm::distance(begin, intersectionPos) / (glm::distance(begin, end)) +
									startingPhase*(glm::distance(end, intersectionPos) / (glm::distance(begin, end)));
								wrecord.distorted_phase_space = wrecord.init_phase_space;//*MathUtilities::normalizedNoiseTiny();
								//std::cout << "PosNorm weight is " << wrecord.weight << " accurate is " << newWeight << std::endl;
								map.nodes[current.x + currentPerpendicular.x][current.y + currentPerpendicular.y].Add(wrecord,time::currentFrameTime);
								map.nodes[current.x + currentPerpendicular.x][current.y + currentPerpendicular.y].weight_sum += wrecord.weight;
								//std::cout << "normal:phase is " << wrecord.init_phase_space << std::endl;
								//std::cout << "normal:intersection point is " << glm::to_string(intersectionPos) << std::endl;
							}
							if (indexNeg.x > 0 && indexNeg.x < map.size.x
								&&indexNeg.y>0 && indexNeg.y < map.size.y) {
								uNeg = MathUtilities::cross((indexNeg)-(current - direction), perpedicular) / (directionsCrossNeg);
								intersectionNeg = (current - direction) + direction*uNeg;
								float newWeight = 1 - glm::distance(indexNeg, intersectionNeg) / glm::distance(intersectionNeg, current + (float)limit.x*direction);
								wrecord.weight = newWeight;
								wrecord.init_phase_space = endingPhase*glm::distance(begin, intersectionNeg) / (glm::distance(begin, end)) +
									startingPhase*(glm::distance(end, intersectionNeg) / (glm::distance(begin, end)));
								wrecord.distorted_phase_space = wrecord.init_phase_space;//*MathUtilities::normalizedNoiseTiny();
								//std::cout << "NegNorm weight is " << wrecord.weight << " accurate is " << newWeight << std::endl;
								//	std::cout << "normal:intersection point is " << glm::to_string(intersectionNeg) << std::endl;
								map.nodes[current.x - currentPerpendicular.x][current.y - currentPerpendicular.y].Add(wrecord,time::currentFrameTime);
								map.nodes[current.x - currentPerpendicular.x][current.y - currentPerpendicular.y].weight_sum += wrecord.weight;
								//std::cout << "adding to " << glm::to_string(current-currentPerpendicular) << "(normal case)" << std::endl;
							}

						}

						current += direction;
					}

				}
			}
		}
	}
}

void DynamicObstacle::UpdateImmersionOutLine()
{
	//Outline in local coordinates//
	//Assumes convex geometry!!!//
	glm::vec3 planeOrigin = //glm::inverse(inst.transform)*
		glm::vec4(0, 0, 0, 1);
	glm::vec3 planeUp = //glm::inverse(inst.transform)*
		glm::vec4(0, 1, 0, 1);
	planeUp = glm::normalize(planeUp - planeOrigin);
	for (int i = 0; i < inst.mod->vertices.size(); i += 3) {
		glm::vec3 p0 = inst.mod->vertices[i];
		glm::vec3 p1 = inst.mod->vertices[i + 1];
		glm::vec3 p2 = inst.mod->vertices[i + 2];
		float distance0 = distance::point2plane(planeOrigin, planeUp, p0);
		float distance1 = distance::point2plane(planeOrigin, planeUp, p1);
		float distance2 = distance::point2plane(planeOrigin, planeUp, p2);

		if (glm::sign(distance0) != glm::sign(distance1)) {
			glm::vec3 globalP0 = inst.transform*glm::vec4(p0, 1.0f);
			glm::vec3 globalP1 = inst.transform*glm::vec4(p1, 1.0f);
			glm::vec3 direction01 = glm::normalize(globalP1 - globalP0);
			float scalar = -(globalP0.y / direction01.y);
			glm::vec3 projection = globalP0 + direction01*scalar;
			glm::vec3 localProjection = glm::inverse(inst.transform)*glm::vec4(projection, 1.0f);
			glm::vec2 localProjection2D = glm::vec2(localProjection.x, localProjection.z);
			if (std::find_if(immersionOutLine.begin(), immersionOutLine.end(), [localProjection2D](auto i) {return glm::distance(i,localProjection2D)<0.01;}) == immersionOutLine.end()) {
				immersionOutLine.push_back(glm::vec2(localProjection.x, localProjection.z));
			}
		}
		if (glm::sign(distance1) != glm::sign(distance2)) {
			glm::vec3 globalP1 = inst.transform*glm::vec4(p1, 1.0f);
			glm::vec3 globalP2 = inst.transform*glm::vec4(p2, 1.0f);
			glm::vec3 direction12 = glm::normalize(globalP2 - globalP1);
			float scalar = -(globalP1.y / direction12.y);
			glm::vec3 projection = globalP1 + direction12*scalar;
			glm::vec3 localProjection = glm::inverse(inst.transform)*glm::vec4(projection, 1.0f);
			glm::vec2 localProjection2D = glm::vec2(localProjection.x, localProjection.z);
			if (std::find_if(immersionOutLine.begin(), immersionOutLine.end(), [localProjection2D](auto i) {return glm::distance(i, localProjection2D)<0.01; }) == immersionOutLine.end()) {
				immersionOutLine.push_back(glm::vec2(localProjection.x, localProjection.z));
			}
		}
		if (glm::sign(distance2) != glm::sign(distance0)) {
			glm::vec3 globalP2 = inst.transform*glm::vec4(p2, 1.0f);
			glm::vec3 globalP0 = inst.transform*glm::vec4(p0, 1.0f);
			glm::vec3 direction20 = glm::normalize(globalP0 - globalP2);
			float scalar = -(globalP2.y / direction20.y);
			glm::vec3 projection = globalP2 + direction20*scalar;
			glm::vec3 localProjection = glm::inverse(inst.transform)*glm::vec4(projection, 1.0f);
			glm::vec2 localProjection2D = glm::vec2(localProjection.x, localProjection.z);
			if (std::find_if(immersionOutLine.begin(), immersionOutLine.end(), [localProjection2D](auto i) {return glm::distance(i, localProjection2D)<0.01; }) == immersionOutLine.end()) {
				immersionOutLine.push_back(glm::vec2(localProjection.x, localProjection.z));
			}
		}
	}
	std::vector<glm::vec2> orientedOutline;
	orientedOutline.resize(immersionOutLine.size());
	//refine immersion line accuracy
	for (int i = 0; i < immersionOutLine.size(); i++) {
		if (glm::abs(immersionOutLine[i].x) < 0.01) {
			immersionOutLine[i].x = 0;
		}
		if (glm::abs(immersionOutLine[i].y) < 0.01) {
			immersionOutLine[i].y = 0;
		}
	}
	//Find convex hull
	///find top-left most point
	glm::vec2 topLeft = immersionOutLine[0];
	for (int i = 1; i < immersionOutLine.size(); i++) {
		float x_distance = glm::distance(immersionOutLine[i].x, topLeft.x);
		float y_distance = glm::distance(immersionOutLine[i].y, topLeft.y);
		if ((immersionOutLine[i].x <= topLeft.x||x_distance<0.01f)&&(immersionOutLine[i].y >= topLeft.y||y_distance<0.01f)) {
			topLeft = immersionOutLine[i];
		}
	}
	///assume horizontal direction and check against other candidates
	orientedOutline[0] = topLeft;
	glm::vec2 previousDirection = glm::vec2(1, 0);
	glm::vec2 previousPivot = topLeft;
	//refine the previous direction
	float minDistance = std::numeric_limits<float>::max();
	for (int i = 0; i < immersionOutLine.size(); i++) {
		if (immersionOutLine[i] == topLeft || immersionOutLine[i].x<topLeft.x) {
			continue;
		}
		if (glm::distance(immersionOutLine[i], topLeft) < minDistance) {
			minDistance = glm::distance(immersionOutLine[i], topLeft);
			previousDirection = glm::normalize(immersionOutLine[i] - topLeft);
		}
	}
	//find the oriented outeline
	//float minAngle = std::numeric_limits<float>::max();
	for (int i = 1; i < immersionOutLine.size(); i++) {
		glm::vec2 bestCandidate = immersionOutLine[0];
		if (bestCandidate == previousPivot)bestCandidate = immersionOutLine[1];
		float bestAngle = glm::dot(glm::normalize(bestCandidate - previousPivot), previousDirection);
		glm::vec2 bestDirection = glm::normalize(bestCandidate - previousPivot);
		for (int j = 0; j < immersionOutLine.size(); j++) {
			if (immersionOutLine[j] == previousPivot)
				continue;
			float angle = glm::dot(glm::normalize(immersionOutLine[j] - previousPivot), previousDirection);
			if (angle>bestAngle&&glm::distance(angle,bestAngle)>0.01) {
				bestCandidate = immersionOutLine[j];
				bestAngle = angle;
				bestDirection = glm::normalize(immersionOutLine[j] - previousPivot);
			}
			else if(glm::distance(angle,bestAngle)<0.01){
				if (glm::distance(immersionOutLine[j], previousPivot) < glm::distance(bestCandidate, previousPivot)) {
					bestCandidate = immersionOutLine[j];
					bestAngle = angle;
					bestDirection = glm::normalize(immersionOutLine[j] - previousPivot);
				}
			}
		}
		orientedOutline[i] = bestCandidate;
		previousDirection = glm::normalize(bestCandidate - previousPivot);
		previousPivot = bestCandidate;
	}
	immersionOutLine = std::vector<glm::vec2>(orientedOutline);
	for (int i = 0; i < immersionOutLine.size() - 1; i++) {
		//std::cout << "size[" << i << "] is " << glm::distance(immersionOutLine[i], immersionOutLine[i + 1]) << std::endl;
	}
	//repeat the first entry to the end
	//if (immersionOutLine.size()>0)
		//immersionOutLine.push_back(*immersionOutLine.begin());
	//remove collinear segments
	std::vector<glm::vec2> refinedOutline;
	refinedOutline.push_back(immersionOutLine[0]);
	if (immersionOutLine.size() > 75)immersionOutLine.resize(75);//Final demo hack, the model is crappy
	for (int i = 1;i<immersionOutLine.size()-1; i++) {
		float angle = glm::dot(glm::normalize(immersionOutLine[i] - immersionOutLine[i-1]),glm::normalize(immersionOutLine[i + 1] - immersionOutLine[i-1]));
		if (glm::abs(angle)<0.999) {//the thershold can be refined for higher simplification
			refinedOutline.push_back(immersionOutLine[i]);
		}
	}
	immersionOutLine.clear();
	immersionOutLine = std::vector<glm::vec2>(refinedOutline);
	std::cout << "length of outline is "<< immersionOutLine.size() << std::endl;
	//repeat the first entry to the end
	if (immersionOutLine.size()>0)
		immersionOutLine.push_back(*immersionOutLine.begin());
}

void DynamicObstacle::UpdateNormals()
{
	//Should be called after UpdateImmersionOutline
	//Update the edge normals
	edgeNormals.resize(immersionOutLine.size() - 1);
	vertexNormals.resize(immersionOutLine.size());
	for (int i = 0; i < immersionOutLine.size()-1; i++) {
		edgeNormals[i] = glm::normalize(immersionOutLine[i + 1] - immersionOutLine[i]);
		edgeNormals[i] = glm::vec2(-edgeNormals[i].y, edgeNormals[i].x);
		//std::cout << "edge is " << glm::to_string(edgeNormals[i]) <<" for "<<glm::to_string(immersionOutLine[i])<<" --> "<<glm::to_string(immersionOutLine[i+1])<<std::endl;
	}
	//Update the vertex normals
	for (int i = 1; i < immersionOutLine.size()-1; i++) {
		vertexNormals[i] = glm::normalize((edgeNormals[i - 1] + edgeNormals[i])*0.5f);
		//std::cout << "vertex is " << glm::to_string(vertexNormals[i]) << " for " << glm::to_string(immersionOutLine[i])<<std::endl ;
	}
	vertexNormals[0] = glm::normalize((edgeNormals.back() + edgeNormals.front())*0.5f);
	vertexNormals[vertexNormals.size()-1]= vertexNormals[0];
}

Rigidbody* DynamicObstacle::ConstructRigidbody()
{
	//create a model from the outline data
	model mod;
	mod.vertices.resize(immersionOutLine.size());
	mod.normals.resize(immersionOutLine.size());
	for (int i = 0; i < mod.vertices.size(); i++) {
		mod.vertices[i] = glm::vec3(immersionOutLine[i].x, 0, immersionOutLine[i].y);
		mod.normals[i] = glm::vec3(vertexNormals[i].x, 0, vertexNormals[i].y);
	}
	mod.calculateUniqueIndices();
	//complete the rigidnbody geometry instance with that model
	instance insta;
	insta.mod = new model(mod);
	insta.mat = inst.mat;
	instance boundingSphere=insta;
	instance OBB=insta;

	Rigidbody* rigidbody = new Rigidbody(inst, inst.transform[3], inst, inst);
	rigidbody->bodyspaceGeometry = mod;
	rigidbody->geometry = insta;
	return rigidbody;
}

void DynamicObstacle::UpdateAnchorPaths()
{
	//check if there is any force acting on the object
	glm::vec2 netForce2D = glm::vec2(rigidbody->netForce.x, rigidbody->netForce.z);
	glm::vec2 velocity2D = glm::vec2(rigidbody->linearMomentum.x, rigidbody->linearMomentum.z) / rigidbody->mass;
	if (netForce2D == glm::vec2(0.0f)||velocity2D==glm::vec2(0.0f)||glm::dot(netForce2D,velocity2D)<=0) {
		anchorPathRight.first = 0;
		anchorPathRight.second = 0;
		anchorPathLeft.first = 0;
		anchorPathLeft.second = 0;
		return;
	}
	glm::vec2 direction = glm::normalize(velocity2D);
	//change the direction to the obstacle local space
	glm::vec3 local_direction = glm::inverse(rigidbody->orientation)*glm::vec3(direction.x,0,direction.y);
	direction = glm::normalize(glm::vec2(local_direction.x, local_direction.z));
	//std::cout << "direction is " << glm::to_string(velocity2D) << " and local is " << glm::to_string(local_direction) << std::endl;
	//find the vertex normal that aligns best with direction vector
	//glm::vec2 bestVertexCandidate = vertexNormals[0];
	int bestVertexCandidateIndex = 0;
	float bestVertexCandidateAngle = glm::dot(direction, vertexNormals[0]);
	for (int i = 1; i < vertexNormals.size(); i++) {
		float angle = glm::dot(direction, vertexNormals[i]);
		if (angle > bestVertexCandidateAngle) {
			bestVertexCandidateIndex = i;
			bestVertexCandidateAngle = angle;
		}
	}
	//find the edge normal that aligns best with direction vector
	int bestEdgeCandidateIndex = 0;
	float bestEdgeCandidateAngle = glm::dot(direction, edgeNormals[0]);
	for (int i = 1; i < edgeNormals.size(); i++) {
		float angle = glm::dot(direction, edgeNormals[i]);
		if (angle > bestEdgeCandidateAngle) {
			bestEdgeCandidateIndex = i;
			bestEdgeCandidateAngle = angle;
		}
	}
	//compare the best angles and assign the paths begin appropriately
 	bool AnchorIsVertex = bestVertexCandidateAngle >= bestEdgeCandidateAngle;
	if (AnchorIsVertex) {
		anchorPathLeft.first = bestVertexCandidateIndex;
		anchorPathRight.first = bestVertexCandidateIndex;
	}
	else {
		anchorPathLeft.first = bestEdgeCandidateIndex;
		//anchorPathRight.first = bestEdgeCandidateIndex == vertexNormals.size() - 1 ? 0 : bestEdgeCandidateIndex + 1;
		anchorPathRight.first = bestEdgeCandidateIndex;
	}
	//find the end of the path by taking the immersion outline path in two directions
	//left path (backwards)
	if (anchorPathLeft.first == 0)anchorPathLeft.first = immersionOutLine.size() - 1;
	int index = anchorPathLeft.first;
	do {
		if (index == 0)index = immersionOutLine.size() - 1;
		anchorPathLeft.second = index;
		index = index - 1;
		float angle = glm::dot(vertexNormals[index], direction);
		if (angle < -0.01f) {
			break;
		}
	}while(index!=anchorPathLeft.first);
	//right path (forwards)
	if (anchorPathRight.first == immersionOutLine.size() - 1)anchorPathRight.first = 0;
	index = anchorPathRight.first;
	do {
		if (index == immersionOutLine.size() - 1)index = 0;
		anchorPathRight.second = index;
		index = index + 1;
		float angle = glm::dot(vertexNormals[index], direction);
		if (angle < -0.01f) {
			break;
		}
	} while (index != anchorPathRight.first);

	/*std::cout << "anchor Right path is (" << anchorPathRight.first << "," << anchorPathRight.second << ")" << std::endl;
	std::cout << "positions are " << glm::to_string(immersionOutLine[anchorPathRight.first]) << " and " << glm::to_string(immersionOutLine[anchorPathRight.second]) << std::endl;
	std::cout << "anchor Left path is (" << anchorPathLeft.first << "," << anchorPathLeft.second << ")" << std::endl;
	std::cout << "positions are " << glm::to_string(immersionOutLine[anchorPathLeft.first]) << " and " << glm::to_string(immersionOutLine[anchorPathLeft.second]) << std::endl;*/
	
}

void DynamicObstacle::PropagateWavefront() {
	glm::vec2 velocity2D = glm::vec2(rigidbody->linearMomentum.x, rigidbody->linearMomentum.z) / rigidbody->mass;
	if (wavefrontLeft!=NULL||wavefrontRight!=NULL) {
		if(wavefrontLeft!=NULL)
		for (int i = 0; i < wavefrontLeft->VWrite.size(); i++) {
			if (wavefrontLeft->VWrite[i].velocity != glm::vec2(0.0f)) {
				wavefrontLeft->VWrite[i].velocity = velocity2D;//glm::normalize(wavefrontLeft->VWrite[i].velocity)*glm::length(velocity2D);
				wavefrontLeft->VWrite[i].position += velocity2D*time::deltaTime;
			}
		}
		if(wavefrontRight!=NULL)
		for (int i = 0; i < wavefrontRight->VWrite.size(); i++) {
			if (wavefrontRight->VWrite[i].velocity != glm::vec2(0.0f)) {
				wavefrontRight->VWrite[i].velocity = velocity2D;//glm::normalize(wavefrontRight->VWrite[i].velocity)*glm::length(velocity2D);
				wavefrontRight->VWrite[i].position += velocity2D*time::deltaTime;
			}
		}
		return;
	}
	if (anchorPathLeft.first == anchorPathLeft.second && anchorPathRight.first == anchorPathRight.second) {
		return;
	}
	if (!produceWavefront)return;
	wavefrontLeft = new Wavefront();
	wavefrontLeft->isCrestWave = true;
	wavefrontLeft->dampingCoeff.peakValue = glm::length(rigidbody->linearMomentum);
	wavefrontLeft->dampingCoeff.startTime = 0.0f;
	wavefrontLeft->dampingCoeff.middle = 0.1f;
	wavefrontLeft->dampingCoeff.endTime = 2.0f;
	//wavefrontLeft->lifeTime=0.0f;
	wavefrontLeft->id = Obstacle_id;
	wavefrontRight = new Wavefront();
	wavefrontRight->isCrestWave = true;
	wavefrontRight->dampingCoeff = wavefrontLeft->dampingCoeff;
	//wavefrontRight->lifeTime = 0.0f;
	wavefrontRight->id = Obstacle_id;
	Wavefront& leftWavefront = *wavefrontLeft;
	Wavefront& rightWavefront = *wavefrontRight;
	//calculate the parameters of the propagated wavefront from the rigidbody
	float angular_frequency = 100.0f;
	float speed = glm::length(velocity2D);
	//create a wavefront with the left path
	int index = anchorPathLeft.first;
	if (anchorPathLeft.first != anchorPathLeft.second) {
		if (index == 0) {
			index = immersionOutLine.size() - 1;
		}
		do {
			Wavefront_vertex vertex;
			Wavefront_edge edge;
			glm::vec3 globalPosition = rigidbody->orientation*glm::vec3(immersionOutLine[index].x, 0, immersionOutLine[index].y) + rigidbody->position;
			glm::vec3 globalNormal = glm::vec3(0.0f);
			if (index == anchorPathLeft.first&&anchorPathRight.first==anchorPathRight.second) {//case the left anchor path does not really exist
				globalNormal = rigidbody->orientation*glm::vec3(edgeNormals[index-1].x, 0, edgeNormals[index-1].y);
			}
			else {
				globalNormal = rigidbody->orientation*glm::vec3(vertexNormals[index].x, 0, vertexNormals[index].y);
			}
			vertex.position = glm::vec2(globalPosition.x, globalPosition.z);
			float angle = glm::dot(glm::normalize(glm::vec2(globalNormal.x, globalNormal.z)), glm::normalize(velocity2D));
			vertex.velocity = glm::normalize(glm::vec2(globalNormal.x, globalNormal.z))*speed;
				//(glm::normalize(velocity2D)+(1.0f-angle)*glm::normalize(glm::vec2(globalNormal.x,globalNormal.z)))*speed;
 			leftWavefront.VRead.push_back(vertex);
			vertex.position += vertexNormals[index]*2.5f;
			leftWavefront.VWrite.push_back(vertex);
			edge.energyDensity = glm::length(rigidbody->mass*0.0005f);
			edge.wavenum = glm::length(velocity2D)*angular_frequency;
			edge.groupSpeed = 5.0f;
			leftWavefront.EWrite.push_back(edge);
			leftWavefront.ERead.push_back(edge);
			index = index - 1;
			if (index == 0) {
				index = immersionOutLine.size() - 1;
			}
		} while (index != anchorPathLeft.second);
		if (index == immersionOutLine.size() - 1) {
			index = 0; 
			if (leftWavefront.EWrite.size() > 1) {
				leftWavefront.VRead.pop_back();
				leftWavefront.VWrite.pop_back();
				leftWavefront.ERead.pop_back();
				leftWavefront.EWrite.pop_back();
			}
		}
		Wavefront_vertex vertex;
		glm::vec3 globalPosition = rigidbody->orientation*glm::vec3(immersionOutLine[index].x, 0, immersionOutLine[index].y) + rigidbody->position;
		glm::vec3 globalNormal;
		globalNormal= rigidbody->orientation*glm::vec3(edgeNormals[index].x, 0, edgeNormals[index].y);
		vertex.position = glm::vec2(globalPosition.x, globalPosition.z);
		float angle = glm::dot(glm::normalize(glm::vec2(globalNormal.x, globalNormal.z)), glm::normalize(velocity2D));
		vertex.velocity = glm::normalize(glm::vec2(globalNormal.x, globalNormal.z))*speed;
		//(glm::normalize(velocity2D)+(1.0f-angle)*glm::normalize(glm::vec2(globalNormal.x,globalNormal.z)))*speed;
		leftWavefront.VRead.push_back(vertex);
		vertex.position += vertexNormals[index]*2.50f;
		leftWavefront.VWrite.push_back(vertex);
	}
	leftWavefront.angular_frequency = angular_frequency; //arbitrary, change according to speed
	leftWavefront.wavenum = angular_frequency*glm::length(velocity2D);//arbitrary, change according to speed
	index = anchorPathRight.first;
	if (anchorPathRight.first != anchorPathRight.second) {
		if (index == immersionOutLine.size()-1) {
			index = 0;
		}
		do {
			Wavefront_vertex vertex;
			Wavefront_edge edge;
			glm::vec3 globalPosition = rigidbody->orientation*glm::vec3(immersionOutLine[index].x, 0, immersionOutLine[index].y) + rigidbody->position;
			glm::vec3 globalNormal = glm::vec3(0.0f);
			if (index == anchorPathRight.first&&anchorPathLeft.first == anchorPathLeft.second) { //case the left path does not exist
				globalNormal = rigidbody->orientation*glm::vec3(edgeNormals[index].x, 0, edgeNormals[index].y);
			}
			else {
				globalNormal = rigidbody->orientation*glm::vec3(vertexNormals[index].x, 0, vertexNormals[index].y);
			}
			vertex.position = glm::vec2(globalPosition.x, globalPosition.z);
			float angle = glm::dot(glm::normalize(glm::vec2(globalNormal.x, globalNormal.z)), glm::normalize(velocity2D));
			vertex.velocity = glm::normalize(glm::vec2(globalNormal.x, globalNormal.z))*speed;
			//(glm::normalize(velocity2D)+(1.0f-angle)*glm::normalize(glm::vec2(globalNormal.x,globalNormal.z)))*speed;
			rightWavefront.VRead.push_back(vertex);
			vertex.position += vertexNormals[index]*2.5f;
			rightWavefront.VWrite.push_back(vertex);
			edge.energyDensity = glm::length(rigidbody->mass*0.0005f);
			edge.wavenum = glm::length(velocity2D)*angular_frequency;;
			edge.groupSpeed = 5.0f;
			rightWavefront.EWrite.push_back(edge);
			rightWavefront.ERead.push_back(edge);
			index = index + 1;
			if (index == immersionOutLine.size()) {
				index = 0;
			}
		} while (index != anchorPathRight.second);
		Wavefront_vertex vertex;
		if (index == 0) {
			index = immersionOutLine.size() - 1;
			if (rightWavefront.EWrite.size() > 1) {
				rightWavefront.VRead.pop_back();
				rightWavefront.VWrite.pop_back();
				rightWavefront.ERead.pop_back();
				rightWavefront.EWrite.pop_back();
			}
		}
		glm::vec3 globalPosition = rigidbody->orientation*glm::vec3(immersionOutLine[index].x, 0, immersionOutLine[index].y) + rigidbody->position;
		//inst.transform*glm::vec4(immersionOutLine[index].x, 0, immersionOutLine[index].y, 1.0f);
		glm::vec3 globalNormal = rigidbody->orientation*glm::vec3(edgeNormals[index-1].x, 0, edgeNormals[index-1].y);
		vertex.position = glm::vec2(globalPosition.x, globalPosition.z);
		float angle = glm::dot(glm::normalize(glm::vec2(globalNormal.x, globalNormal.z)), glm::normalize(velocity2D));
		vertex.velocity = glm::normalize(glm::vec2(globalNormal.x, globalNormal.z))*speed;
		//(glm::normalize(velocity2D)+(1.0f-angle)*glm::normalize(glm::vec2(globalNormal.x,globalNormal.z)))*speed;
		rightWavefront.VRead.push_back(vertex);
		vertex.position +=vertexNormals[index]*2.50f;
		rightWavefront.VWrite.push_back(vertex);
	}
	rightWavefront.angular_frequency = angular_frequency; //arbitrary, change according to speed
	rightWavefront.wavenum = angular_frequency*glm::length(velocity2D);//arbitrary, change according to speed
}

void DynamicObstacle::CheckWavefrontRefreshStatus()
{
	releaseWavefront = false;
	produceWavefront = false;
	if (rigidbody->linearMomentum == glm::vec3(0.0f)) {
		releaseWavefront = true;
		return;
	}
	if (rigidbody->netForce == glm::vec3(0.0f)) {
		releaseWavefront = true;
		return;
	}
	//get the angle between linear mom and net force
	float angle = glm::dot(glm::normalize(rigidbody->netForce), glm::normalize(rigidbody->linearMomentum));
	//if angle is positive then produce
	if (angle > 0&&(wavefrontLeft==NULL&&wavefrontRight==NULL)) {
		produceWavefront = true;
	}
	if (angle < 1&&wavefrontLeft!=NULL&&wavefrontRight!=NULL) {
		releaseWavefront = true;
	}
}

void DynamicObstacle::Update(Wavefront & w)
{
	for (int i = 0; i < w.VWrite.size(); i++) { //for each vertex in the wavefront
		Wavefront_vertex& v0 = w.VRead[i];
		Wavefront_vertex& v1 = w.VWrite[i];
		for (int j = 0; j < immersionOutLine.size() - 1; j++) {
			glm::vec4 global0= inst.transform*glm::vec4(immersionOutLine[j].x, 0, immersionOutLine[j].y, 1.0f);
			glm::vec4 global1= inst.transform*glm::vec4(immersionOutLine[j+1].x, 0, immersionOutLine[j+1].y, 1.0f);
			glm::vec2 outline0 = glm::vec2(global0.x, global0.z);
			glm::vec2 outline1 = glm::vec2(global1.x, global1.z);
			Location location1 = findLocation(v0.position, v1.position, outline0);
			Location location2 = findLocation(v0.position, v1.position, outline1);
			if (location1 != location2&&location1 != LOCATION_ON&&location2 != LOCATION_ON) {
				location1 = findLocation(outline0, outline1, v0.position);
				location2 = findLocation(outline0, outline1, v1.position);
				if (location1 != location2&&location1 != LOCATION_ON&&location2 != LOCATION_ON) {
					//find the crossing point and the normal,tangent and incident vectors
					glm::vec2 crossing = ffindCrossingPoint(v0.position, v1.position, outline0, outline1);
					glm::vec2 incident = glm::normalize(v1.position - v0.position);
					glm::vec2 tangent = glm::normalize(outline1 - outline0);
					glm::vec2 normal = glm::vec2(-tangent.y, tangent.x);
					//change the position to the crossing point
					v1.position = crossing + normal*0.1f;
					//find the angle to check if the point should reflect or difract
					float angle = glm::dot(normal, -incident);
					//reflect or diffract
					//std::cout << "angle is " << angle << std::endl;
					if (angle > 0.63) {//reflect
						v1.velocity = glm::reflect(incident, normal)*glm::length(v1.velocity);
					}
					else {//refract
						if (glm::dot(tangent, -incident) > 0) {//on the bitangent
							v1.velocity = -tangent*glm::length(v1.velocity);
						}
						else {//on the tangent
							v1.velocity = tangent*glm::length(v1.velocity);
						}
					}
				}
			}
		}
	}
}

void DynamicObstacle::Render()
{
	//inst.Render();
	//inst.BindMaterial();
	//inst.transform = glm::mat4(1.0f);
	rigidbody->Render(*inst.mat,false);
	glm::vec4 colour = inst.mat->color;
	inst.mat->color = glm::vec4(1, 1, 0, 1);
	inst.transform = glm::mat4(1.0f);
	inst.transform = rigidbody->orientation;
	inst.transform[3] = glm::vec4(rigidbody->position+glm::vec3(0.0f,0.0f,0.0f), 1.0f);
	inst.BindMaterial();
	inst.RenderTriangles();
	glBegin(GL_LINE_LOOP);
	glm::vec4 position;
	for (int i = 0; i < immersionOutLine.size(); i++) {
		position = glm::vec4(immersionOutLine[i].x, 0, immersionOutLine[i].y, 1);
		glVertex3f(position.x+vertexNormals[i].x*0.1f, 0, position.z+vertexNormals[i].y*0.1f);
	}
	glEnd();
	inst.mat->color = glm::vec4(1, 0, 0, 1);
	inst.transform = rigidbody->orientation;
	inst.transform[3] = glm::vec4(rigidbody->position, 1.0f);
	inst.BindMaterial();
	glBegin(GL_LINE_LOOP);
	for (int i = 0; i < immersionOutLine.size(); i++) {
		position = glm::vec4(immersionOutLine[i].x, 0, immersionOutLine[i].y, 1);
		glVertex3f(position.x + vertexNormals[i].x*0.1f, 0, position.z + vertexNormals[i].y*0.1f);
	}
	glEnd();
	inst.mat->color = colour;
	inst.BindMaterial();
}
