#include "shaders.h"
#include "textfile.h"
#include "debug.h"
#include <iostream>
void shaders::loadShaders(std::vector<std::tuple<std::string, std::string,std::string>> shaderPaths, std::vector<GLuint>& shaderIDs)
{
	//local containers
	std::vector<std::string> vertexSource;  //source code of the vertex shaders
	std::vector<std::string> fragmentSource; //source code of the fragment shaders
	std::vector<std::string> geometrySource; //source code of the geometry shaders
	std::vector<GLuint> vertexIDs;  //list of vertex shaders IDs
	std::vector<GLuint> fragmentIDs; //list of fragment shaders IDs
	std::vector<GLuint> geometryIDs; //list of geometry shaders IDs
	//resize the containers to match the size of the shader paths list
	vertexSource.resize(shaderPaths.size());
	fragmentSource.resize(shaderPaths.size());
	geometrySource.resize(shaderPaths.size());
	vertexIDs.resize(shaderPaths.size());
	fragmentIDs.resize(shaderPaths.size());
	geometryIDs.resize(shaderPaths.size());
	shaderIDs.resize(shaderPaths.size());
	
	//create and compile the vertex and fragment shaders, log to console if there is a problem with the compilation
	for (std::vector<GLuint>::iterator it = vertexIDs.begin(); it != vertexIDs.end(); it++) {
		*it = glCreateShader(GL_VERTEX_SHADER);
		char* cpath =textFileRead((char*)std::get<0>(shaderPaths[std::distance(vertexIDs.begin(), it)]).c_str());
		if (cpath == NULL)exit(-1);
		glShaderSource(*it, 1, &cpath, NULL);
		glCompileShader(*it);
		int compilation_status;
		glGetShaderiv(*it, GL_COMPILE_STATUS, &compilation_status);
		printConsole(std::get<0>(shaderPaths[std::distance(vertexIDs.begin(), it)]).append("\n"));
		debugShader(*it);
		if (compilation_status == GL_FALSE) {
			exit(-2);
		}
	}
	for (std::vector<GLuint>::iterator it = fragmentIDs.begin(); it != fragmentIDs.end(); it++) {
		*it = glCreateShader(GL_FRAGMENT_SHADER);
		const char* cpath = textFileRead((char*)std::get<1>(shaderPaths[std::distance(fragmentIDs.begin(), it)]).c_str());
		if (cpath == NULL)exit(-3);
		glShaderSource(*it, 1, &cpath, NULL);
		glCompileShader(*it);
		int compilation_status;
		glGetShaderiv(*it, GL_COMPILE_STATUS, &compilation_status);
		printConsole(std::get<1>(shaderPaths[std::distance(fragmentIDs.begin(), it)]).append("\n"));
		debugShader(*it);
		if (compilation_status == GL_FALSE) {
			exit(-4);
		}
	}

	for (std::vector<GLuint>::iterator it = geometryIDs.begin(); it != geometryIDs.end(); it++) {
		if ((char*)std::get<2>(shaderPaths[std::distance(geometryIDs.begin(), it)]).length()==0) {
			continue;
		}
		*it = glCreateShader(GL_GEOMETRY_SHADER);
		const char* cpath = textFileRead((char*)std::get<2>(shaderPaths[std::distance(geometryIDs.begin(), it)]).c_str());
		if (cpath == NULL) {
			std::cout << "index is " << std::distance(geometryIDs.begin(), it) << std::endl;
			std::cout << "Path is |" << (char*)std::get<2>(shaderPaths[std::distance(geometryIDs.begin(), it)]).c_str() <<"|"<< std::endl;
			exit(-5);
		}
		glShaderSource(*it, 1, &cpath, NULL);
		glCompileShader(*it);
		int compilation_status;
		glGetShaderiv(*it, GL_COMPILE_STATUS, &compilation_status);
		printConsole(std::get<2>(shaderPaths[std::distance(geometryIDs.begin(), it)]).append("\n"));
		debugShader(*it);
		if (compilation_status == GL_FALSE) {
			exit(-6);
		}
	}
	//create the shader programs and attach the vertex and fragment shaders, link the shader programs 
	for (std::vector<GLuint>::iterator it = shaderIDs.begin(); it < shaderIDs.end(); it++) {
		*it = glCreateProgram();
		glAttachShader(*it, vertexIDs[std::distance(shaderIDs.begin(), it)]);
		glAttachShader(*it, geometryIDs[std::distance(shaderIDs.begin(), it)]);
		glAttachShader(*it, fragmentIDs[std::distance(shaderIDs.begin(), it)]);
		glLinkProgram(*it);
	}
}