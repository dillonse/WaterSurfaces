#include "TerrainHeightmap.h"
#include <GL\glew.h>
#include <random>
#include <glm/exponential.hpp>
#include <glm/gtx/string_cast.hpp>
#include "instance.h"
#include <iostream>
#include <algorithm>
#include "VBO.h"

std::default_random_engine gennnerator;
std::uniform_real_distribution<float> diiistribution(-0.01f, 0.01f);

void MidpointDisplacement(TerrainHeightMap& map) {
	for (int i = 0; i < map.size.x; i++) {
		for (int j = 0; j < map.size.y; j++) {
			//glm::ivec2 ind = glm::vec2(i - map.size.x*0.35f, j - map.size.y*0.35f);
			glm::vec2 center = glm::vec2(map.size)*0.5f;
			float divOrth = glm::min(
				glm::min(glm::distance(center, glm::vec2(i,map.size.y)), glm::distance(glm::vec2(map.size.x, j), center)),
				glm::min(glm::distance(center,glm::vec2(i,0.0f)), glm::distance(glm::vec2(0,j), center))
				);
			float divDiag = glm::min(
				glm::min(glm::distance(center, glm::vec2(map.size.x, map.size.y)), glm::distance(glm::vec2(map.size.x,0.0f), center)),
				glm::min(glm::distance(center, glm::vec2(0.0f, 0.0f)), glm::distance(glm::vec2(0, map.size.y), center))
			);
			float div = glm::min(divDiag, divOrth);
			map.nodes[i][j].height = 0.0-100.0f*(1-glm::length(glm::vec2(i, j) - center) / div);
				//-((map.size.x*0.5 - i)*(map.size.y*0.5-j))*0.0005;//+diiistribution(gennnerator);
			map.nodes[i][j].refractionNormal = glm::vec2(0.0f);
		}
	}
	//add some islands
	glm::vec2 isle_center = glm::vec2(50, 125);
	float isle_radius = 50;
	float isle_amp = 50.0f;
	for (int i = 0; i < map.size.x; i++) {
		for (int j = 0; j < map.size.y; j++) {
			glm::vec2 position = glm::vec2(i, j)*map.offset.x;
			if (glm::distance(position, isle_center) < isle_radius) {
				map.nodes[i][j].height += isle_amp*((isle_radius - glm::distance(position,isle_center))/(isle_radius));
			}
		}
	}
	isle_center = glm::vec2(50, 75);
	for (int i = 0; i < map.size.x; i++) {
		for (int j = 0; j < map.size.y; j++) {
			glm::vec2 position = glm::vec2(i, j)*map.offset.x;
			if (glm::distance(position, isle_center) < isle_radius) {
				map.nodes[i][j].height += isle_amp*((isle_radius - glm::distance(position, isle_center)) / (isle_radius));
			}
		}
	}
	//create contour lines
	///collect the heights
	std::vector<float> heights;
	heights.reserve(map.size.x*map.size.y);
	for (int i = 0; i < map.size.x; i++) {
		for (int j = 0; j < map.size.y; j++) {
			if(map.nodes[i][j].height>-10)
			heights.push_back(map.nodes[i][j].height);
		}
	}
	///sort them
	std::sort(heights.begin(), heights.end());
	///create a list of height values, the size of which is equivalent to the num of contour lines
	std::vector<float> contourHeights;
	contourHeights.resize(6);
	int j = 0;
	for (int i = 0; i < heights.size(); i += heights.size() / (contourHeights.size()-1)) {
		contourHeights[j++] = heights[i];
		//std::cout << "contour height [" << j - 1 << "] is " << contourHeights[j - 1] << std::endl;
	}
	contourHeights.push_back(0.1);
	///classify the map points according to the height
	for (int i = 0; i < map.size.x; i++) {
		for (int j = 0; j < map.size.y; j++) {
			float & height = map.nodes[i][j].height;
			std::vector<float>::iterator result= std::find_if(contourHeights.begin(), contourHeights.end(), [height](auto i) {return height < i; });
			if (result != contourHeights.end()) {
				map.nodes[i][j].contourIndex = std::distance(contourHeights.begin(), result);
				//std::cout << "contour index of " << height << " is " << map.nodes[i][j].contourIndex << std::endl;
			}
		}
	}
	////calculate the refraction normals
	for (int i = 0; i < map.size.x; i++) {
		for (int j = 0; j < map.size.y;j++) {
			///count the distances to a deeper waterbed in 8 directions
			float distLeft=std::numeric_limits<float>::max();
			float distRight = std::numeric_limits<float>::max();
			float distUp = std::numeric_limits<float>::max();
			float distDown = std::numeric_limits<float>::max();
			float distRU= std::numeric_limits<float>::max();
			float distRD= std::numeric_limits<float>::max();
			float distLU = std::numeric_limits<float>::max();
			float distLD = std::numeric_limits<float>::max();
			for (int k = i; k < map.size.x; k++) {
				if (map.nodes[k][j].contourIndex < map.nodes[i][j].contourIndex) {
					distRight = (k-i)*map.offset.x;
					break;
				}
			}
			for (int k = i; k > 0; k--) {
				if (map.nodes[k][j].contourIndex < map.nodes[i][j].contourIndex) {
					distLeft = (i-k)*map.offset.x;
					break;
				}
			}
			for (int k = j; k < map.size.y; k++) {
				if (map.nodes[i][k].contourIndex < map.nodes[i][j].contourIndex) {
					distUp = (k-j)*map.offset.y;
					break;
				}
			}
			for (int k = j; k > 0; k--) {
				if (map.nodes[i][k].contourIndex < map.nodes[i][j].contourIndex) {
					distDown = (j-k)*map.offset.y;
					break;
				}
			}
			int k = i; int l = j;
			while (++k < map.size.x&&++l < map.size.y) {
				if (map.nodes[k][l].contourIndex < map.nodes[i][j].contourIndex) {
					distRU = glm::distance(glm::vec2(i*map.offset.x, j*map.offset.y), glm::vec2(k*map.offset.x,l*map.offset.y));
					break;
				}
			}
			k = i; l = j;
			while (--k >0&&++l < map.size.y) {
				if (map.nodes[k][l].contourIndex < map.nodes[i][j].contourIndex) {
					distLU = glm::distance(glm::vec2(i*map.offset.x, j*map.offset.y), glm::vec2(k*map.offset.x, l*map.offset.y));
					break;
				}
			}
			k = i; l = j;
			while (--k >0 && --l > 0) {
				if (map.nodes[k][l].contourIndex < map.nodes[i][j].contourIndex) {
					distLD = glm::distance(glm::vec2(i*map.offset.x, j*map.offset.y), glm::vec2(k*map.offset.x, l*map.offset.y));
					break;
				}
			}
			k = i; l = j;
			while (++k <map.size.x && --l > 0) {
				if (map.nodes[k][l].contourIndex < map.nodes[i][j].contourIndex) {
					distRD = glm::distance(glm::vec2(i*map.offset.x, j*map.offset.y), glm::vec2(k*map.offset.x, l*map.offset.y));
					break;
				}
			}
			std::vector<std::pair<float,int>> distances;
			distances.reserve(8);
			distances.push_back(std::make_pair(distLeft,0));
			distances.push_back(std::make_pair(distRight,1));
			distances.push_back(std::make_pair(distUp,2));
			distances.push_back(std::make_pair(distDown,3));
			distances.push_back(std::make_pair(distRU,4));
			distances.push_back(std::make_pair(distRD,5));
			distances.push_back(std::make_pair(distLU,6));
			distances.push_back(std::make_pair(distLD,7));
			///sort the distances vector
			std::sort(distances.begin(), distances.end(), [](auto i, auto j) {return i.first < j.first; });
			///find the limit (first element that is not equal to element 0)
			float first_dist = distances[0].first;
			std::vector<std::pair<float, int>>::iterator result = std::find_if(distances.begin(), distances.end(), [first_dist](auto i) {return i.first != first_dist; });
			int limit = distances.size();
			if (result != distances.end()) {
				limit = std::distance(distances.begin(), result);
			}
			map.nodes[i][j].refractionNormal = glm::vec2(0.0f);
			
			if (first_dist != std::numeric_limits<float>::max()) {
				for (int k = 0; k < limit; k++) {
					std::pair<float, int>& element = distances[k];
					if (element.second == 0) {
						map.nodes[i][j].refractionNormal += glm::vec2(-1, 0);
					}
					else if (element.second == 1) {
						map.nodes[i][j].refractionNormal += glm::vec2(1, 0);
					}
					else if (element.second == 2) {
						map.nodes[i][j].refractionNormal += glm::vec2(0, 1);
					}
					else if (element.second == 3) {
						map.nodes[i][j].refractionNormal += glm::vec2(0, -1);
					}
					else if (element.second == 4) {
						map.nodes[i][j].refractionNormal += glm::vec2(1, 1);
					}
					else if (element.second == 5) {
						map.nodes[i][j].refractionNormal += glm::vec2(1, -1);
					}
					else if (element.second == 6) {
						map.nodes[i][j].refractionNormal += glm::vec2(-1, 1);
					}
					else if (element.second == 7) {
						map.nodes[i][j].refractionNormal += glm::vec2(-1, -1);
					}
				}
			}
			
			if (map.nodes[i][j].refractionNormal != glm::vec2(0.0f))
				map.nodes[i][j].refractionNormal = glm::normalize(map.nodes[i][j].refractionNormal);
		}
	}
}

TerrainHeightMap::TerrainHeightMap(glm::ivec2 dimensions, glm::vec2 offsets)
{
		nodes.resize(dimensions.x);
		for (int i = 0; i < dimensions.x; i++) {
			nodes[i].resize(dimensions.y);
		}
		size = dimensions;
		offset = offsets;
		MidpointDisplacement(*this);
}

float TerrainHeightMap::getHeight(int i, int j, HeightParameters & pars)
{
	if(i<size.x&&j<size.y&&i>=0&&j>=0)
		return nodes[i][j].height;
	return 0.0f;
}

float TerrainHeightMap::getHeight(glm::vec2 query)
{
	if (query.x<0 || query.y<0 || query.x>(size.x)*offset.x || query.y>(size.y)*offset.y)
		return 0.0f;
	query /= offset;
	//take the four corner points by taking the ceil and floor of the query coordinates
	glm::ivec2 NW = glm::ivec2(glm::floor(query));
	glm::ivec2 NE = glm::ivec2(glm::floor(query.x), glm::ceil(query.y));
	glm::ivec2 SE = glm::ivec2(glm::ceil(query));
	glm::ivec2 SW = glm::ivec2(glm::ceil(query.x), glm::ceil(query.y));
	//take the blending coeff by taking the decimal parts of the query point
	glm::vec2 blend = glm::fract(query);
	//calculate the height using bilinear interpolation:
	///calculate the northside height
	HeightParameters pars;
	float NHeight = getHeight(NW.x, NW.y, pars)*(1 - blend.x)+getHeight(NE.x,NE.y,pars)*blend.x;
	float SHeight = getHeight(SW.x, SW.y, pars)*(1 - blend.x) + getHeight(SE.x, SE.y, pars)*blend.x;
	return NHeight*(1 - blend.y) + SHeight*blend.y;

}

glm::vec2 TerrainHeightMap::getRefractionNormal(int i, int j)
{
	if (i<size.x&&j<size.y)
		return nodes[i][j].refractionNormal;
	return glm::vec2(0.0f);
}

glm::vec2 TerrainHeightMap::getRefractionNormal(glm::vec2 query)
{
	if (query.x<0 || query.y<0 || query.x>(size.x)*offset.x || query.y>(size.y)*offset.y)
		return glm::vec2(0.0f);
	query /= offset;
	//take the four corner points by taking the ceil and floor of the query coordinates
	glm::ivec2 NW = glm::ivec2(glm::floor(query));
	glm::ivec2 NE = glm::ivec2(glm::floor(query.x), glm::ceil(query.y));
	glm::ivec2 SE = glm::ivec2(glm::ceil(query));
	glm::ivec2 SW = glm::ivec2(glm::ceil(query.x), glm::ceil(query.y));
	//take the blending coeff by taking the decimal parts of the query point
	glm::vec2 blend = glm::fract(query);
	//calculate the height using bilinear interpolation:
	///calculate the northside height
	//HeightParameters pars;
	glm::vec2 NRefNormal = getRefractionNormal(NW.x, NW.y)*(1 - blend.x) + getRefractionNormal(NE.x, NE.y)*blend.x;
	glm::vec2 SRefNormal = getRefractionNormal(SW.x, SW.y)*(1 - blend.x) + getRefractionNormal(SE.x, SE.y)*blend.x;
	return NRefNormal*(1 - blend.y) + SRefNormal*blend.y;
}

int TerrainHeightMap::getContourIndex(int i, int j) {
	if (i<size.x&&j<size.y)
		return nodes[i][j].contourIndex;
	return -1;
}

int TerrainHeightMap::getContourIndex(glm::vec2 query) {
	if (query.x<0 || query.y<0 || query.x>(size.x)*offset.x || query.y>(size.y)*offset.y)
		return -1;
	query /= offset;
	//take the four corner points by taking the ceil and floor of the query coordinates
	glm::ivec2 NW = glm::ivec2(glm::floor(query));
	glm::ivec2 NE = glm::ivec2(glm::floor(query.x), glm::ceil(query.y));
	glm::ivec2 SE = glm::ivec2(glm::ceil(query));
	glm::ivec2 SW = glm::ivec2(glm::ceil(query.x), glm::ceil(query.y));
	//take the blending coeff by taking the decimal parts of the query point
	glm::vec2 blend = glm::fract(query);
	//calculate the height using bilinear interpolation:
	///calculate the northside height
	//HeightParameters pars;
	int NRefNormal = getContourIndex(NW.x, NW.y)*(1 - blend.x) + getContourIndex(NE.x, NE.y)*blend.x;
	int SRefNormal = getContourIndex(SW.x, SW.y)*(1 - blend.x) + getContourIndex(SE.x, SE.y)*blend.x;
	return NRefNormal*(1 - blend.y) + SRefNormal*blend.y;;
}



void TerrainHeightMap::Render(RenderParameters & pars)
{
	inst.mat = ((TRenderParameters&) pars).mat;
	inst.RenderTriangles();
}

void TerrainHeightMap::PrepareInstanceForRendering()
{
	HeightParameters parsHeight;
	inst.mod = new model();
	inst.mod->vertices.resize(size.x*size.y*6);
	inst.mod->normals.resize(inst.mod->vertices.size());
	inst.mod->UVs.resize(inst.mod->vertices.size());
	for (int i = 0; i < size.x - 1; i++) {
		for (int j = 0; j < size.y - 1; j++) {
			glm::vec3 p0 = glm::vec3(i*offset.x, getHeight(i, j, parsHeight), j*offset.y);
			glm::vec3 p1 = glm::vec3(i*offset.x, getHeight(i, j+1, parsHeight), (j+1)*offset.y);
			glm::vec3 p2 = glm::vec3((i+1)*offset.x, getHeight(i+1, j + 1, parsHeight), (j + 1)*offset.y);
			glm::vec3 p3 = glm::vec3((i + 1)*offset.x, getHeight(i + 1, j, parsHeight), (j)*offset.y);
			glm::vec2 uv0 = glm::vec2(0.5, 0.5);
			glm::vec2 uv1 = glm::vec2(0.5, 0.5);
			glm::vec2 uv2 = glm::vec2(0.5, 0.5);
			glm::vec2 uv3 = glm::vec2(0.5, 0.5);
			float offset = 0.01;
			if (i % 2 == 0) {
				if (j % 2 == 1) {
					uv0 = glm::vec2(1.0f-offset, 0.0f+offset);
					uv1 = glm::vec2(1.0f-offset, 1.0f-offset);
					uv2 = glm::vec2(0.0f+offset, 1.0f-offset);
					uv3 = glm::vec2(0.0f+offset, 0.0f+offset);
				}
				else {
					uv0 = glm::vec2(1.0f-offset, 1.0f- offset);
					uv1 = glm::vec2(1.0f- offset, 0.0f+ offset);
					uv2 = glm::vec2(0.0f+ offset, 0.0f+ offset);
					uv3 = glm::vec2(0.0f+ offset, 1.0f- offset);
				}
			}
			else {
				if (j % 2 == 0) {
					uv0 = glm::vec2(0.0f+ offset, 1.0f- offset);
					uv1 = glm::vec2(0.0f+ offset, 0.0f+ offset);
					uv2 = glm::vec2(1.0f- offset, 0.0f+ offset);
					uv3 = glm::vec2(1.0f- offset, 1.0f- offset);
				}
				else {
					uv0 = glm::vec2(0.0f+ offset, 0.0f+ offset);
					uv1 = glm::vec2(0.0f+ offset, 1.0f- offset);
					uv2 = glm::vec2(1.0f- offset, 1.0f- offset);
					uv3 = glm::vec2(1.0f- offset, 0.0f+ offset);
				}

			}
			int index = (i*size.x + j) * 6;
			glm::vec3 normalforall = glm::vec3(0, 1, 0);
			inst.mod->vertices[index] = p0;
			inst.mod->UVs[index] = uv0;
			inst.mod->normals[index] = normalforall;
			inst.mod->vertices[index + 1] = p1;
			inst.mod->UVs[index+1] = uv1;
			inst.mod->normals[index+1] = normalforall;
			inst.mod->vertices[index + 2] = p3;
			inst.mod->UVs[index+2] = uv3;
			inst.mod->normals[index+2] = normalforall;
			inst.mod->vertices[index + 3] = p1;
			inst.mod->UVs[index+3] = uv1;
			inst.mod->normals[index+3] = normalforall;
			inst.mod->vertices[index + 4] = p2;
			inst.mod->UVs[index+4] = uv2;
			inst.mod->normals[index+4] = normalforall;
			inst.mod->vertices[index + 5] = p3;
			inst.mod->UVs[index+5] = uv3;
			inst.mod->normals[index+5] = normalforall;
			
		}
	}
	buffers::Load(*inst.mod);
	
}
