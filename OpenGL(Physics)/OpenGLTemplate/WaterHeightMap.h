#pragma once
#include "HeightMap.h"
#include "model.h"
#include "material.h"
#include <vector>
#include"Dampers.h"

class WaterHeightMapNodeRecord {
public:
	int wavefront_id;
	float amplitude;
	float angular_frequency;
	float init_phase_space;
	float init_phase_time;
	float distorted_phase_time;
	float distorted_phase_space;
	float weight;
	float noiseMagnitude;
	glm::vec2 direction;
	bool isCrestWave;
	DampingCoefficients dampingCoeff;
};

class WaterHeightMapNode {
public:
	void Add(WaterHeightMapNodeRecord& record,float time);
	std::vector<WaterHeightMapNodeRecord> records;
	glm::vec3 normal;
	float weight_sum = 0;
};

class WaterHeightMapHeightParameters:public HeightParameters {
public:
	float time;
};

class WaterHeightMapRenderParameters:public RenderParameters {
public:
	float time;
	model* mod;
	material* mat;
};

class WaterHeightMap:public HeightMap
{
public:
	WaterHeightMap() {};
	WaterHeightMap(glm::ivec2 dimensions, glm::vec2 offsets);
	std::vector<std::vector<WaterHeightMapNode>> nodes;
	float getHeight(int i, int j, HeightParameters& pars);
	glm::vec2 getDirection(int i, int j,HeightParameters& pars);
	float getSpeed(int i, int j, HeightParameters& pars);
	void Render(RenderParameters& pars);
	void PrepareInstanceHeightsForRendering(RenderParameters& pars,std::vector<GLfloat>& renderingHeights);
	void PrepareInstanceDirectionForRendering(RenderParameters& pars, std::vector<glm::vec2>& renderingDirections);
	void PrepareInstanceNormalsForRendering(std::vector<std::vector<float>>& heights, std::vector<std::vector<glm::vec3>>& normals);
	void finalizeRenderingSpeeds(std::vector<glm::vec2>& renderingDirections,std::vector<GLfloat>& renderingSpeeds);
	void calculateNormals(WaterHeightMapRenderParameters& pars);
};

