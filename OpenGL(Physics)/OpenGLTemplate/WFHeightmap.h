#pragma once
#include "HeightMap.h"
#include <vector>
#include <glm/trigonometric.hpp>
#include "material.h"
#include "model.h"
#include "Dampers.h"

class WFHeightMapNodeRecord {
public:
	int wavefront_id;
	float amplitude;
	float travel_time;
	float angular_frequency;
	DampingCoefficients damper;
	glm::vec2 velocity;
};

class WFHeighMapNode {
public:
	std::vector<WFHeightMapNodeRecord> records;
	glm::vec3 normal;
	void RemoveWorstRecord();
};


class WFHeightParameters :public HeightParameters {
public:
	float time = 0.0f;
};

class WFRenderParameters : public RenderParameters {
public:
	float time = 0.0f;
	material* mat=NULL;
	model* mod = NULL;
};

class WFHeightMap : public HeightMap {
public:
	WFHeightMap(glm::ivec2 dimensions, glm::vec2 offsets);
	float getHeight(int i, int j, HeightParameters& pars);
	void calculateNormals(WFRenderParameters& pars);
	void Render(RenderParameters& pars);
	std::vector<std::vector<WFHeighMapNode>> nodes;
};