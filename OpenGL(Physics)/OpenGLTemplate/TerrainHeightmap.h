#pragma once
#include "Heightmap.h"
#include <vector>
#include "material.h"
#include "instance.h"

class TRenderParameters :public RenderParameters {
public:
	material* mat = NULL;
};

class TerrainHeightMapNode {
public:
	float height;
	glm::vec2 refractionNormal;
	int contourIndex;
};

class TerrainHeightMap : public HeightMap {
public:
	instance inst;
	TerrainHeightMap(glm::ivec2 dimensions, glm::vec2 offset);
	std::vector<std::vector<TerrainHeightMapNode>> nodes;
	float getHeight(int i, int j, HeightParameters& pars);
	float getHeight(glm::vec2 query);
	glm::vec2 getRefractionNormal(int i, int j);
	glm::vec2 getRefractionNormal(glm::vec2 query);
	int getContourIndex(int i, int j);
	int getContourIndex(glm::vec2 query);
	void Render(RenderParameters& pars);
	void PrepareInstanceForRendering();
};