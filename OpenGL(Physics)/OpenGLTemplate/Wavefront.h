#pragma once
#include <vector>
#include <glm\common.hpp>
#include "material.h"
#include "TerrainHeightMap.h"
#include "Dampers.h"

class Wavefront_vertex {
public:
	Wavefront_vertex(glm::vec2 pos, glm::vec2 vel) :position(pos), velocity(vel) { ; };
	Wavefront_vertex() { position = glm::vec2(0.0f); velocity = glm::vec2(0.0f); };
	void Update(float wavenum, float depth);
	glm::vec2 position;
	glm::vec2 velocity;
};

class Wavefront_edge {
public:
	Wavefront_edge(float energDens, float gSpeed,float ampl,float wnum) :energyDensity(energDens),groupSpeed(gSpeed),amplitude(ampl),wavenum(wnum){ ; };
	Wavefront_edge() { energyDensity = 0.0f; groupSpeed = 0.0f; wavenum = 0.0f; amplitude = 0.0f; }
	void Update(float angular_freq, float depth,float initValue);
	float energyDensity;
	float groupSpeed;
	float wavenum;
	float amplitude;
};

class Wavefront {
public:
	//constructors
	static int instance_counter;
	Wavefront() { id=instance_counter++; };
	Wavefront(Wavefront& wavefront, int begin, int end);
	//vertices
	std::vector<Wavefront_vertex> VRead; //reader and writter pointers
	std::vector<Wavefront_vertex> VWrite;
	//edges
	std::vector<Wavefront_edge> ERead; //reader and writter pointers
	std::vector<Wavefront_edge> EWrite;
	float wavenum;
	float angular_frequency;
	int id;
	DampingCoefficients dampingCoeff;
	bool isDestructable = false;
	bool isCrestWave = false;
	float lifeTime = 0.0f;
	void Init(TerrainHeightMap& map,float energDens);
	void Render();
	void Update(TerrainHeightMap& map);
};
