#pragma once
#include<Windows.h>
#include <string>
#include "debug.h"
std::string message = "";
void printConsole(std::string& str)
{
	std::wstring wstr = std::wstring(str.begin(), str.end());
	LPCWSTR lpcwstr = wstr.c_str();
	OutputDebugString(lpcwstr);
}

void debugShader(GLuint shader) {
	int maxLength;
	glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &maxLength);
	std::string errorLog; errorLog.resize(maxLength);
	glGetShaderInfoLog(shader, maxLength, &maxLength, &errorLog[0]);
	printConsole(errorLog);
}

void printScreen() {
	glDisable(GL_DEPTH_TEST);
	glUseProgram(0);
	glColor3f(1, 0, 0);
	float vOffset = 0.04f;
	int nlines = 0;
	glRasterPos3f(-1, 1 - vOffset*nlines , -1.0f);
	for (int i = 0; i<message.size(); i++)
	{
		if (message[i] == '\n') {
			nlines++;
			glRasterPos3f(-1, 1 - vOffset*nlines, -1.0f);
			continue;
		}
		glutBitmapCharacter(GLUT_BITMAP_TIMES_ROMAN_10, message[i]);
	}
	glEnable(GL_DEPTH_TEST);
}

