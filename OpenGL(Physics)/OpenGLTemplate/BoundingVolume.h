#pragma once
#include <glm\vec3.hpp>
#include <vector>
#include "instance.h"
class BoundingSphere
{
public:
	instance inst;
	float radius;
	glm::vec3 position;
	glm::vec3 bodyspacePosition;
	BoundingSphere(instance& render_inst){
		this->inst.mod = new model(*render_inst.mod);
		this->inst.mat = new material(*render_inst.mat);
	};
	void Init(instance& geometry);
	void Render();
};

class OBB
{
public:
	instance inst;
	glm::vec3 extends;
	glm::vec3 position;
	glm::vec3 bodyspacePosition;
	std::vector<glm::vec3> faceNormals;
	std::vector<glm::vec3> bodyspaceFaceNormals;
	OBB(instance& render_inst) :inst(render_inst) {
		this->inst.mod = new model(*inst.mod);
		this->inst.mat = new material(*inst.mat);
	};
	void Init(instance& geometry);
	void Render(glm::mat3& orientation);
};
