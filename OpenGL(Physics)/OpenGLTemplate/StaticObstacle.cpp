#include "StaticObstacle.h"
#include "Collider.h"
#include <iostream>
#include <glm\gtx\string_cast.hpp>


float crosss(glm::vec2 v0, glm::vec2 v1) {
	return v0.x*v1.y - v0.y*v1.x;
}

glm::vec2 findCrossingPoint(glm::vec2 p0, glm::vec2 p1, glm::vec2 q0, glm::vec2 q1) {
	glm::vec2 p = p0;
	glm::vec2 q = q0;
	glm::vec2 direction_p = glm::normalize(p1 - p0);
	glm::vec2 direction_q = glm::normalize(q1 - q0);
	float scalar_q = crosss(q - p, direction_p) / crosss(direction_p, direction_q);
	return q + direction_q*scalar_q;
}

void StaticObstacle::findCollisions(Wavefront &w,std::vector<Wavefront>& outWavefronts) {
	for (int i = 0; i < w.VWrite.size()-1; i++) {
		Wavefront_vertex& v1 = w.VWrite[i];
		Wavefront_vertex& v2 = w.VWrite[i + 1];
		Wavefront_vertex& v1past = w.VRead[i];
		Wavefront_vertex& v2past = w.VRead[i + 1];
		CollisionType type= findCollisionType(v1, v2);
		if (type == COLLISION_INSIDE) {
			//find the intersection points between the outline and the time traces
			//findIntersections(v1, v1past,);
			//findIntersections(v2, v2past);
		}
		else if (type == COLLISION_OUTSIDE) {
			//do nothing
		}
		else if (type == COLLISION_STRONGLY_OUTSIDE) {
			//do nothing
		}
		else if (type == COLLISION_INBETWEEN) {
			Wavefront reflection;
			Wavefront_vertex v0out;
			Wavefront_vertex v1out;
			reflection.angular_frequency = w.angular_frequency;
			reflection.wavenum = w.wavenum;
			if (findIntersections(v1, v2, w.EWrite[i], reflection,v0out,v1out)) {
				Wavefront second;
				second.angular_frequency = w.angular_frequency;
				second.wavenum = w.wavenum;
				second.VWrite.insert(second.VWrite.end(), std::next(w.VWrite.begin(),i+1), w.VWrite.end());
				second.VRead.insert(second.VRead.end(), std::next(w.VRead.begin(), i + 1), w.VRead.end());
				second.EWrite.insert(second.EWrite.end(), std::next(w.EWrite.begin(), i + 1), w.EWrite.end());
				second.ERead.insert(second.ERead.end(), std::next(w.ERead.begin(), i + 1), w.ERead.end());
				second.VWrite[0] = v1out;
				second.VRead[0] = v1out;
				second.VRead[0].position -= second.VRead[0].velocity*0.01f;

				w.VWrite.resize(i+1);
				w.VRead.resize(i+1);
				w.EWrite.resize(i);
				w.ERead.resize(i);
				w.VWrite[i] = v0out;
				w.VRead[i] = v0out;
				w.VRead[i].position -= second.VRead[0].velocity*0.01f;
				outWavefronts.push_back(reflection);
 				outWavefronts.push_back(second);
			}
			//find the intersection point between the outline and the edge 
		}

		/*if (type == 2) {
			std::cout << "type is " <<"STRONGLY_OUTSIDE" << std::endl;
		}
		else if (type == 0) {
			std::cout << "type is " << "INSIDE" << std::endl;
		}
		else if (type == 1) {
			std::cout << "type is " << "OUTSIDE" << std::endl;
		}
		else if (type == 3) {
			std::cout << "type is " << "IN BETWEEN" << std::endl;
		}*/
	}
}

bool StaticObstacle::findIntersections(Wavefront_vertex & v0, Wavefront_vertex & v1, Wavefront_edge& edge,Wavefront& outWavefront, Wavefront_vertex& v0out, Wavefront_vertex& v1out)
{
	//static bool hasDone = false;
	//if (hasDone)return false;
	std::vector<glm::vec2> newOutline(immersionOutLine);
	std::vector<int> crossingPoints;
	Wavefront_vertex v1refl;
	int offset = 0;
	std::cout << "v0 is " << glm::to_string(v0.position) << " and v1 is " << glm::to_string(v1.position) << std::endl;
	for (int i = 0; i < immersionOutLine.size()-1; i++) {
		glm::vec2 p0 = immersionOutLine[i];
		glm::vec2 p1 = immersionOutLine[i + 1];
		std::cout << "looking at " << glm::to_string(p0) << "-" << glm::to_string(p1) << "...";
		//check if v1 v2 crosses p0 p1
		Location location1 = findLocation(p0, p1, v0.position);
		Location location2 = findLocation(p0, p1, v1.position);
		if (location1 != location2) {
			location1 = findLocation(v0.position, v1.position, p0);
			location2 = findLocation(v0.position, v1.position, p1);
			if (location1!=location2) {//it's a crossing
				glm::vec2 crossingPoint = findCrossingPoint(p0, p1, v0.position, v1.position);
				//std::cout << "crossing point is " << glm::to_string(crossingPoint) << " and points are " << glm::to_string(p0) << glm::to_string(p1) << std::endl;
				newOutline.insert(std::next(newOutline.begin(), i+1+offset), crossingPoint);
				crossingPoints.push_back(i +1+ offset);
				offset++;
				std::cout << "YES" << std::endl;
			}
			else {
				std::cout << "NO" << std::endl;
			}
		}
		else {
			std::cout << "NO" << std::endl;
		}
	}
	glm::vec2 direction = (glm::normalize(v0.velocity)+glm::normalize(v1.velocity))*0.5f;
	if (crossingPoints.size() == 1) {
		std::cout << "one case" << std::endl;
		//find which point is inside
		glm::vec2 p0 = immersionOutLine[crossingPoints[0]];
		int next = crossingPoints[0] + 1 - ((crossingPoints[0]+1) / (newOutline.size()))*newOutline.size();
		glm::vec2 p1 = immersionOutLine[next];
		Location location = findLocation(p0, p1, v0.position);
		if (location == LOCATION_INSIDE) {
			//
		}
		else if (location == LOCATION_OUTSIDE) {

		}

	}
	else if (crossingPoints.size() == 2) {
		std::cout << "classic case" << std::endl;
		glm::vec2 A = newOutline[crossingPoints[0]];
		glm::vec2 B = newOutline[crossingPoints[1]];
		v0out = Wavefront_vertex(v0);
		v1out = Wavefront_vertex(v1);
		
		//std::cout << "direction is " << glm::to_string(direction) << std::endl;
		//std::cout << "crossings at " << glm::to_string(A) << " and " << glm::to_string(B) << std::endl;
		//chcek if B is left to A or vice versa
		//find where the start and the end of the outline
		int start, end;
		start = 0; end = 0;
		Location location = findLocation(A, A + direction, B);

		if (location == LOCATION_INSIDE) {
			//B->A
 			start = crossingPoints[1];
			end = crossingPoints[0];
		}
		else if (location == LOCATION_OUTSIDE) {
			//A->B
			start = crossingPoints[0];  
			end = crossingPoints[1];
		}
		else {
			//std::cout << "???" << std::endl;
		}
		//find the position and velocity of v0out and v1out
		if (glm::distance(v0.position, newOutline[crossingPoints[0]]) < glm::distance(v0.position, newOutline[crossingPoints[1]])) {
			v0out.position = newOutline[crossingPoints[0]];
			v1out.position = newOutline[crossingPoints[1]];
			//v0out velocity
			int next0 = (crossingPoints[0] + 1) - ((crossingPoints[0] + 1) / newOutline.size())*newOutline.size();
			if (newOutline[crossingPoints[0]] == newOutline[next0]) {
				next0 = (crossingPoints[0] + 2) - ((crossingPoints[0] + 2) / newOutline.size())*newOutline.size();
			}
			glm::vec2 tangent0 = glm::normalize(newOutline[next0] - newOutline[crossingPoints[0]]);
			float angle0 = glm::dot(tangent0, direction);
			v0out.velocity = glm::length(v0.velocity)*tangent0*glm::sign(angle0);
			//v1out velocity
			int next1 = (crossingPoints[1] + 1) - ((crossingPoints[1] + 1) / newOutline.size())*newOutline.size();
			if (newOutline[crossingPoints[1]] == newOutline[next1]) {
				next0 = (crossingPoints[1] + 2) - ((crossingPoints[1] + 2) / newOutline.size())*newOutline.size();
			}
			glm::vec2 tangent1 = glm::normalize(newOutline[next1] - newOutline[crossingPoints[1]]);
			float angle1 = glm::dot(tangent1, direction);
			v1out.velocity = glm::length(v1.velocity)*tangent1*glm::sign(angle1);
		}
		else {
			v0out.position = newOutline[crossingPoints[1]];
			v1out.position = newOutline[crossingPoints[0]];
			//v0out velocity
			int next0 = (crossingPoints[1] + 1) - ((crossingPoints[1] + 1) / newOutline.size())*newOutline.size();
			if (newOutline[crossingPoints[1]] == newOutline[next0]) {
				next0 = (crossingPoints[1] + 2) - ((crossingPoints[1] + 2) / newOutline.size())*newOutline.size();
			}
			glm::vec2 tangent0 = glm::normalize(newOutline[next0] - newOutline[crossingPoints[1]]);
			float angle0 = glm::dot(tangent0, direction);
			v0out.velocity = glm::length(v0.velocity)*tangent0*glm::sign(angle0);
			//v1out velocity
			int next1 = (crossingPoints[0] + 1) - ((crossingPoints[0] + 1) / newOutline.size())*newOutline.size();
			if (newOutline[crossingPoints[0]] == newOutline[next1]) {
				next0 = (crossingPoints[0] + 2) - ((crossingPoints[0] + 2) / newOutline.size())*newOutline.size();
			}
			glm::vec2 tangent1 = glm::normalize(newOutline[next1] - newOutline[crossingPoints[0]]);
			float angle1 = glm::dot(tangent1, direction);
			v1out.velocity = glm::length(v1.velocity)*tangent1*glm::sign(angle1);
		}


		//loop over the outline
		int index = start;
		int counter = index;
		int size = newOutline.size();
		
		while (index != end) {
			//calculate the perperndicular direction
			int nextIndex = (counter + 1) - ((counter + 1) / size)*size;
			if (newOutline[index] != newOutline[nextIndex]) {
				glm::vec2 perpendicular = glm::normalize(newOutline[nextIndex] - newOutline[index]);
				perpendicular = -glm::vec2(-perpendicular.y, perpendicular.x);

				//check the corner between the perpendicular and the wave direction
				float angle = glm::dot(perpendicular, direction);
				if (angle > 0.63) {
					//std::cout << "perp is " << glm::to_string(perpendicular) << std::endl;
					//add crossing points to wavefront
					Wavefront_vertex v0refl(v0);
					v1refl = Wavefront_vertex(v1);
					Wavefront_edge edgerefl(edge);
					v0refl.velocity = glm::reflect(direction, perpendicular)*glm::length(v0.velocity); //(maybe reduce magnitude due to collision absorbing energy?
					v0refl.position = newOutline[index];
					v1refl.position = newOutline[nextIndex];
					v1refl.velocity = glm::normalize(v0refl.velocity)*glm::length(v1.velocity);
					outWavefront.VWrite.push_back(v0refl);
					//outWavefront.VWrite.push_back(v1refl);
					outWavefront.EWrite.push_back(edgerefl);
					v0refl.position += -v0.velocity;
					//v1refl.position += -v1.velocity;
					outWavefront.VRead.push_back(v0refl);
					//outWavefront.VRead.push_back(v1refl);
					outWavefront.ERead.push_back(edgerefl);
					if (v0refl.velocity != v0refl.velocity) {
						//std::cout << "gotcha" << std::endl;
					}
					if (v1refl.velocity != v1refl.velocity) {
						//std::cout << "gotcha2" << std::endl;
					}
					//std::cout << "new direction " << glm::to_string(glm::normalize(v0refl.velocity)) << std::endl;

				}
				
			}
			counter++;
			index = counter - (counter / size)*size;
		}
	}
	else if (crossingPoints.size() > 2) {
		//std::cout << "crazy case:" <<crossingPoints.size()<< std::endl;
	}
	//outWavefront.VRead = std::vector<Wavefront_vertex>(outWavefront.VWrite);
	//outWavefront.ERead = std::vector<Wavefront_edge>(outWavefront.EWrite);
	if (outWavefront.VWrite.size()> 0) {
		outWavefront.VWrite.push_back(v1refl);
		v1refl.position += -v1refl.velocity;
		outWavefront.VRead.push_back(v1refl);
		//hasDone = true;
		std::cout << "size is " << outWavefront.VWrite.size() << std::endl;
	}
	return (outWavefront.VWrite.size() > 0);

	/*for (int i = 0; i < crossingPoints.size(); i++) {
		std::cout << "crossing point is " << glm::to_string(newOutline[crossingPoints[i]]) << std::endl;
	}*/

}

StaticObstacle::CollisionType StaticObstacle::findCollisionType(Wavefront_vertex &v1,Wavefront_vertex &v2)
{
	CollisionType type=COLLISION_INSIDE;
	for (int i = 0; i < immersionOutLine.size()-1; i ++) {
		glm::vec2 p0 = immersionOutLine[i];
		glm::vec2 p1 = immersionOutLine[i + 1];
		Location location1 = findLocation(p0, p1, v1.position);
		Location location2 = findLocation(p0, p1, v2.position);
		if (location1 == location2) {
			if (location1 == LOCATION_OUTSIDE) {
				type = COLLISION_STRONGLY_OUTSIDE;
				break;
			}
			else if (location1 == LOCATION_INSIDE) {
				//type = COLLISION_INSIDE;
			}
		}
		else {
			//check if it is a crossing
			location1 = findLocation(v1.position, v2.position, p0);
			location2 = findLocation(v1.position, v2.position, p1);
			//std::cout << "case!" << std::endl;
			if (location1 == location2) {//not crossing
				//type = COLLISION_OUTSIDE;
			}
			else {//crossing
				type = COLLISION_INBETWEEN;
				break;
			}
		}
	}
	return type;
}

StaticObstacle::Location StaticObstacle::findLocation(glm::vec2 p0,glm::vec2 p1,glm::vec2 query)
{
	Location location;
	float sign = (p1.x - p0.x)*(query.y - p0.y) - (query.x - p0.x)*(p1.y - p0.y);
	if (sign > 0) {
		location = LOCATION_OUTSIDE;
	}
	else if (sign == 0) {
		location = LOCATION_ON;
	}
	else {
		location = LOCATION_INSIDE;
	}
	return location;
}

void StaticObstacle::UpdateImmersionOutLine()
{
	//Outline in global coordinates//
	//Assumes convex geometry!!!//
	glm::vec3 planeOrigin = glm::inverse(inst.transform)*glm::vec4(0, 0, 0, 1);
	glm::vec3 planeUp = glm::inverse(inst.transform)*glm::vec4(0, 1, 0, 1);
	planeUp = glm::normalize(planeUp - planeOrigin);
	for (int i = 0; i < inst.mod->vertices.size(); i+=3) {
		glm::vec3 p0 =inst.mod->vertices[i];
		glm::vec3 p1 = inst.mod->vertices[i + 1];
		glm::vec3 p2 = inst.mod->vertices[i + 2];
		float distance0 = distance::point2plane(planeOrigin, planeUp, p0);
		float distance1 = distance::point2plane(planeOrigin, planeUp, p1);
		float distance2 = distance::point2plane(planeOrigin, planeUp, p2);
		
		if (glm::sign(distance0) != glm::sign(distance1)) {
			glm::vec3 globalP0 = inst.transform*glm::vec4(p0, 1.0f);
			glm::vec3 globalP1 = inst.transform*glm::vec4(p1, 1.0f);
			glm::vec3 direction01 = glm::normalize(globalP1 - globalP0);
			float scalar = -(globalP0.y / direction01.y);
			glm::vec3 projection = globalP0 + direction01*scalar;
			if (std::find(immersionOutLine.begin(), immersionOutLine.end(), glm::vec2(projection.x,projection.z)) == immersionOutLine.end()) {
				immersionOutLine.push_back(glm::vec2(projection.x, projection.z));
			}
		}
		if (glm::sign(distance1) != glm::sign(distance2)) {
			glm::vec3 globalP1 = inst.transform*glm::vec4(p1, 1.0f);
			glm::vec3 globalP2 = inst.transform*glm::vec4(p2, 1.0f);
			glm::vec3 direction12 = glm::normalize(globalP2 - globalP1);
			float scalar = -(globalP1.y / direction12.y);
			glm::vec3 projection = globalP1 + direction12*scalar;
			if (std::find(immersionOutLine.begin(), immersionOutLine.end(), glm::vec2(projection.x,projection.z)) == immersionOutLine.end()) {
				immersionOutLine.push_back(glm::vec2(projection.x, projection.z));
			}
		}
		if (glm::sign(distance2) != glm::sign(distance0)) {
			glm::vec3 globalP2 = inst.transform*glm::vec4(p2, 1.0f);
			glm::vec3 globalP0 = inst.transform*glm::vec4(p0, 1.0f);
			glm::vec3 direction20 = glm::normalize(globalP0 - globalP2);
			float scalar = -(globalP2.y / direction20.y);
			glm::vec3 projection = globalP2 + direction20*scalar;
			if (std::find(immersionOutLine.begin(), immersionOutLine.end(), glm::vec2(projection.x,projection.z)) == immersionOutLine.end()) {
				immersionOutLine.push_back(glm::vec2(projection.x, projection.z));
			}
		}
	}
	std::vector<glm::vec2> orientedOutline;
	orientedOutline.resize(immersionOutLine.size());

	//Find convex hull
	///find top-left most point
	glm::vec2 topLeft = immersionOutLine[0];
	for (int i = 1; i < immersionOutLine.size(); i++) {
		if (immersionOutLine[i].x<=topLeft.x&&immersionOutLine[i].y>=topLeft.y) {
			topLeft = immersionOutLine[i];
		}
	}
	///assume horizontal direction and check against other candidates
	orientedOutline[0] = topLeft;
	glm::vec2 previousDirection = glm::vec2(1, 0);
	glm::vec2 previousPivot = topLeft;
	//refine the previous direction
	float minDistance = std::numeric_limits<float>::max();
	for (int i = 0; i < immersionOutLine.size(); i++) {
		if (immersionOutLine[i] == topLeft||immersionOutLine[i].x<topLeft.x) {
			continue;
		}
		if (glm::distance(immersionOutLine[i], topLeft) < minDistance) {
			minDistance = glm::distance(immersionOutLine[i], topLeft);
			previousDirection = glm::normalize(immersionOutLine[i] - topLeft);
		}
	}
	//find the oriented outeline
	float minAngle = std::numeric_limits<float>::max();
	for (int i = 1; i < immersionOutLine.size(); i++) {
		glm::vec2 bestCandidate = immersionOutLine[0];
		if (bestCandidate == previousPivot)bestCandidate = immersionOutLine[1];
		float bestAngle = glm::dot(glm::normalize(bestCandidate-previousPivot),previousDirection);
		glm::vec2 bestDirection = glm::normalize(bestCandidate - previousPivot);
		for (int j = 0; j < immersionOutLine.size(); j++) {
			if (immersionOutLine[j] == previousPivot)continue;
			float angle = glm::dot(glm::normalize(immersionOutLine[j]-previousPivot), previousDirection);
			if (angle > bestAngle) {
				bestCandidate = immersionOutLine[j];
				bestAngle = angle;
				bestDirection = glm::normalize(immersionOutLine[j] - previousPivot);
			}
			else if (angle == bestAngle) {
				if (glm::distance(immersionOutLine[j], previousPivot) < glm::distance(bestCandidate, previousPivot)) {
					bestCandidate = immersionOutLine[j];
					bestAngle = angle;
					bestDirection = glm::normalize(immersionOutLine[j] - previousPivot);
				}
			}
		}
		orientedOutline[i] = bestCandidate;
		previousDirection = glm::normalize(bestCandidate - previousPivot);
		previousPivot = bestCandidate;
	}
	immersionOutLine = std::vector<glm::vec2>(orientedOutline);
	for (int i = 0; i < immersionOutLine.size()-1; i++) {
		std::cout << "size[" << i << "] is " << glm::distance(immersionOutLine[i], immersionOutLine[i + 1]) << std::endl;
	}
	if(immersionOutLine.size()>0)
		immersionOutLine.push_back(*immersionOutLine.begin());
	//std::cout << "im outline size is " << immersionOutLine.size() << std::endl;

}

void StaticObstacle::Update(Wavefront& w,std::vector<Wavefront>& outWavefronts)
{
	findCollisions(w,outWavefronts);
}

void StaticObstacle::Update(Wavefront & w)
{
	for (int i = 0; i < w.VWrite.size(); i++) { //for each vertex in the wavefront
		Wavefront_vertex& v0 = w.VRead[i];
		Wavefront_vertex& v1 = w.VWrite[i];
		for (int j = 0; j < immersionOutLine.size()-1; j++) {
			Location location1 = findLocation(v0.position, v1.position, immersionOutLine[j]);
			Location location2 = findLocation(v0.position, v1.position, immersionOutLine[j+1]);
			if (location1 != location2&&location1!=LOCATION_ON&&location2!=LOCATION_ON) {
				location1 = findLocation(immersionOutLine[j], immersionOutLine[j + 1], v0.position);
				location2 = findLocation(immersionOutLine[j], immersionOutLine[j + 1], v1.position);
				if (location1 != location2&&location1 != LOCATION_ON&&location2 != LOCATION_ON) {
					//find the crossing point and the normal,tangent and incident vectors
					glm::vec2 crossing = findCrossingPoint(v0.position, v1.position, immersionOutLine[j], immersionOutLine[j + 1]);
					glm::vec2 incident = glm::normalize(v1.position - v0.position);
					glm::vec2 tangent = glm::normalize(immersionOutLine[j + 1] - immersionOutLine[j]);
					glm::vec2 normal = glm::vec2(-tangent.y, tangent.x);
					//change the position to the crossing point
					v1.position = crossing+normal*0.1f;
					//find the angle to check if the point should reflect or difract
					float angle = glm::dot(normal, -incident);
					//reflect or diffract
					//std::cout << "angle is " << angle << std::endl;
					if (angle > 0.63) {//reflect
						v1.velocity = glm::reflect(incident, normal)*glm::length(v1.velocity);
					}
					else {//refract
						if (glm::dot(tangent,-incident) > 0) {//on the bitangent
							v1.velocity = -tangent*glm::length(v1.velocity);
						}
						else {//on the tangent
							v1.velocity = tangent*glm::length(v1.velocity);
						}
					}
				}
			}
		}
	}
}

void StaticObstacle::Render()
{
	return;
	inst.RenderTriangles();
	glm::vec4 colour = inst.mat->color;
	inst.mat->color = glm::vec4(1, 0, 1, 1);
	inst.BindMaterial();
	glBegin(GL_LINE_LOOP);
	glm::mat4 inverse = glm::inverse(inst.transform);
	glm::vec4 position;
	for (int i = 0; i < immersionOutLine.size(); i++) {
		position = inverse*glm::vec4(immersionOutLine[i].x,0,immersionOutLine[i].y,1);
		glVertex3f(position.x,0,position.z);
	}
	glEnd();
	inst.mat->color = colour;
	inst.BindMaterial();

}
