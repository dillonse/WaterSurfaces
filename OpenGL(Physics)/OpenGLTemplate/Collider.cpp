#include "Collider.h"
#include <cmath>
#include <glm\gtx\string_cast.hpp>
#include<glm\geometric.hpp>
#include <glm\glm.hpp>
#include <iostream>
#include "debug.h"

float distance::point2point(glm::vec3 p0, glm::vec3 p1) {
	return std::sqrt((p0.x - p1.x)*(p0.x - p1.x) + (p0.y - p1.y)*(p0.y - p1.y) + (p0.z - p1.z)*(p0.z - p1.z));
}

float distance::point2edge(glm::vec3 p0, glm::vec3 p1, glm::vec3 p2) {
	//In my understanding we need only the d1, the d2 and d3 will always be >=d1 but I follow the slides
	float d1 = glm::length(glm::cross((p2-p1),(p0-p1)))/glm::length(p2-p1);
	float d2 = glm::length(p0 - p1);
	float d3 = glm::length(p0 - p2);
	
	return (glm::min)(d1, (glm::min)(d2, d3));
}

float distance::point2plane(glm::vec3 p0, glm::vec3 normal, glm::vec3 p1) {
	return (glm::dot((p1 - p0), normal));
}

glm::vec3 nearest::point2Plane(glm::vec3 p,Plane plane) {
	return p - glm::dot(p-plane.position,plane.normal)*plane.normal;
}

glm::vec3 nearest::point2Triangle(glm::vec3 p0, glm::vec3 p1, glm::vec3 p2, glm::vec3 p3,nearest::FEATURE& feat) {
	//check if the p0 lies in the voronoi region of the point
	//p1
	if (glm::dot((p0 - p1), (p2 - p1)) <= 0 && glm::dot((p0 - p1), (p3 - p1)) <= 0) {
		feat = nearest::FEATURE::P1;
		return p1;
	}
	else if (glm::dot((p0 - p2), (p1 - p2)) <= 0 && glm::dot((p0 - p2), (p3 - p2)) <= 0) {
		feat = nearest::FEATURE::P2;
		return p2;
	}
	else if (glm::dot((p0 - p3), (p1 - p3)) <= 0 && glm::dot((p0 - p3), (p2 - p3)) <= 0) {
		feat = nearest::FEATURE::P3;
		return p3;
	}
	//check if the p0 lies on the voronoi region of the edges
	//p1 p2
	else if (glm::dot(glm::cross(glm::cross(p3 - p2, p1 - p2), p1 - p2), (p0 - p2)) >= 0
		&& glm::dot(p0 - p1, p2 - p1) >= 0
		&& glm::dot(p0 - p2, p1 - p2) >= 0
		) {
		//calculate the projection
		glm::vec3 u_d = glm::normalize(p2 - p1);
		glm::vec3 pe = p1 + glm::dot(p0 - p1, u_d)*u_d;
		//std::cout << "p2 is " << glm::to_string(p2) << " p1 is " << glm::to_string(p1) << " and " << glm::to_string(pe) << std::endl;
		feat = nearest::FEATURE::P12;
		return pe;
	}
	//p2-p3
	else if (glm::dot(glm::cross(glm::cross(p1 - p3, p2 - p3), p2 - p3), (p0 - p3)) >= 0
		&& glm::dot(p0 - p2, p3 - p2) >= 0
		&& glm::dot(p0 - p3, p2 - p3) >= 0
		) {
		//calculate the projection
		glm::vec3 u_d = glm::normalize(p3 - p2);
		glm::vec3 pe = p2 + glm::dot(p0 - p2, u_d)*u_d;
		feat = nearest::FEATURE::P23;
		return pe;
	}
	//p3-p1
	else if (glm::dot(glm::cross(glm::cross(p2 - p1, p3 - p1), p3 - p1), (p0 - p1)) >= 0
		&& glm::dot(p0 - p3, p1 - p3) >= 0
		&& glm::dot(p0 - p1, p3 - p1) >= 0
		) {
		//calculate the projection
		glm::vec3 u_d = glm::normalize(p1 - p3);
		glm::vec3 pe = p3 + glm::dot(p0 - p3, u_d)*u_d;
		feat = nearest::FEATURE::P31;
		return pe;
	}
	//it is the triangle inner area
	else {
		glm::vec3 n = glm::normalize(glm::cross((p2 - p1), (p3 - p1)));
		glm::vec3 pf = p0 - glm::dot((p0 - p1), n)*n;
		feat = nearest::FEATURE::P123;
		return pf;
	}
}

bool detection::Sphere(Rigidbody& r1, Rigidbody& r2) {
	//check if the two bodies collide based on their bounding spheres
	if (glm::length(glm::vec3(r1.bsphere.position) - glm::vec3(r2.bsphere.position)) <= r1.bsphere.radius + r2.bsphere.radius) {
		return true;
	}
	return false;
}

bool detection::OBB(Rigidbody& r1, Rigidbody& r2) {

	//check if the two bodies collide based on their OBBs
	std::vector<glm::vec3> axes;
	axes.reserve(15);
	//faces of first OBB
	glm::vec3 u0a = r1.obb.faceNormals[0];
	glm::vec3 u1a = r1.obb.faceNormals[1];
	glm::vec3 u2a = r1.obb.faceNormals[2];
	//faces of second OBB
	glm::vec3 u0b = r2.obb.faceNormals[0];
	glm::vec3 u1b = r2.obb.faceNormals[1];
	glm::vec3 u2b = r2.obb.faceNormals[2];
	//cross products (edge axes)
	glm::vec3 u0axu0b = glm::cross(u0a, u0b);
	glm::vec3 u0axu1b = glm::cross(u0a, u1b);
	glm::vec3 u0axu2b = glm::cross(u0a, u2b);

	glm::vec3 u1axu0b = glm::cross(u1a, u0b);
	glm::vec3 u1axu1b = glm::cross(u1a, u1b);
	glm::vec3 u1axu2b = glm::cross(u1a, u2b);

	glm::vec3 u2axu0b = glm::cross(u2a, u0b);
	glm::vec3 u2axu1b = glm::cross(u2a, u1b);
	glm::vec3 u2axu2b = glm::cross(u2a, u2b);
	//push dem on the axes vector
	axes.push_back(u0a);
	axes.push_back(u1a);
	axes.push_back(u2a);
	axes.push_back(u0b);
	axes.push_back(u1b);
	axes.push_back(u2b);
	axes.push_back(u0axu0b);
	axes.push_back(u0axu1b);
	axes.push_back(u0axu2b);
	axes.push_back(u1axu0b);
	axes.push_back(u1axu1b);
	axes.push_back(u1axu2b);
	axes.push_back(u2axu0b);
	axes.push_back(u2axu1b);
	axes.push_back(u2axu2b);
	bool isIntersecting = true;
	for (std::vector<glm::vec3>::iterator itAxis = axes.begin(); itAxis != axes.end(); itAxis++) { //for all 15 axes
		*itAxis = glm::normalize(*itAxis);
		//calculate midpoint separation
		float s = glm::abs(glm::dot(r1.position - r2.position, *itAxis));
		//calculate the "radii" of the OBBs
		//calculate the projected radii
		float ra = r1.obb.extends.x*glm::abs(glm::dot(u0a, *itAxis)) +
			r1.obb.extends.y*glm::abs(glm::dot(u1a, *itAxis)) +
			r1.obb.extends.z*glm::abs(glm::dot(u2a, *itAxis));
		float rb = r2.obb.extends.x*glm::abs(glm::dot(u0b, *itAxis)) +
			r2.obb.extends.y*glm::abs(glm::dot(u1b, *itAxis)) +
			r2.obb.extends.z*glm::abs(glm::dot(u2b, *itAxis));
		if (s > ra + rb) {
			isIntersecting = false;
			break;
		}

	}
	axes.clear();
	if (isIntersecting)
		return true;
	return false;
}
